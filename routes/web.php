<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/','HomeController@index')->name('home');
// Route::get('/login/custom','HomeController@showlogin')->name('login.get');
Route::get('/registration','HomeController@showregsitration')->name('register');
Route::post('/registration-store','HomeController@registration')->name('storeregister');
Auth::routes();
Route::middleware('auth')->group(function () {

    Route::get('/dashboard','DashboardController@dashboard')->name('dashboard');
    Route::get('/submission','SubmissionController@showform')->name('submissionform');
    Route::post('/submission-store','SubmissionController@storeform')->name('storesubmission');
    Route::get('/livesession','DashboardController@livelink')->name('livelinks');
});

    Route::post('lang', function(){
       
        if(\Session::has('lang') && \Session::get('lang') == 'en'){
            \Session::put('lang','ar');
        }else{
            \Session::put('lang','en');
        }
        return back();
    })->name('lang');   

Route::prefix('admin')->group(function(){

    Route::get('/login','Auth\AdminController@ShowLogin')->name('adminlogin');
    Route::post('/login','Auth\AdminController@login')->name('checkadminlogin');

    Route::middleware(['auth:admin'])->group(function(){
    Route::get('admin/edit', 'AdminController@edit')->name('admin.edit');
    Route::put('admin/update', 'AdminController@update')->name('admin.update');
    Route::get('/dashboard','AdminController@dashboard')->name('admindashboard');
    Route::get('/users','AdminController@AllUsers')->name('admin.attendants');
    Route::get('/submissions','AdminController@AllSubmissions')->name('admin.submissions');
    Route::get('/imagesubmission/{fileupload}','SubmissionController@SubmissionImage')->name('admin.submissions.image');
    Route::post('logout', 'Auth\AdminController@logout')->name('admin.logout');
    Route::get('/speakers', 'AdminController@speakers')->name('admin.speakers');
    Route::get('/acceptsubmission/{id}','SubmissionController@Accept')->name('admin.accept.submission');
    Route::get('/rejectsubmission/{id}','SubmissionController@Reject')->name('admin.reject.submission');
    Route::prefix('speaker')->group(function () {
        Route::get('/create', 'AdminController@speakerForm')->name('admin.speaker.create');
        Route::post('/stored', 'AdminController@speakerCreate')->name('admin.speaker.store');
        Route::post('/approve', 'AdminController@speakerApprove')->name('admin.speaker.approve');
        Route::get('/{id}', 'AdminController@viewSpeaker')->name('admin.speaker.view');
        Route::get('edit/{id}', 'AdminController@editSpeaker')->name('admin.speaker.edit');
        Route::post('update/{id}', 'AdminController@updateSpeaker')->name('admin.speaker.update');
        Route::post('/sessions', 'AdminController@speakerSessions')->name('admin.speaker.sessions');
        Route::post('/session', 'AdminController@speakerSession')->name('admin.speaker.session');
        Route::post('/meeting', 'AdminController@speakerMeeting')->name('admin.speaker.meeting');

    });
    Route::delete('speakers/{speaker}', 'AdminController@deleteSpeakers')->name('admin.speaker.delete');
    Route::post('speakers/{speaker}/restore', 'AdminController@restoreSpeakers')->name('admin.speaker.restore');
    // -----------------Writers-----------------------------------------
    Route::get('/writers', 'AdminController@writers')->name('admin.writers');
    Route::prefix('writer')->group(function () {
        Route::get('/create', 'AdminController@writerForm')->name('admin.writer.create');
        Route::post('/stored', 'AdminController@writerCreate')->name('admin.writer.store');
        Route::post('/approve', 'AdminController@writerApprove')->name('admin.writer.approve');
        Route::get('/{id}', 'AdminController@viewWriter')->name('admin.writer.view');
        Route::get('edit/{id}', 'AdminController@editWriter')->name('admin.writer.edit');
        Route::post('update/{id}', 'AdminController@updateWriter')->name('admin.writer.update');
        Route::post('/sessions', 'AdminController@writerSessions')->name('admin.writer.sessions');
        Route::post('/session', 'AdminController@writerSession')->name('admin.writer.session');
        Route::post('/meeting', 'AdminController@writerMeeting')->name('admin.writer.meeting');

    });
    Route::delete('writers/{writer}', 'AdminController@deleteWriter')->name('admin.writer.delete');
    Route::post('writers/{writer}/restore', 'AdminController@restoreWriter')->name('admin.writer.restore');
     // -----------------Sessions-----------------------------------------
     Route::get('/sessions', 'AdminController@sessions')->name('admin.sessions');
     Route::prefix('session')->group(function () {
         Route::get('/create', 'AdminController@sessionForm')->name('admin.session.create');
         Route::post('/stored', 'AdminController@sessionCreate')->name('admin.session.store');
         Route::post('/approve', 'AdminController@sessionApprove')->name('admin.session.approve');
         Route::get('/{id}', 'AdminController@viewSession')->name('admin.session.view');
         Route::get('edit/{id}', 'AdminController@editSession')->name('admin.session.edit');
         Route::post('update/{id}', 'AdminController@updateSession')->name('admin.session.update');
         Route::post('/sessions', 'AdminController@writerSessions')->name('admin.writer.sessions');
         Route::post('/session', 'AdminController@writerSession')->name('admin.writer.session');
         Route::post('/meeting', 'AdminController@writerMeeting')->name('admin.writer.meeting');

     });
     Route::delete('sessions/{session}', 'AdminController@deleteSession')->name('admin.session.delete');
     Route::post('sessions/{session}/restore', 'AdminController@restoreSession')->name('admin.session.restore');

    //  -----------------------Links-------------------------------------------------------------
    Route::get('links', 'AdminController@links')->name('admin.links');
        Route::prefix('link')->group(function () {
            Route::post('/create', 'AdminController@linkCreate')->name('admin.link.create');
            Route::post('/update/{id}', 'AdminController@linkUpdate')->name('admin.link.update');
            Route::get('/toggle/status', 'AdminController@toggleLink')->name('admin.link.toggle');
        });
    //  -----------------------Agenda-------------------------------------------------------------

        Route::prefix('agenda')->group(function () {
            Route::get('/', 'AdminController@agenda')->name('admin.agenda.index');
            Route::get('/create', 'AdminController@agendaAdd')->name('admin.agenda.add');
            Route::post('/create-agenda', 'AdminController@agendaCreate')->name('admin.agenda.create');
            Route::get('/edit/{id}', 'AdminController@agendaEdit')->name('admin.agenda.edit');
            Route::post('/update/{id}', 'AdminController@agendaUpdate')->name('admin.agenda.update');

        });
        Route::delete('agenda/{agenda}', 'AdminController@deleteAgenda')->name('admin.agenda.delete');
        Route::post('agenda/{agenda}/restore', 'AdminController@restoreAgenda')->name('admin.agenda.restore');
        Route::post('send/emails/{type}', 'EmailController@send')->name('admin.send.emails');

        //-----------------------------Template ------------------------------------
        Route::put('email/templates/{template}/status', 'TemplateController@status')->name('admin.emails.templates.status');
        Route::get('email/templates', 'TemplateController@index')->name('admin.emails.templates.index');
        Route::get('email/templates/create', 'TemplateController@create')->name('admin.emails.templates.create');
        Route::post('email/templates', 'TemplateController@store')->name('admin.emails.templates.store');
        Route::get('email/templates/{template}/edit', 'TemplateController@edit')->name('admin.emails.templates.edit');
        Route::put('email/templates/{template}', 'TemplateController@update')->name('admin.emails.templates.update');
        Route::get('email/templates/{template}/show', 'TemplateController@show')->name('admin.templates.show');


        Route::get('questions', 'QuestionController@index')->name('admin.questions.index');
        Route::put('questions/{question}/status', 'QuestionController@status')->name('admin.questions.status');
        Route::post('send/emails/{type}', 'EmailController@send')->name('admin.send.emails');
        Route::get('send/emails', 'EmailController@sendEmail')->name('admin.sendemail.emails');
    Route::post('/ajax-accept', 'SubmissionController@AcceptAjax')->name('acceptajax');


});

});
// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
