<?php

namespace App\Http\Controllers;
use App\Models\MeetingLink;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    function dashboard(){
        return view('dashboard');
    }
    function livelink(){
        $youtubelink = MeetingLink::where('title','youtube')->get();
        $twitterlink = MeetingLink::where('title','twitter')->get();
        return view('livelink',compact('youtubelink','twitterlink'));
    }
}
