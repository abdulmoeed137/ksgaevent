<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'Name' => ['required'],
            'Email' => ['required','email', 'unique:users'],
            'Company' => ['required'],
            'Position' => ['required'],
            'Address' => ['required'],
            'Phone' => ['required'],
            'Password' => ['required','confirmed'],
            'Attendance' => ['required'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'Name' => $data['Name'],
            'Email' => $data['Email'],
            'Company' => $data['Company'],
            'Position' => $data['Position'],
            'Phone' => $data['Phone'],
            'Address' => $data['Address'],
            'Phone' => $data['Phone'],
            'Password' => Hash::make($data['Password']),
            'Attendance' => $data['Attendance'],

        ]);
        
        return redirect()->route('register')->with('message','You are Successfully Registered');
    }
}
