<?php

namespace App\Http\Controllers;

use App\Jobs\ReminderEmail;
use Illuminate\Http\Request;

class EmailController extends Controller
{
    public function send(Request $request, $type)
    {
        try {
            $request->validate([
                'template' => 'required|integer|exists:templates,id',
                'recipients' => 'required|array|min:1|distinct',
                'recipients.*' => 'required|integer|exists:' . $type . 's,id'
            ]);
            if ($type == 'speaker') {
                $recipients = Speaker::whereIn('id', $request->recipients)->get();
            } else {
                $recipients = Attendant::whereIn('id', $request->recipients)->get();
            }
            foreach ($recipients as $recipient) {
                $recipient->mails()->create([
                    'template_id' => $request->template
                ]);
            }
            dispatch((new SendEmails())->delay(now()->addSeconds(10)));
            return response()->json(['message' => 'Email send successfully'], 200);
        } catch (\Exception $e) {
            if ($e instanceof ValidationException) {
                return response()->json([
                    'message'    => 'Error',
                    'success' => false,
                    'errors' => $e->errors(),
                ], 422);
            } else {
                return response()->json([
                    'message'    => 'Error',
                    'success' => false,
                    'errors' => $e->getMessage(),
                    'trace' => $e->getTrace(),
                ], 500);
            }
        }
    }
    public function sendEmail(){
        dispatch(new ReminderEmail)->delay(now()->addSecond());
        return response()->json(['message'=>'Email Sent Succesfully']);
    }
}
