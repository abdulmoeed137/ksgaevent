<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Models\Submission;
use Auth;

class SubmissionController extends Controller
{
    function showform(){
        return view('submission');
    }
    function storeform(Request $request){

        // dd();
        $this->validate($request,[
            'Name' => 'required',
            'Email' => 'required|email',
            'Organization' => 'required',
            'ConferenceTopic' => 'required',
            'PhoneNumber' => 'required|regex:/^([0-9\+]*)$/',
            'Titleofresearch' => 'required',
            'Country'=>'required',
            'City'=>'required',
            'fileupload'=>'required|max:5000|mimes:pdf',

        // dd($request)

        ],
        [
            'Name.required'    => 'Input is required here',
            'Email.email'    => 'Email format invalid',
            'Email.unique'    => 'This email already registered',
            'Email.required'    => 'Input is required here',
            'Organization.required'    => 'Input is required here',
            'ConferenceTopic.required'    => 'Input is required here',
            'PhoneNumber.required'    => 'Input is required here',
            'PhoneNumber.regex'    => 'Phone Number invalid',
            'Titleofresearch.required'    => 'Input is required here',
            'Country.required'    => 'Input is required here',
            'City.required'    => 'Input is required here',
            'fileupload.required'    => 'Input is required here',
            'fileupload.mimes'    => 'File should be pdf format',
        ]
    );
        // dd($request);

        $submission = new Submission();
        $submission->name = $request->Name;
        $submission->email = $request->Email;
        $submission->organization = $request->Organization;
        $submission->conferencetopic = $request->ConferenceTopic;
        $submission->phonenumber = $request->PhoneNumber;
        $submission->titleofresearch = $request->Titleofresearch;
        $submission->country = $request->Country;
        $submission->city = $request->City;
        $imageName = time().'.'.$request->fileupload->getClientOriginalExtension();
        $request->fileupload->move(public_path('/submissionimages'), $imageName);
        $submission->fileupload = $imageName;
        $submission->user_id = Auth::user()->id;
        $submission->status = 'pending';
        $submission->save();
        //  dd($request);
       if(\App::getLocale()=="en"){
        return redirect('/submission')->with('message','Your submission is saved successfully');
       }
       return redirect('/submission')->with('message','تم الارسال بنجاح');

    }
    function Accept($id){
        $submission = Submission::findorFail($id);
        $submission->status = "accepted";
        $submission->save();
        return redirect()->back()->with('message','Submission Accepted Successfully');
    }
    function Reject($id){
        $submission = Submission::findorFail($id);
        $submission->status = "reject";
        $submission->save();
        return redirect()->back()->with('message','Submission Rejected Successfully');
    }
    function SubmissionImage($fileupload){
        $file_path = public_path('submissionimages/'.$fileupload);
        return response()->download($file_path);
    }
}
