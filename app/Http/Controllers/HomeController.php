<?php

namespace App\Http\Controllers;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\Speaker;
use App\Models\Session;
use App\Models\Writer;
use App\Models\Agenda;
use App\Models\Template;



class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // dd(\App::getLocale());
        $speakers = Speaker::all();
        $sessions = Session::all();
        $writers = Writer::all();
        $DayoneAgenda = Agenda::where('day','1')->get();
        $DaytwoAgenda = Agenda::where('day','2')->get();
        $SSAgenda = Agenda::where('type','session')->get();
        $BRAgenda = Agenda::where('type','break')->get();
        // dd("come here");
        return view('home',compact('speakers','sessions','writers','DayoneAgenda','DaytwoAgenda','BRAgenda'));
    }

    function registration(Request $request){
        // dd($request);
        // dd(\App::getLocale());
        $messages = [];
        if(\App::getLocale()=="en"){
            $messages = [
                'Name.required'    => 'Input is required here',
                'Email.email'    => 'Email format invalid',
                'Email.unique'    => 'This email already registered',
                'Email.required'    => 'Input is required here',
                'Company.required'    => 'Input is required here',
                'Position.required'    => 'Input is required here',
                'Phone.required'    => 'Input is required here',
                'Phone.regex'    => 'Phone Number invalid',
                'Address.required'    => 'Input is required here',
                'Password.required'    => 'Input is required here',
                'Confirm_Password.same'    => 'The Confirm Password and Password must match',
            ];
        }
       
        $this->validate($request,[
            'Name' => 'required',
            'Email' => 'required|email|unique:users',
            'Company' => 'required',
            'Position' => 'required',
            'Phone' => 'required|regex:/^([0-9\+]*)$/',
            'Address' => 'required',
            'Password'=>'required',
            'Confirm_Password'=>'same:Password',

            // 'c_password' => 'required|same:password',
        ],$messages
        
    );
        $data = new User();
        $data->name=$request->Name;
        $data->email=$request->Email;
        $data->company=$request->Company;
        $data->position=$request->Position;
        $data->phone=$request->Phone;
        $data->address=$request->Address;
        $data->password=Hash::make($request->Password);
        $data->attendance = $request->attendance;
        // if($request->attendance=='on'){
        //     $data->attendance="Onsight";
        // }else{
        //     $data->attendance="Online";
        // }
        $data->save();

        $template = Template::where('name','Registration')->first();
        // dd($template);
        $details = [

            'subject' => $template->subject,
            'content'=>$template->content,
            // 'body' => 'You have successfully registered at King Salman Global Academy.',
            // 'email' => $request->Email,
            // 'name'=>$request->Name,
        ];
       \Mail::to($request->Email)->send(new \App\Mail\RegisterMail($details));
       if(\App::getLocale()=="en"){
        return redirect()->route('register')->with('message','You are Successfully Registered');

       }
        return redirect()->route('register')->with('message','تم التسجيل بنجاح');
    }
}
