<?php

namespace App\Http\Controllers;
use DataTables;
use App\Models\User;
use App\Models\Submission;
use App\Models\Speaker;
use App\Models\Template;
use App\Models\Writer;
use App\Models\Session;
use App\Models\Setting;
use App\Models\MeetingLink;
use App\Models\Agenda;
// use App\Chart;
use Validator;
use Illuminate\Support\Facades\DB;


use Hash;

use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function edit()
    {
        $user = auth()->user();
        return view('Admin.edit', compact('user'));
    }

    public function update(Request $request)
    {
        $user = auth()->user();
        // dd($user->id);
        $rules = [
            'name' => 'required|string|min:2|max:255',
            'email' => 'required|email:filter|max:255|unique:users,email,' . $user->id
        ];

        $msg = 'Profile info updated successfully';
        if ($request->hasAny(['old_password', 'password', 'confirmation_password'])) {
            $rules = [
                'old_password' => 'required|min:8|max:255',
                'password' => 'required|min:8|max:255',
            ];
            $msg = 'Profile password changed successfully';
        }

        $validated = $request->validate($rules);
        // dd('sdjzfxsfvcak');

        if ($request->hasAny(['old_password', 'password', 'confirmation_password'])) {
            $user->update(['password' => Hash::make($validated['password'])]);
        } else {
            $user->update([
                'name' => $validated['name'],
                'email' => $validated['email']
            ]);
        }
        toastr()->success($msg);
        return back();
    }

    public function dashboard()
    {

        // dd($per_day_attendants);
        // dd();
        // dd('im on dashboard');
//----------------------------------------Start Commented BY T---------------------------

        // $attendants = Attendant::count();
        // $speakers = Speaker::count();
        $per_day_users =DB::table('users')->select('created_at','name', DB::raw("COUNT(id) as users_count"))->groupBy(DB::raw("DATE(created_at)"))->get();
        // dd($per_day_users);
        $label = [];
        $graph_data = [];
        $colours = [];
        $name = [];
 foreach ($per_day_users as $per_day_attendant) {
            $label[] = date('d', strtotime($per_day_attendant->created_at)) . '-' . date('M', strtotime($per_day_attendant->created_at));
            $graph_data[] = $per_day_attendant->users_count;
            $name[]= $per_day_attendant->name;
            $colours[] = '#' . substr(str_shuffle('ABCDEF0123456789'), 0, 6);
        }

        $chart = new User;
        $chart->labels = ($label);
        $chart->dataset = ($graph_data);
        $chart->colours = $colours;
        $chart->name = ($name);
        // // $usersChart->title('Attendants Registration', 30, "#000", true, 'Helvetica Neue');



        // foreach ($per_day_users as $per_day_attendant) {
        //     $label[] = date('d', strtotime($per_day_attendant->created_at)) . '-' . date('M', strtotime($per_day_attendant->created_at));
        //     $graph_data[] = $per_day_attendant->users_count;
        // }
        // $usersChart->labels($label);
        // $usersChart->dataset('Registrations by date', 'line', $graph_data);
//----------------------------------------End Commented BY T---------------------------
        // chart 1
        // $oneYear = \Carbon\Carbon::today()->subDays(365);
        // $oneMonth = \Carbon\Carbon::today()->subDays(30);
        // $label = [];
        // $graph_data = [];
        // $users_group = Attendant::groupBy(DB::raw("DATE(created_at)"))
        // ->select('created_at', DB::raw("COUNT(id) as users_count"))
        // ->where('created_at', '>=', $oneYear)->orderBy('created_at', 'ASC')->get();
        // foreach($users_group as $label) {
        //     $label[] = date('d', strtotime($label->created_at) ).'/'.date('M', strtotime($label->created_at) );
        //     $graph_data[] = $label->users_count;
        // }
        // $usersChart = new UserChart;
        // $usersChart->title('Users Signup', 30, "#000", true, 'Helvetica Neue');
        // $usersChart->labels($label);
        // $borderColors = [
        //     "rgba(25, 186, 159, 1.0)",
        //     "rgba(34,58,102, 1.0)",
        //     "rgba(255, 205, 86, 1.0)",
        //     "rgba(51,105,232, 1.0)",
        //     "rgba(244,67,54, 1.0)",
        //     "rgba(34,198,246, 1.0)",
        //     "rgba(153, 102, 255, 1.0)",
        //     "rgba(255, 159, 64, 1.0)",
        //     "rgba(233,30,99, 1.0)",
        //     "rgba(205,220,57, 1.0)"
        // ];
        // $fillColors = [
        //     "rgba(25, 186, 159, 0.2)",
        //     "rgba(34,58,102, 0.2)",
        //     "rgba(255, 205, 86, 0.2)",
        //     "rgba(51,105,232, 0.2)",
        //     "rgba(244,67,54, 0.2)",
        //     "rgba(34,198,246, 0.2)",
        //     "rgba(153, 102, 255, 0.2)",
        //     "rgba(255, 159, 64, 0.2)",
        //     "rgba(233,30,99, 0.2)",
        //     "rgba(205,220,57, 0.2)"
        // ];
        // $usersChart->dataset('Last 12 months payments', 'line', $graph_data)
        //     ->color($borderColors)->backgroundcolor($fillColors)->fill(true);
        // $usersChart->displaylegend(true);

        // return view('admin.dashboard', compact('attendants', 'speakers', 'usersChart'));
        // dd('this is here');
        $speakers = Speaker::count();
        $submissions = Submission::count();
        $sessions = Session::count();
        $writers = Writer::count();
        $users = User::count();
        return view('Admin.dashboard',compact('speakers','submissions','sessions','writers','users','chart'));
    }

    public function AllUsers(Request $request)
    {
        $user = User::all();
        $ids = $user->pluck('id')->toArray();
        $templates = Template::where('status', 1)->orderBy('name')->latest()->get();
        if ($request->ajax()) {
            return \DataTables::of($user)
                ->addColumn('selection', function ($row) {
                    return "<input type='checkbox' class='sender' name='user" . $row->id . "' id='user" . $row->id . "' value='" . $row->id . "' >";
                })

                ->addColumn('created_at', function ($row) {
                    return $row->created_at->format('d-M-Y H:i a');
                })
                ->rawColumns(['selection'])
                ->make(true);
        }
        return view('Admin.users', compact('user', 'ids', 'templates'));
    }

    public function AllSubmissions(Request $request)
    {
        $submission = Submission::with('user')->get();

        // dd(Submission::with('user')->get());
        // $ids = $submission->pluck('id')->toArray();
        $templates = Template::where('status', 1)->orderBy('name')->latest()->get();
        // if ($request->ajax()) {
        //     return \DataTables::of($submission)
        //         ->addColumn('selection', function ($row) {
        //             return "<input type='checkbox' class='sender' name='user" . $row->id . "' id='user" . $row->id . "' value='" . $row->id . "' >";
        //         })

        //         ->addColumn('created_at', function ($row) {
        //             return $row->created_at->format('d-M-Y H:i a');
        //         })
        //         ->rawColumns(['selection'])
        //         ->make(true);
        // }
        return view('Admin.submission', compact('submission','templates'));
    }


    // ----------------------------------------Speakers---------------------------------------

    public function speakers()
    {
        $speakers = Speaker::withTrashed()->latest()->get();
        // dd($speakers);
        return view('Admin.Speaker.index', compact('speakers'));
    }

    public function speakerForm()
    {
        return view('Admin.Speaker.create');
    }

    public function speakerCreate(Request $request)
    {
        $request->validate([
            'image' => 'required|file|mimes:png,jpg',
            'ar_name' => 'required|string|max:255',
            'ar_title' => 'required|string|max:255',
            'ar_description' => 'required|string',
            'en_name' => 'required|string|max:255',
            'en_title' => 'required|string|max:255',
            'en_description' => 'required|string',
        ]);
        $speaker = new Speaker();
        $name = md5($request->en_name . time()) . ".{$request->image->getClientOriginalExtension()}";
        $path = $request->image->storeAs("public/speakers", $name);
        $speaker->image = str_replace('public', 'storage', $path);
        $speaker->ar = [
            'name' => $request->ar_name,
            'title' => $request->ar_title,
            'description' => $request->ar_description,
        ];
        $speaker->en = [
            'name' => $request->en_name,
            'title' => $request->en_title,
            'description' => $request->en_description,
        ];

        $speaker->save();
        toastr()->success("Speaker created successfully");

        // $this->sendPasswordEmail($data['email'], $password);
        return redirect()->to(route('admin.speakers'));
    }

    public function editSpeaker($id)
    {
        $speaker = Speaker::findOrFail($id);
        return view('Admin.Speaker.update', compact('speaker'));
    }

    public function updateSpeaker(Request $request, $id)
    {
        $speaker = Speaker::findOrFail($id);
        $request->validate([
            'image' => 'nullable|file|mimes:png,jpg',
            'ar_name' => 'required|string|max:255',
            'ar_title' => 'required|string|max:255',
            'ar_description' => 'required|string',
            'en_name' => 'required|string|max:255',
            'en_title' => 'required|string|max:255',
            'en_description' => 'required|string',
        ]);

        if ($request->has('image')) {
            $url = public_path("/storage/speakers/");
            $parts = explode('/', $speaker->image);
            $url .= end($parts);
            @unlink($url);

            $name = md5($request->en_name . time()) . ".{$request->image->getClientOriginalExtension()}";
            $path = $request->image->storeAs("public/speakers", $name);
            $speaker->image = str_replace('public', 'storage', $path);
        }
        $speaker->ar = [
            'name' => $request->ar_name,
            'title' => $request->ar_title,
            'description' => $request->ar_description,
        ];
        $speaker->en = [
            'name' => $request->en_name,
            'title' => $request->en_title,
            'description' => $request->en_description,
        ];

        $speaker->save();
        toastr()->success("Speaker has successfully updated");
        return redirect()->route('admin.speakers')->with('message','Speaker has successfully updated');
    }

    public function deleteSpeakers(Speaker $speaker)
    {
        $speaker->delete();
        toastr()->success("Speaker deleted successfully");
        return back()->with('message','Speaker deleted successfully');
    }
    public function restoreSpeakers($id)
    {
        $speaker = Speaker::withTrashed()->findOrFail($id);
        $speaker->restore();
        toastr()->success("Speaker restored successfully");
        return back()->with('message','Speaker restored successfully');
    }
    // ----------------------------Writers---------------------------------

    public function writers()
    {
        $writers = Writer::withTrashed()->latest()->get();
        // dd($speakers);
        return view('Admin.Writer.index', compact('writers'));
    }

    public function writerForm()
    {
        return view('Admin.Writer.create');
    }

    public function writerCreate(Request $request)
    {
        $request->validate([
            'image' => 'required|file|mimes:png,jpg',
            'ar_name' => 'required|string|max:255',
            'ar_title' => 'required|string|max:255',
            'ar_description' => 'required|string',
            'en_name' => 'required|string|max:255',
            'en_title' => 'required|string|max:255',
            'en_description' => 'required|string',
        ]);
        $writer = new Writer();
        $name = md5($request->en_name . time()) . ".{$request->image->getClientOriginalExtension()}";
        $path = $request->image->storeAs("public/writers", $name);
        $writer->image = str_replace('public', 'storage', $path);
        $writer->ar = [
            'name' => $request->ar_name,
            'title' => $request->ar_title,
            'description' => $request->ar_description,
        ];
        $writer->en = [
            'name' => $request->en_name,
            'title' => $request->en_title,
            'description' => $request->en_description,
        ];

        $writer->save();
        toastr()->success("Writer created successfully");

        // $this->sendPasswordEmail($data['email'], $password);
        return redirect()->to(route('admin.writers'));
    }

    public function editWriter($id)
    {
        $writer = Writer::findOrFail($id);
        return view('Admin.Writer.update', compact('writer'));
    }

    public function updateWriter(Request $request, $id)
    {
        $writer = Writer::findOrFail($id);
        $request->validate([
            'image' => 'nullable|file|mimes:png,jpg',
            'ar_name' => 'required|string|max:255',
            'ar_title' => 'required|string|max:255',
            'ar_description' => 'required|string',
            'en_name' => 'required|string|max:255',
            'en_title' => 'required|string|max:255',
            'en_description' => 'required|string',
        ]);

        if ($request->has('image')) {
            $url = public_path("/storage/writers/");
            $parts = explode('/', $writer->image);
            $url .= end($parts);
            @unlink($url);

            $name = md5($request->en_name . time()) . ".{$request->image->getClientOriginalExtension()}";
            $path = $request->image->storeAs("public/writers", $name);
            $writer->image = str_replace('public', 'storage', $path);
        }
        $writer->ar = [
            'name' => $request->ar_name,
            'title' => $request->ar_title,
            'description' => $request->ar_description,
        ];
        $writer->en = [
            'name' => $request->en_name,
            'title' => $request->en_title,
            'description' => $request->en_description,
        ];

        $writer->save();
        toastr()->success("Writer has successfully updated");
        return redirect()->route('admin.writers')->with('message','Writer has successfully updated');
    }

    public function deleteWriter(Writer $writer)
    {
        $writer->delete();
        toastr()->success("Writer deleted successfully");
        return back()->with('message','Writer deleted successfully');
    }
    public function restoreWriter($id)
    {
        $writer = Writer::withTrashed()->findOrFail($id);
        $writer->restore();
        toastr()->success("Writer restored successfully");
        return back()->with('message','Writer restored successfully');
    }
    // ----------------------------Sessions---------------------------------

    public function sessions()
    {
        $sessions = Session::withTrashed()->latest()->get();
        // dd($speakers);
        return view('Admin.Session.index', compact('sessions'));
    }

    public function sessionForm()
    {
        return view('Admin.Session.create');
    }

    public function sessionCreate(Request $request)
    {
        $request->validate([
            'image' => 'required|file|mimes:png,jpg',
            'ar_name' => 'required|string|max:255',
            'ar_title' => 'required|string|max:255',
            'ar_description' => 'required|string',
            'en_name' => 'required|string|max:255',
            'en_title' => 'required|string|max:255',
            'en_description' => 'required|string',
        ]);
        $session = new Session();
        $name = md5($request->en_name . time()) . ".{$request->image->getClientOriginalExtension()}";
        $path = $request->image->storeAs("public/sessions", $name);
        $session->image = str_replace('public', 'storage', $path);
        $session->ar = [
            'name' => $request->ar_name,
            'title' => $request->ar_title,
            'description' => $request->ar_description,
        ];
        $session->en = [
            'name' => $request->en_name,
            'title' => $request->en_title,
            'description' => $request->en_description,
        ];

        $session->save();
        toastr()->success("Session created successfully");

        // $this->sendPasswordEmail($data['email'], $password);
        return redirect()->to(route('admin.sessions'));
    }

    public function editSession($id)
    {
        $session = Session::findOrFail($id);
        return view('Admin.Session.update', compact('session'));
    }

    public function updateSession(Request $request, $id)
    {
        $session = Session::findOrFail($id);
        $request->validate([
            'image' => 'nullable|file|mimes:png,jpg',
            'ar_name' => 'required|string|max:255',
            'ar_title' => 'required|string|max:255',
            'ar_description' => 'required|string',
            'en_name' => 'required|string|max:255',
            'en_title' => 'required|string|max:255',
            'en_description' => 'required|string',
        ]);

        if ($request->has('image')) {
            $url = public_path("/storage/writers/");
            $parts = explode('/', $session->image);
            $url .= end($parts);
            @unlink($url);

            $name = md5($request->en_name . time()) . ".{$request->image->getClientOriginalExtension()}";
            $path = $request->image->storeAs("public/writers", $name);
            $session->image = str_replace('public', 'storage', $path);
        }
        $session->ar = [
            'name' => $request->ar_name,
            'title' => $request->ar_title,
            'description' => $request->ar_description,
        ];
        $session->en = [
            'name' => $request->en_name,
            'title' => $request->en_title,
            'description' => $request->en_description,
        ];

        $session->save();
        toastr()->success("Session has successfully updated");
        return redirect()->route('admin.sessions')->with('message','Session has successfully updated');
    }

    public function deleteSession(Session $session)
    {
        $session->delete();
        toastr()->success("Session deleted successfully");
        return back()->with('message','Session deleted successfully');
    }
    public function restoreSession($id)
    {
        $session = Session::withTrashed()->findOrFail($id);
        $session->restore();
        toastr()->success("Session restored successfully");
        return back()->with('message','Session restored successfully');
    }
    // -----------------------Links Admin Panel-------------------------
    public function links()
    {
        $links = MeetingLink::all();
        $active = Setting::firstOrCreate(
            ['title' => 'active_links'],
            ['value' => 0]
        )->value;
        return view('Admin.links', compact('links', 'active'));
    }

    public function toggleLink()
    {
        $active = Setting::where('title', 'active_links')->first();
        $active->update(['value' => $active->value == 1 ? 0 : 1]);
        toastr()->success('Links active status changed successfully');
        return back();
    }

    public function linkCreate(Request $request)
    {
        MeetingLink::create($request->all());
        toastr()->success('Meeting Link has successfuly added');
        return back();
    }

    public function linkUpdate(Request $request, $id)
    {
        $data = $request->all();
        MeetingLink::find($id)->update($data);
        toastr()->success('Meeting Link has successfuly updated');
        return back();
    }

    // ---------------------------------------------Agenda -----------------------------------


    public function agenda()
    {
        $agendas = Agenda::withTrashed()->latest()->get();
        return view('Admin.Agenda.index', compact('agendas'));
    }

    public function agendaAdd()
    {
        return view('Admin.Agenda.create');
    }

    public function agendaCreate(Request $request) {
        $data = $request->all();
        Agenda::create($data);
            toastr()->success('Agenda has successfuly created');
            return redirect(route('admin.agenda.index'));

    }
    public function agendaEdit($id){
        $agendaData = Agenda::findorFail($id);
        // dd($agendaData);
        return view('Admin.Agenda.update',compact('agendaData'));
    }
    public function agendaUpdate(Request $request,$id){
        $agendaData = Agenda::findorFail($id);
        if($request->type=="Break"){
            $agendaData->day = $request->day;
            $agendaData->date = $request->date;
            $agendaData->start = $request->start;
            $agendaData->end = $request->end;
            $agendaData->type = $request->type;
            $agendaData->title_ar = NULL;
            $agendaData->title_en = NULL;
            $agendaData->topic_en = NULL;
            $agendaData->topic_ar = NULL;
            $agendaData->desc_en = NULL;
            $agendaData->desc_ar = NULL;
            $agendaData->save();
            toastr()->success('Agenda has successfuly updated');
            return redirect(route('admin.agenda.index'));
        }
        if($request->type=="Session"){
            $agendaData->day = $request->day;
            $agendaData->date = $request->date;
            $agendaData->start = $request->start;
            $agendaData->end = $request->end;
            $agendaData->type = $request->type;
            $agendaData->title_ar = $request->title_ar;
            $agendaData->title_en = $request->title_en;
            $agendaData->topic_en = $request->topic_en;
            $agendaData->topic_ar = $request->topic_ar;
            $agendaData->desc_en = $request->desc_en;
            $agendaData->desc_ar = $request->desc_ar;
            $agendaData->save();
            toastr()->success('Agenda has successfuly updated');
            return redirect(route('admin.agenda.index'));
        }
            $agendaData->day = $request->day;
            $agendaData->date = $request->date;
            $agendaData->start = $request->start;
            $agendaData->end = $request->end;
            $agendaData->type = $request->type;
            $agendaData->title_ar = NULL;
            $agendaData->title_en = NULL;
            $agendaData->topic_en = $request->topic_en;
            $agendaData->topic_ar = $request->topic_ar;
            $agendaData->desc_en = $request->desc_en;
            $agendaData->desc_ar = $request->desc_ar;
            $agendaData->save();
            toastr()->success('Agenda has successfuly updated');
            return redirect(route('admin.agenda.index'));
    }
    public function deleteAgenda(Agenda $agenda)
    {
        $agenda->delete();
        toastr()->success("Agenda deleted successfully");
        return back()->with('message','Agenda deleted successfully');
    }
    public function restoreAgenda($id)
    {
        $agenda = Agenda::withTrashed()->findOrFail($id);
        $agenda->restore();
        toastr()->success("Agenda restored successfully");
        return back()->with('message','Agenda restored successfully');
    }
}
