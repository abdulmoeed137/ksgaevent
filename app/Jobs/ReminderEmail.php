<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\User;
use App\Models\Template;

class ReminderEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $user = User::all('email')->pluck('email')->toArray();
        $template = Template::where('name','Reminder')->first();
        $data = array('subject'=>$template->subject,'content'=>$template->content);
        \Mail::send('Admin.emails.template',$data,function($message) use ($user){
            foreach($user as $item){
                $message->to($item);
            }
        });

    }
}
