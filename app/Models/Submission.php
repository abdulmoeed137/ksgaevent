<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Submission extends Model
{
    use HasFactory;
    protected $fillable = ['name','email','organization','conferencetopic','phonenumber','titleofresearch','country','city','fileupload','user_id','status'];

    
    public function user(){
        return $this->belongsTo(User::class);
    }
}
