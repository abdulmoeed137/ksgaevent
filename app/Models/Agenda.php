<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Agenda extends Model
{
    use HasFactory, SoftDeletes;
    protected $fillable = ['day','date','start','end','type','topic_en','topic_ar','desc_en','desc_ar','title_en','title_ar'];
}
