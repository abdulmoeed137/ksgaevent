<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="{{ app()->getLocale() == 'en' ? 'ltr' : 'rtl' }}">

<!-- index-mp-layout309:18-->

<head>

    <!-- Meta Tags -->
    <meta name="viewport" content="width=device-width,initial-scale=1.0" />
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta name="description" content="LearnPress | Education & Courses HTML Template" />
    <meta name="keywords" content="academy, course, education, education html theme, #, learning," />


    <!-- Page Title -->
    <title>KSGA</title>

    <!-- Favicon and Touch Icons -->
    <link href="{{asset('images/favicon.png')}}" rel="shortcut icon" type="image/png">

    <!-- Stylesheet -->
    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/jquery-ui.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/animate.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/css-plugin-collections.css')}}" rel="stylesheet" />
    <!-- CSS | menuzord megamenu skins -->
    <link id="menuzord-menu-skins" href="{{asset('css/menuzord-skins/menuzord-rounded-boxed.css')}}" rel="stylesheet" />
    <!-- CSS | Main style file -->
    <link href="{{asset('css/style-main.css')}}" rel="stylesheet" type="text/css">
    <!-- CSS | Preloader Styles -->
    <link href="{{asset('css/preloader.css')}}" rel="stylesheet" type="text/css">
    <!-- CSS | Custom Margin Padding Collection -->
    <link href="{{asset('css/custom-bootstrap-margin-padding.css')}}" rel="stylesheet" type="text/css">
    <!-- CSS | Responsive media queries -->
    <link href="{{asset('css/responsive.css')}}" rel="stylesheet" type="text/css">
    <!-- CSS | Style css. This is the file where you can place your own custom css code. Just uncomment it and use it. -->
    <!-- <link href="css/style.css" rel="stylesheet" type="text/css"> -->

    <!-- Revolution Slider 5.x CSS settings -->
    <link href="{{asset('js/revolution-slider/css/settings.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('js/revolution-slider/css/layers.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('js/revolution-slider/css/navigation.css')}}" rel="stylesheet" type="text/css" />

    <!-- CSS | Theme Color -->
    <link href="{{asset('css/colors/theme-skin-color-set-1.css')}}" rel="stylesheet" type="text/css">
    <!--------------------Toaster Message--------------->
    {{-- <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-
     alpha/css/bootstrap.css" rel="stylesheet"> --}}

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    {{-- <link rel="stylesheet" type="text/css"
        href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css"> --}}

    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

    <!--------------------End Toaster Message--------------->
    <!-- external javascripts -->
    <script src="{{asset('js/jquery-2.2.4.min.js')}}"></script>
    <script src="{{asset('js/jquery-ui.min.js')}}"></script>
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <!-- JS | jquery plugin collection for this theme -->
    <script src="{{asset('js/jquery-plugin-collection.js')}}"></script>

    <!-- Revolution Slider 5.x SCRIPTS -->
    <script src="{{asset('js/revolution-slider/js/jquery.themepunch.tools.min.js')}}"></script>
    <script src="{{asset('js/revolution-slider/js/jquery.themepunch.revolution.min.js')}}"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body class="">
    <div id="wrapper" class="clearfix">
        <!-- preloader -->
        {{-- <div id="preloader">
            <div id="spinner">
                <img alt="" src="{{asset('images/preloaders/11.gif')}}">
            </div>
        </div> --}}

        <!-- Header -->
        <header id="header" class="header">

            <div class="header-nav">
                <div class="header-nav-wrapper navbar-scrolltofixed bg-white">
                    <div class="container">
                        <nav id="menuzord-right" class="menuzord default">
                            <a class="menuzord-brand pull-left flip" href="{{route('home')}}">
                                <img src="{{asset('images/logo-wide.png')}}" alt="" width="400">
                            </a>
                            <ul class="menuzord-menu">
                                <li class="{{ request()->is('/*') ? 'active' : '' }}"><a href="{{route('home')}}">{{__('Home')}}</a>
                                </li>
                                <li class="{{ request()->is('') ? 'active' : '' }}"><a>{{__('Academy')}}</a>
                                </li>


                                @guest
                                @if (Route::has('register'))

                                <li class="{{ request()->is('register*') ? 'active' : '' }}" ><a href="{{route('register')}}">{{__('Registration')}}</a>

                                </li>
                                @endif
                                @if (Route::has('login'))
                                <li class="{{ request()->is('login*') ? 'active' : '' }}"><a href="{{route('login')}}">{{__('Log In')}}</a>

                                </li>
                                @endif
                                @else
                                <li class="{{ request()->is('submission*') ? 'active' : '' }}">

                                        <a href="{{route('submissionform')}}">{{__('Submission')}}
                                        </a>
                                </li>
                                <li class="{{ request()->is('dashboard*') ? 'active' : '' }}">

                                        <a href="{{route('dashboard')}}">{{__('Dashboard')}}
                                        </a>
                                </li>

                                <li class="">
                                <!-- <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }}
                                </a> -->


                                    <a href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>

                            </li>

                                @endguest
                                <li class="cl-han">
                                    <a onClick="document.getElementById('lang').submit();">{{ \App::getLocale()=='en'?'AR':'EN' }}</a>
                                <form id="lang" method="POST" action="{{ route('lang') }}">@csrf</form>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </header>

        <!-- Start main-content -->

        <!-- end main-content -->

        @yield('content')

        <!-- Section: inner-header -->


        <!-- Section: Schedule -->

        <!-- Footer -->
        <footer id="footer" class="footer bg-black-222" data-bg-img="{{asset('images/footer-bg.png')}}">
            <div class="container pt-70 pb-40">
                <div class="row foot-row">
                    <div class="col-sm-6 col-md-4 col-sm-12 col-xs-12">
                        <div class="widget dark">
                            <img class="mt-10 mb-15" alt="" src="{{asset('images/logo-wide.png')}}" width="500">
                            {{-- <p class="font-16 mb-10">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Similique, veniam eligendi
                                aliquam est harum .
                            </p> --}}
                            <ul class="styled-icons icon-dark mt-20">
                                <li class="wow fadeInLeft" data-wow-duration="1.5s" data-wow-delay=".1s"
                                    data-wow-offset="10"><a href="#" data-bg-color="#3B5998"><i
                                            class="fa fa-facebook"></i></a></li>
                                <li class="wow fadeInLeft" data-wow-duration="1.5s" data-wow-delay=".2s"
                                    data-wow-offset="10"><a href="#" data-bg-color="#02B0E8"><i
                                            class="fa fa-twitter"></i></a></li>


                                <li class="wow fadeInLeft" data-wow-duration="1.5s" data-wow-delay=".5s"
                                    data-wow-offset="10"><a href="#" data-bg-color="#C22E2A"><i
                                            class="fa fa-youtube"></i></a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-sm-6 col-md-4 col-sm-12 col-xs-12">
                        <div class="widget dark">
                            <h5 class="widget-title line-bottom">{{__('Useful Links')}}</h5>
                            <ul class="list angle-double-right list-border">
                                <li><a href="{{route('register')}}">{{__('Registration')}}</a></li>
                                <li><a href="{{route('submissionform')}}">{{__('Submission')}}</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3 col-sm-12 col-xs-12">
                        <div class="widget dark">
                            <h5 class="widget-title line-bottom">{{__('Quick Contact')}}</h5>
                            <ul class="list-border">
                                <li><a href="#">+(012) 345 6789</a></li>
                                <li><a href="#">hello@yourdomain.com</a></li>
                                <!-- <li><a href="#" class="lineheight-20">121 King Street, Melbourne Victoria 3000, Australia</a></li> -->
                            </ul>
                            {{-- <p class="font-16 text-white mb-5 mt-15">{{__('Subscribe to our newsletter')}}</p>
                            <form id="footer-mailchimp-subscription-form" class="newsletter-form mt-10">
                                <label class="display-block" for="mce-EMAIL"></label>
                                <div class="input-group">
                                    <input type="email" value="" name="EMAIL" placeholder="{{__('Your Email')}}"
                                        class="form-control" data-height="37px" id="mce-EMAIL">
                                    <span class="input-group-btn">
                                        <button type="submit" class="btn btn-colored btn-theme-colored m-0"><i
                                                class="fa fa-paper-plane-o text-white"></i></button>
                                    </span>
                                </div>
                            </form> --}}


                        </div>
                    </div>
                </div>
            </div>

        </footer>
        {{-- <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a> --}}
    </div>
  </footer>
  {{-- <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a> --}}
</div>


<a id="button"></a>

        <a href="{{route('register')}}"  class="reg login btn btn-lg btn-link button-glow scrollToTop2 {{ request()->is('register*') ? 'd-none' : ''  }}">{{__('Register')}}</a>

<!-- end wrapper -->

    <!-- Footer Scripts -->
    <!-- JS | Custom script for all pages -->
    <script src="{{asset('js/custom.js')}}"></script>

    <!-- SLIDER REVOLUTION 5.0 EXTENSIONS
      (Load Extensions only on Local File Systems !
       The following part can be removed on Server for On Demand Loading) -->
    <script type="text/javascript"
        src="{{asset('js/revolution-slider/js/extensions/revolution.extension.actions.min.js')}}"></script>
    <script type="text/javascript"
        src="{{asset('js/revolution-slider/js/extensions/revolution.extension.carousel.min.js')}}"></script>
    <script type="text/javascript"
        src="{{asset('js/revolution-slider/js/extensions/revolution.extension.kenburn.min.js')}}"></script>
    <script type="text/javascript"
        src="{{asset('js/revolution-slider/js/extensions/revolution.extension.layeranimation.min.js')}}"></script>
    <script type="text/javascript"
        src="{{asset('js/revolution-slider/js/extensions/revolution.extension.migration.min.js')}}"></script>
    <script type="text/javascript"
        src="{{asset('js/revolution-slider/js/extensions/revolution.extension.navigation.min.js')}}"></script>
    <script type="text/javascript"
        src="{{asset('js/revolution-slider/js/extensions/revolution.extension.parallax.min.js')}}"></script>
    <script type="text/javascript"
        src="{{asset('js/revolution-slider/js/extensions/revolution.extension.slideanims.min.js')}}"></script>
    <script type="text/javascript"
        src="{{asset('js/revolution-slider/js/extensions/revolution.extension.video.min.js')}}"></script>

    <script>

    // Set the date we're counting down to
    var countDownDate = new Date("May 25, 2022 15:37:25").getTime();


    // Update the count down every 1 second
    var x = setInterval(function() {

        // Get today's date and time
        var now = new Date().getTime();

        // Find the distance between now and the count down date
        var distance = countDownDate - now;

        // Time calculations for days, hours, minutes and seconds
        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);

        // Output the result in an element with id="demo"
        document.getElementById("demo").innerHTML = days + " : " + hours + " : " +
            minutes + " : " + seconds + "";
        // If the count down is over, write some text
        if (distance < 0) {
            clearInterval(x);
            document.getElementById("demo").innerHTML = "Expired";
            // document.getElementById("demo").innerHTML = `<iframe style="display: block;" width="100%" height="450px;" src="https://www.youtube.com/watch?v=BLl32FvcdVM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>`;

        }
    }, 1000);
    </script>

<script>

    // Set the date we're counting down to
    var countDownDate = new Date("May 25, 2022 15:37:25").getTime();


    // Update the count down every 1 second
    var x = setInterval(function() {

        // Get today's date and time
        var now = new Date().getTime();

        // Find the distance between now and the count down date
        var distance = countDownDate - now;

        // Time calculations for days, hours, minutes and seconds
        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);

        // Output the result in an element with id="demo"
        document.getElementById("demo1").innerHTML = days + " : " + hours + " : " +
            minutes + " : " + seconds + "";
            // element.getElementsByClassName("iframediv").style.display : 'none';

        // If the count down is over, write some text
        if (distance < 0) {
            clearInterval(x);
            document.getElementById("demo1").innerHTML = "Expired";
            // element.getElementsByClassName("demo1div").style.display : 'none';
            // element.getElementsByClassName("iframediv").style.display : 'block';
        }
    }, 1000);
    </script>
    <script>
        var btn = $('#button');

$(window).scroll(function() {
  if ($(window).scrollTop() > 300) {
    btn.addClass('show');
  } else {
    btn.removeClass('show');
  }
});

btn.on('click', function(e) {
  e.preventDefault();
  $('html, body').animate({scrollTop:0}, '300');
});

    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.matchHeight/0.7.2/jquery.matchHeight-min.js">

    </script>
    <script>
        $(document).ready(function(){
    $('.match').matchHeight();
})
    </script>
    <script>
        $(document).on('click','#checklang',function(){
            alert("")
        })
    </script>
    <script>
        const countdown = () => {
  const countDate = new Date('May 10, 2022 00:00:00').getTime();
  const now = new Date().getTime();
  const gap = countDate - now;

  const second = 1000,
    minute = second * 60,
    hour = minute * 60,
    day = hour * 24;

  const textDay = Math.floor(gap / day),
    textHour = Math.floor((gap % day) / hour),
    textMinute = Math.floor((gap % hour) / minute),
    textSecond = Math.floor((gap % minute) / second);

  document.querySelector('.day').innerText = textDay;
  document.querySelector('.hour').innerText = textHour;
  document.querySelector('.minute').innerText = textMinute;
  document.querySelector('.second').innerText = textSecond;
};

setInterval(countdown, 1000);

    </script>
    <script>
        var countDownDate = new Date("May 25, 2022 15:37:25").getTime();
        // var countDownDate = new Date("March 10, 2022 12:37:25").getTime();

// Update the count down every 1 second
const lang = "{{ app()->getLocale() }}"
var x = setInterval(function () {

    // Get today's date and time
    var now = new Date().getTime();

    // Find the distance between now and the count down date
    var distance = countDownDate - now;

    // Time calculations for days, hours, minutes and seconds
    var days = (("0" + Math.floor(distance / (1000 * 60 * 60 * 24))).slice(-2)).split("");
    var hours = (("0" + Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60))).slice(-2)).split("");
    var minutes = (("0" + Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60))).slice(-2)).split("");
    var seconds = (("0" + Math.floor((distance % (1000 * 60)) / 1000)).slice(-2)).split("");

    // Output the result in an element with id="demo"
    document.getElementById("days").innerHTML = `<span class="${lang}-${days[0]}"></span><span class="${lang}-${days[1]}"></span>`;
    document.getElementById("hours").innerHTML = `<span class="${lang}-${hours[0]}"></span><span class="${lang}-${hours[1]}"></span>`;
    document.getElementById("minutes").innerHTML = `<span class="${lang}-${minutes[0]}"></span><span class="${lang}-${minutes[1]}"></span>`;
    document.getElementById("seconds").innerHTML = `<span class="${lang}-${seconds[0]}"></span><span class="${lang}-${seconds[1]}"></span>`;
    // If the count down is over, write some text
    // element.getElementsByClassName("aftercountdownbutton").style.display : 'none';


    if (distance < 0) {
        clearInterval(x);
        document.getElementById("countdown").innerHTML = "Expired";
        // element.getElementsByClassName("countdown").style.display : 'none';
        // element.getElementsByClassName("aftercountdownbutton").style.display : 'block';

        // document.getElementById("demo").innerHTML = `<iframe style="display: block;" width="100%" height="450px;" src="https://www.youtube.com/watch?v=BLl32FvcdVM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>`;
    }
}, 1000);

    </script>

    <script>
document.getElementById("aftercountdownbutton").style.display : 'none';

    </script>
</body>

<!-- index-mp-layout309:18-->

</html>
