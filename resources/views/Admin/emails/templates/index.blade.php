@extends('layouts.admin')

@section('custom-css')
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- DataTables -->
    <link rel="stylesheet"
        href="{{ URL::asset('admin-assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet"
        href="{{ URL::asset('admin-assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/2.0.1/css/buttons.dataTables.min.css">
@endsection

@section('section-content')
    <style>
        /* btn animation  */
        .custom-btn {
            color: #0abc9c !important;
            border-radius: 5px;
            padding: 9px 55px;
            font-family: 'Lato', sans-serif;
            font-weight: 500;
            background: transparent;
            cursor: pointer;
            transition: all 0.3s ease;
            position: relative;
            display: inline-block;
            /* box-shadow: inset 2px 2px 2px 0px rgb(255 255 255 / 50%), 7px 7px 20px 0px rgb(0 0 0 / 10%), 4px 4px 5px 0px rgb(0 0 0 / 10%); */
            outline: none;
            text-decoration: none;
            border: 1px solid #0abc9c !important;
        }


        /* 10 */
        .btn-10 {
            background: rgb(22, 9, 240);
            background: linear-gradient(0deg, rgb(255 255 255) 0%, rgb(255 255 255) 100%);
            color: #fff;
            border: none;
            transition: all 0.3s ease;
            overflow: hidden;
        }

        .btn-10:after {
            position: absolute;
            content: " ";
            top: 0;
            left: 0;
            z-index: -1;
            width: 100%;
            height: 100%;
            transition: all 0.3s ease;
            -webkit-transform: scale(.1);
            transform: scale(.1);
        }

        .btn-10:hover {
            color: #fff !important;
            border: none;
            background: transparent;
            text-decoration: none;
        }

        .btn-10:hover:after {
            background: #075995;
            -webkit-transform: scale(1);
            transform: scale(1);
            color: white !important;
        }

    </style>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Email Templates</h1>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container">
            <div class="card">
                <div class="card-header">
                    <div class="d-flex justify-content-between align-items-center">
                        <h3 class="card-title">All Email Templates</h3>
                        <a href="{{ route('admin.emails.templates.create') }}" style="z-index: 99999999;"
                            class="send ml-1 custom-btn btn-10">Create Template</a>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <table id="example2" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Subject</th>
                                <th>Active</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($templates as $template)
                                <tr>
                                    <td>{{ $template->id }}</td>
                                    <td>{{ $template->name }}</td>
                                    <td>{{ $template->subject }}</td>
                                    <td>{{ $template->status == 1 ? 'Active' : 'Deactive' }}</td>
                                    <td>
                                        <a style="z-index: 99999999;" class="send ml-1 custom-btn btn-10"
                                            href="{{ route('admin.emails.templates.edit', $template->id) }}">
                                            <i class="fas fa-edit"></i> Edit
                                        </a>
                                        @if ($template->status == 0)
                                            <a style="z-index: 9999;" class="status ml-1 custom-btn btn-10" href="#"
                                                data-status="1" data-template="{{ $template->id }}">
                                                <i class="fas fa-check"></i> Activate
                                            </a>
                                        @else
                                            <a style="z-index: 9999;" class="status ml-1 custom-btn btn-10" href="#"
                                                data-status="0" data-template="{{ $template->id }}">
                                                <i class="fas fa-times"></i> Deactivate
                                            </a>
                                        @endif
                                        <a style="z-index: 9999;" class="send ml-1 custom-btn btn-10"
                                            href="{{ route('admin.templates.show', $template->id) }}" target="_blank">
                                            <i class="fas fa-eye"></i> View
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tfoot>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection

@section('custom-script')
    <!-- DataTables -->
    <script src="{{ URL::asset('admin-assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ URL::asset('admin-assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ URL::asset('admin-assets/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}">
    </script>
    <script src="{{ URL::asset('admin-assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}">
    </script>
    <script src="https://cdn.datatables.net/buttons/2.0.1/js/dataTables.buttons.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.0.1/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.0.1/js/buttons.colVis.min.js"></script>
    <link href="plugins/WYSIWYG/editor.css" type="text/css" rel="stylesheet" />
    <script src="plugins/WYSIWYG/editor.js"></script>
    <script>
        $(document).ready(function() {
            $('#example2').DataTable({
                dom: 'Bfrtip',
                buttons: [{
                        extend: 'excelHtml5',
                        title: 'Email Templates',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    {
                        extend: 'pdfHtml5',
                        title: 'Email Templates',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    'colvis'
                ]
            });

            $("a.status").on("click", function(e) {
                // alert('tahmeer');
                e.preventDefault()
                const _this = $(e.target)
                const formData = new FormData()
                formData.append("status", _this.data("status"))
                formData.append("_token", "{{ csrf_token() }}")
                formData.append("_method", "PUT")

                let url = "{{ route('admin.emails.templates.status', 't_id') }}"
                url = url.replace("t_id", _this.data("template"))
                $.ajax({
                    method: "POST",
                    url: url,
                    data: formData,
                    processData: false,
                    contentType: false,
                }).done(function(res) {
                    if (res.hasOwnProperty("message")) {
                        toastr.success(res.message, {
                            extendedTimeOut: 0,
                            timeOut: 800
                        })
                        setTimeout(function() {
                            window.location.reload();
                        }, 1000);
                    }
                }).fail(function(error) {
                    console.log(error.status);
                })
            })
        });

        function changeActive(e, status, id) {
            e.preventDefault();
            const formData = new FormData()


            let url = "{{ route('admin.questions.status', 'q_id') }}"
            url = url.replace("q_id", id)
            $(function() {
                $.ajax({
                    method: "POST",
                    url: url,
                    data: formData,
                    processData: false,
                    contentType: false,
                }).done(function(res) {
                    if (res.hasOwnProperty("message")) {
                        toastr.success(res.message, {
                            extendedTimeOut: 0,
                            timeOut: 800
                        })
                        setTimeout(function() {
                            window.location.reload();
                        }, 1000);
                    }
                }).fail(function(error) {
                    console.log(error.status);
                })
            });
        }
    </script>
@endsection
