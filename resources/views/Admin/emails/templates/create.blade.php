@extends('layouts.admin')

@section('custom-css')
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.css" rel="stylesheet">
@endsection

@section('section-content')
<style>

  /* btn animation  */
  .custom-btn {
    color: #0abc9c !important;
    border-radius: 5px;
    padding: 9px 55px;
    font-family: 'Lato', sans-serif;
    font-weight: 500;
    background: transparent;
    cursor: pointer;
    transition: all 0.3s ease;
    position: relative;
    display: inline-block;
    /* box-shadow: inset 2px 2px 2px 0px rgb(255 255 255 / 50%), 7px 7px 20px 0px rgb(0 0 0 / 10%), 4px 4px 5px 0px rgb(0 0 0 / 10%); */
    outline: none;
	text-decoration: none;
    border: 1px solid #0abc9c !important;
  }


/* 10 */
.btn-10 {
    background: rgb(22,9,240);
    background: linear-gradient(0deg, rgb(255 255 255) 0%, rgb(255 255 255) 100%);
	color: #fff;
	border: none;
	transition: all 0.3s ease;
	overflow: hidden;
  }
  .btn-10:after {
	position: absolute;
	content: " ";
	top: 0;
	left: 0;
	z-index: -1;
	width: 100%;
	height: 100%;
	transition: all 0.3s ease;
	-webkit-transform: scale(.1);
	transform: scale(.1);
  }
  .btn-10:hover {
	color: #fff !important;
	border: none;
	background: transparent;
	text-decoration: none;
  }
  .btn-10:hover:after {
	background:#075995;
	-webkit-transform: scale(1);
	transform: scale(1);
	color: white !important;
  }

</style>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Email Templates</h1>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container">
            <div class="card">
                <div class="card-header">
                    <div class="d-flex">
                        <h3 class="card-title">Create Email Templates</h3>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <form id="temp-form">
                        @csrf
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" name="name" id="name" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="subject">Subject</label>
                            <input type="text" name="subject" id="subject" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="content">Content</label>
                            <textarea id="content" name="content"></textarea>
                        </div>
                    <div class="col-md-12 text-right">
                    <button type="submit" style="z-index: 99999999;" class="send ml-1 custom-btn btn-10">Save</button>
                    </div>
                    </form>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection

@section('custom-script')
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.js"></script>

    <script>
        $(document).ready(function() {
            $("#content").summernote({
                height: 500,
                disableResizeEditor: true,
            })

            $("#temp-form").on("submit", function(e) {
                e.preventDefault();
                const formData = new FormData(e.target)
                $.ajax({
                    method: "POST",
                    url: "{{ route('admin.emails.templates.store') }}",
                    data: formData,
                    processData: false,
                    contentType: false,
                }).done(function(res) {
                    if (res.hasOwnProperty("message")) {
                        toastr.success(res.message, {
                            extendedTimeOut: 0,
                            timeOut: 800
                        })
                        setTimeout(function() {
                            window.location.replace(
                                "{{ route('admin.emails.templates.index') }}");
                        }, 1000);
                    }
                }).fail(function(error) {
                    if (error.status == "422") {
                        setErrors(error.responseJSON.errors)
                    }
                })
            })

            function setErrors(errors) {
                for (const [field, message] of Object.entries(errors)) {
                    $(`#${field}`).addClass("is-invalid")
                    $(`<div class="invalid-feedback" role="alert">
                        <strong>${message[0]}</strong>
                    </div>`).insertAfter(`#${field}`);
                }
            }
        });
    </script>
@endsection
