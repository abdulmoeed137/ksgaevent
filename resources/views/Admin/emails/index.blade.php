@extends('layouts.admin')

@section('custom-css')
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- DataTables -->
    <link rel="stylesheet"
        href="{{ URL::asset('admin-assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet"
        href="{{ URL::asset('admin-assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/2.0.1/css/buttons.dataTables.min.css">
@endsection

@section('section-content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Email</h1>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">All Emails</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <table id="example2" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Receiver</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($emails as $email)
                                <tr>
                                    <td>{{ $email->id }}</td>
                                    <td>
                                        <p>Name: {{ $email->receiver->name }}</p>
                                        <p>Email: {{ $email->receiver->email }}</p>
                                    </td>
                                    <td>{{ $email->status[1] }}</td>
                                </tr>
                            @endforeach
                            </tfoot>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection

@section('custom-script')
    <!-- DataTables -->
    <script src="{{ URL::asset('admin-assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ URL::asset('admin-assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ URL::asset('admin-assets/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ URL::asset('admin-assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
    <script src="https://cdn.datatables.net/buttons/2.0.1/js/dataTables.buttons.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.0.1/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.0.1/js/buttons.colVis.min.js"></script>
    <link href="plugins/WYSIWYG/editor.css" type="text/css" rel="stylesheet" />
    <script src="plugins/WYSIWYG/editor.js"></script>
    <script>
        $(document).ready(function() {
            $('#example2').DataTable({
                dom: 'Bfrtip',
                buttons: [{
                        extend: 'excelHtml5',
                        title: 'Emails',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    {
                        extend: 'pdfHtml5',
                        title: 'Emails',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    'colvis'
                ]
            });

            $("a.status").on("click", function(e) {
                e.preventDefault()
                const _this = $(e.target)
                const formData = new FormData()
                formData.append("status", _this.data("status"))
                formData.append("_token", "{{ csrf_token() }}")
                formData.append("_method", "PUT")

                let url = ""
                url = url.replace("t_id", _this.data("email"))
                $.ajax({
                    method: "POST",
                    url: url,
                    data: formData,
                    processData: false,
                    contentType: false,
                }).done(function(res) {
                    if (res.hasOwnProperty("message")) {
                        toastr.success(res.message, {
                            extendedTimeOut: 0,
                            timeOut: 800
                        })
                        setTimeout(function() {
                            window.location.reload();
                        }, 1000);
                    }
                }).fail(function(error) {
                    console.log(error.status);
                })
            })
        });

        function changeActive(e, status, id) {
            e.preventDefault();
            const formData = new FormData()


            let url = ""
            url = url.replace("q_id", id)
            $(function() {
                $.ajax({
                    method: "POST",
                    url: url,
                    data: formData,
                    processData: false,
                    contentType: false,
                }).done(function(res) {
                    if (res.hasOwnProperty("message")) {
                        toastr.success(res.message, {
                            extendedTimeOut: 0,
                            timeOut: 800
                        })
                        setTimeout(function() {
                            window.location.reload();
                        }, 1000);
                    }
                }).fail(function(error) {
                    console.log(error.status);
                })
            });
        }
    </script>
@endsection
