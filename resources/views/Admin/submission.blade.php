@extends('layouts.admin')

@section('custom-css')
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- DataTables -->
    <link rel="stylesheet"
        href="{{ URL::asset('admin-assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet"
        href="{{ URL::asset('admin-assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/2.0.1/css/buttons.dataTables.min.css">
@endsection
<style>
    /* btn animation  */
    .table {
        width: max-content !important;
    }

    .custom-btn {
        color: #ffffff !important;
        border-radius: 5px;
        padding: 12px 55px;
        font-family: 'Lato', sans-serif;
        font-weight: 500;
        background: #153E58 !important;
        cursor: pointer;
        transition: all 0.3s ease;
        position: relative;
        display: inline-block;
        /* box-shadow: inset 2px 2px 2px 0px rgb(255 255 255 / 50%), 7px 7px 20px 0px rgb(0 0 0 / 10%), 4px 4px 5px 0px rgb(0 0 0 / 10%); */
        outline: none;
        text-decoration: none;
        width: 100%;
    }


    /* 10 */
    .btn-10 {
        background: rgb(22, 9, 240);
        background: linear-gradient(0deg, rgb(255 255 255) 0%, rgb(255 255 255) 100%);
        color: #fff;
        border: none;
        transition: all 0.3s ease;
        overflow: hidden;
    }

    .btn-10:after {
        position: absolute;
        content: " ";
        top: 0;
        left: 0;
        z-index: -1;
        width: 100%;
        height: 100%;
        transition: all 0.3s ease;
        -webkit-transform: scale(.1);
        transform: scale(.1);
    }

    .btn-10:hover {
        color: #fff !important;
        background: transparent;
        text-decoration: none;
    }

    .btn-10:hover:after {
        background: #F5CC44;
        -webkit-transform: scale(1);
        transform: scale(1);
        color: white !important;

    }

    .content-header h1 {
        font-size: 1.8rem !important;
        margin: 0;
    }

    .card-title {
        font-size: 19px !important;
    }

    .pagination>.active>a,
    .pagination>.active>a:focus,
    .pagination>.active>a:hover,
    .pagination>.active>span,
    .pagination>.active>span:focus,
    .pagination>.active>span:hover {
        z-index: 3;
        color: #fff;
        cursor: default;
        background-color: #153E58 !important;
        border-color: #153E58 !important;
    }
    table th {
        font-weight: 100 !important;
    }
</style>
@section('section-content')
    <div class="col-md-12">
        {{-- <div class="row mt-5">
            <div class="col-md-9">
                <h3>All Submissions</h3>
            </div>
            <div class="col-md-3">
                <button type="button" style="z-index: 99999999;" class="send ml-1 custom-btn btn-10 mt-3" data-toggle="modal"
                    data-target="#modal-default" disabled>Send Email</button>
            </div>
        </div> --}}
        <!-- Content Header (Page header) -->
        {{-- <section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Submissions</h1>
            </div>
        </div>
    </div>
</section> --}}

        <!-- Main content -->
        <section class="content mt-5">
            <div class="row">
                <div class="col-12">
                    <div class="card mt-3">
                        <div class="card-header">
                            <div class="d-flex justify-content-between align-items-center">
                                <h3 class="card-title">All Submissions</h3>
                                <div class="d-flex justify-content-between align-item-center">
                                    <button type="button" style="z-index: 99999999;" class="send ml-1 custom-btn btn-10"
                                        data-toggle="modal" data-target="#modal-default" disabled>Send Email</button>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="example2" class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Organization</th>
                                            <th>Conference Topic</th>
                                            <th>Phone Number</th>
                                            <th>Title Of Research</th>
                                            <th>Country</th>
                                            <th>City</th>
                                            <th>Submitted By</th>
                                            <th>Created At</th>
                                            <th>Status</th>
                                            <th>File</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($submission as $submission)
                                            <tr>

                                                <td>{{ $submission->id }}</td>
                                                <td>{{ $submission->name }}</td>
                                                <td>{{ $submission->email }}</td>
                                                <td>{{ $submission->organization }}</td>
                                                <td>{{ $submission->conferencetopic }}</td>
                                                <td>{{ $submission->phonenumber }}</td>
                                                <td>{{ $submission->titleofresearch }}</td>
                                                <td>{{ $submission->country }}</td>
                                                <td>{{ $submission->city }}</td>
                                                <td>{{ $submission->user->name }}</td>
                                                <td>{{ $submission->created_at }}</td>
                                                <td>{{ $submission->status }}</td>
                                                <td>
                                                    <a href="{{route('admin.submissions.image',$submission->fileupload)}}" target="_blank">
                                                        <i class="fa fa-download" aria-hidden="true"></i>
                                                    </a>
                                                </td>
                                                <td>

                                                    @if ($submission->status == 'accepted')
                                                        <a href="#" value="{{$submission->id}}" class="btn btn-info" disabled>Accepted</a>
                                                    @elseif($submission->status == 'reject')
                                                        <a href="#" value="{{$submission->id}}" class="btn btn-danger" disabled>Rejected</a>
                                                    @else
                                                        <a href="{{route('admin.accept.submission',$submission->id)}}"  id="acceptbtn"
                                                            class="btn btn-success">Accept</a>
                                                        <a href="{{route('admin.reject.submission',$submission->id)}}" id="rejectbtn" class="btn btn-danger">Reject</a>

                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->

        <div class="modal fade" id="modal-default">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Send Email</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form id="send-form" method="POST">
                            @csrf
                            <div class="form-group">
                                <label for="template">Template</label>
                                <select name="template" id="template" class="custom-select">
                                    @foreach ($templates as $template)
                                        <option value="{{ $template->id }}">
                                            {{ $template->name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer justify-content-end">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary" form="send-form"
                            onclick="!this.form && document.getElementById('send-form').submit()">Send</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    </div>

    <script>
        @if(Session::has('message'))
        toastr.options = {
            "closeButton": true,
            "progressBar": true
        }
        toastr.success("{{ session('message') }}");
        @endif
        </script>
@endsection

@section('custom-script')
    <!-- DataTables -->
     <script src="{{ URL::asset('admin-assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
            <script src="{{ URL::asset('admin-assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
            <script src="{{ URL::asset('admin-assets/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}">
            </script>
            <script src="{{ URL::asset('admin-assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}">
            </script>
            <script src="https://cdn.datatables.net/buttons/2.0.1/js/dataTables.buttons.min.js"></script>

            <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
            <script src="https://cdn.datatables.net/buttons/2.0.1/js/buttons.html5.min.js"></script>
            <script src="https://cdn.datatables.net/buttons/2.0.1/js/buttons.colVis.min.js"></script>
    <!-- --------------------------DataTable Start ----------------- -->
    {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css"> --}}
    <!-- --------------------------DataTable End ----------------- -->

    <script>
        $(document).ready(function() {
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true,
                "responsive": true,
                "columnDefs": [{
                    "orderable": false,
                    "targets": 0
                }],
                'aaSorting': [
                    [0, 'desc']
                ],
                dom: 'Bfrtip',
                buttons: [
                    // {
                    //     extend: 'copyHtml5',
                    //     exportOptions: {
                    //         columns: [ 0, ':visible' ]
                    //     }
                    // },
                    {
                        extend: 'excelHtml5',
                        title: 'submissions',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    {
                        extend: 'pdfHtml5',
                        title: 'submissions',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    'colvis'
                ]
            });
            // $('#submissiontable').dataTable();
        });
    </script>
    <script>
        $('#acceptbtn').click(function(e) {
            // alert();
            var id = $(this).val();
            // alert(id);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            // alert();
            $.ajax({
                url: "/ajax-accept",
                type: "POST",
                dataType: 'JSON',
                data: {
                    id: id,
                },
                // alert(),
                success: function(response) {
                    console.log(response);
                    if (response) {
                        $('.success').text(response.success);
                        $("#ajaxform")[0].reset();
                    }
                },
                error: function(error) {
                    console.log(error);
                }
            });
        })
    </script>

@endsection
