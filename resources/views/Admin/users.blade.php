@extends('layouts.admin')

@section('custom-css')
<!-- Ionicons -->
<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<!-- DataTables -->
<link rel="stylesheet" href="{{ URL::asset('admin-assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ URL::asset('admin-assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/2.0.1/css/buttons.dataTables.min.css">
@endsection

@section('section-content')
<style>
    table th {
        font-weight: 100 !important;
    }

    .table {
        width: max-content;
    }

    /* btn animation  */
    .custom-btn {
        color: #ffffff !important;
        border-radius: 5px;
        padding: 9px 55px;
        font-family: 'Lato', sans-serif;
        font-weight: 500;
        background: #153E58 !important;
        cursor: pointer;
        transition: all 0.3s ease;
        position: relative;
        display: inline-block;
        /* box-shadow: inset 2px 2px 2px 0px rgb(255 255 255 / 50%), 7px 7px 20px 0px rgb(0 0 0 / 10%), 4px 4px 5px 0px rgb(0 0 0 / 10%); */
        outline: none;
        text-decoration: none;
        width: 100%;
    }


    /* 10 */
    .btn-10 {
        background: rgb(22, 9, 240);
        background: linear-gradient(0deg, rgb(255 255 255) 0%, rgb(255 255 255) 100%);
        color: #fff;
        border: none;
        transition: all 0.3s ease;
        overflow: hidden;
    }

    .btn-10:after {
        position: absolute;
        content: " ";
        top: 0;
        left: 0;
        z-index: -1;
        width: 100%;
        height: 100%;
        transition: all 0.3s ease;
        -webkit-transform: scale(.1);
        transform: scale(.1);
    }

    .btn-10:hover {
        color: #fff !important;
        background: transparent;
        text-decoration: none;
    }

    .btn-10:hover:after {
        background: #F5CC44;
        -webkit-transform: scale(1);
        transform: scale(1);
        color: white !important;

    }

    .card-title {
        font-size: 18px;
    }
</style>
<!-- Content Header (Page header) -->
{{-- <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Users</h1>
                </div>
            </div>
        </div>
    </section> --}}

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-12">
            <div class="card mt-3">
                <div class="card-header">
                    <div class="d-flex justify-content-between align-items-center">
                        <h3 class="card-title">All Registrations</h3>
                        <div class="d-flex justify-content-between align-item-center">
                            {{-- <button type="button" class="btn btn-success mr-1" data-toggle="modal"
                                    data-target="#modal-create">Create Attendant</button> --}}
                            <button type="button" style="z-index: 99999999;" class="send ml-1 custom-btn btn-10" id="emailsend">Send Email</button>
                            <!-- <button type="button" style="z-index: 99999999;" class="send ml-1 custom-btn btn-10" data-toggle="modal"
                                    data-target="#modal-default">Send Email</button> -->
                        </div>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>
                                        <input type="checkbox" name="all" id="all" value="0">
                                    </th>
                                    <th>Id</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Company</th>
                                    <th>Position</th>
                                    <th>Address</th>
                                    <th>Attendance</th>
                                    <th>Created At</th>
                                    {{-- <th>Actions</th> --}}
                                </tr>
                            </thead>
                            <tbody>
                                <!-- @foreach ($user as $user)
                                        <tr>
                                            <td>
                                                <input type="checkbox" class="sender" name="attendant{{ $user->id }}"
                                                    id="attendant{{ $user->id }}" value="{{ $user->id }}">
                                            </td>
                                            <td>{{ $user->id }}</td>
                                            <td>{{ $user->name }}</td>
                                            <td>{{ $user->email }}</td>
                                            <td>{{ $user->phone }}</td>
                                            <td>{{ $user->company }}</td>
                                            <td>{{ $user->position }}</td>
                                            <td>{{ $user->address }}</td>
                                            <td>{{ $user->attendance }}</td>
                                            <td>{{ $user->created_at->format('d-M-Y H:i a') }}</td>
                                            <td>
                                                <button type="button" class="btn btn-danger btn-sm"
                                                    onclick="bringEditModal({{ $user->id }})">
                                                    <i class="fas fa-edit"></i> Edit
                                                </button>
                                            </td>
                                        </tr>
                                    @endforeach  -->
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->

<div class="modal fade" id="modal-default">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Send Email</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="send-form" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="template">Template</label>
                        <select name="template" id="template" class="custom-select">
                            @foreach ($templates as $template)
                            <option value="{{ $template->id }}">
                                {{ $template->name }}
                            </option>
                            @endforeach
                        </select>
                    </div>
                </form>
            </div>
            <div class="modal-footer justify-content-end">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary" form="send-form" onclick="!this.form && document.getElementById('send-form').submit()">Send</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@endsection

@section('custom-script')
<!-- DataTables -->
<script src="{{ URL::asset('admin-assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('admin-assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('admin-assets/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('admin-assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script src="https://cdn.datatables.net/buttons/2.0.1/js/dataTables.buttons.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/2.0.1/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.0.1/js/buttons.colVis.min.js"></script>


<script>
    let id = ""
    $(document).ready(function() {
        let recipients = []
        const all = JSON.parse("{{ json_encode($ids) }}")

        // $('#example2').DataTable({
        //     "paging": true,
        //     "lengthChange": false,
        //     "searching": true,
        //     "ordering": true,
        //     "info": true,
        //     "autoWidth": false,
        //     "responsive": false,
        //     "columnDefs": [{
        //         "orderable": false,
        //         "targets": 0
        //     }],
        //     'aaSorting': [
        //         [1, 'asc']
        //     ],
        //     dom: 'Bfrtip',
        //     buttons: [
        //         {
        //             extend: 'excelHtml5',
        //             title: 'Attendants',
        //             exportOptions: {
        //                 columns: ':visible'
        //             }
        //         },
        //         {
        //             extend: 'pdfHtml5',
        //             title: 'Attendants',
        //             orientation : 'landscape',
        //             // pageSize : 'LEGAL',
        //             exportOptions: {
        //                 columns: ':visible'
        //             }
        //         },
        //         'colvis'
        //     ]
        // });

        $('#example2').DataTable({
            ordering: true,
            info: true,
            autoWidth: false,
            responsive: false,
            columnDefs: [{
                orderable: false,
                targets: 0
            }],
            aaSorting: [
                [1, 'asc']
            ],
            processing: true,
            bLengthChange: false,
            bFilter: true,
            serverSide: true,
            ajax: "{{ route('admin.attendants') }}",
            columns: [{
                    data: 'selection'
                },
                {
                    data: 'id',
                    name: 'id'
                },
                {
                    data: 'name',
                    name: 'name'
                },

                {
                    data: 'email',
                    name: 'email'
                },
                {
                    data: 'phone',
                    name: 'phone'
                },

                {
                    data: 'company',
                    name: 'company'
                },
                {
                    data: 'position',
                    name: 'position'
                },
                {
                    data: 'address',
                    name: 'address'
                },
                {
                    data: 'attendance',
                    name: 'attendance'
                },
                {
                    data: 'created_at'
                }
            ],
            dom: 'Bfrtip',
            buttons: [{
                    extend: 'excelHtml5',
                    title: 'Attendants',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'pdfHtml5',
                    title: 'Attendants',
                    orientation: 'landscape',
                    // pageSize : 'LEGAL',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                'colvis'
            ]
        });

        // $("input[type=checkbox][name!='all'][class='sender']").on("change", function(e) {
        $(document).on('click', function(e) {
            if ($(e.target).hasClass('sender')) {
                if (e.target.checked) {
                    recipients.push(e.target.value)
                } else {
                    recipients = recipients.filter(recipient => recipient != e.target.value)
                }

                if (recipients.length > 0) {
                    $("button.send").prop("disabled", false)
                } else {
                    $("button.send").prop("disabled", true)
                }
            }
        })

        $("input[type=checkbox][name='all']").on("change", function(e) {
            $("input[type=checkbox][class='sender']").not(this).prop('checked', this.checked);
            if (e.target.checked) {
                recipients = all
            } else {
                recipients = []
            }
            if (recipients.length > 0) {
                $("button.send").prop("disabled", false)
            } else {
                $("button.send").prop("disabled", true)
            }
        })

        // ---------------------Here Code On Notepad First Part ----------------

        //------------------------Before SetError Function --------------------------------
        $("#send-form").on("submit", function(e) {
            //  alert('tahmeer');
            e.preventDefault();
            const _this = $(e.target)
            const formData = new FormData(e.target)
            recipients.forEach(recipient => {
                formData.append("recipients[]", recipient)
            })
            $(_this).find("button[type='submit']").prop('disabled', true)
            $.ajax({
                method: "POST",
                url: "",
                //  admin.send.emails, attendant

                data: formData,
                processData: false,
                contentType: false,
            }).done(function(res) {
                if (res.hasOwnProperty("message")) {
                    $("#modal-default").modal("toggle")
                    toastr.success(res.message, {
                        extendedTimeOut: 0,
                        timeOut: 800
                    })
                    setTimeout(function() {
                        window.location.reload();
                    }, 1000);
                }
            }).fail(function(error) {
                console.log(error);
            })
        })
        $("#create-form").on("submit", function(e) {
            e.preventDefault();
            const _this = $(e.target)
            const formData = new FormData(e.target)
            $(_this).find("button[type='submit']").prop('disabled', true)
            let url = ""
            //  admin.attendants.store
            if (id) {
                url = ""
                //admin.attendants.update', 'a_id'
                url = url.replace('a_id', id)
                formData.append("_method", "PUT")
            }
            $.ajax({
                method: "POST",
                url: url,
                data: formData,
                processData: false,
                contentType: false,
            }).done(function(res) {
                if (res.hasOwnProperty("message")) {
                    $("#modal-create").modal("toggle")
                    toastr.success(res.message, {
                        extendedTimeOut: 0,
                        timeOut: 800
                    })
                    setTimeout(function() {
                        window.location.reload();
                    }, 1000);
                }
            }).fail(function(error) {
                if (error.status == "422") {
                    setErrors(error.responseJSON.errors)
                }
            })
        })

        $("#create-form").on("submit", function(e) {
            e.preventDefault();
            const _this = $(e.target)
            const formData = new FormData(e.target)
            $(_this).find("button[type='submit']").prop('disabled', true)
        })


        // -----------------------------------End SetError code----------------------




        function setErrors(errors) {
            for (const [field, message] of Object.entries(errors)) {
                $(`#${field}`).addClass("is-invalid")
                $(`<div class="invalid-feedback" role="alert">
                        <strong>${message[0]}</strong>
                    </div>`).insertAfter(`#${field}`);
            }
        }

        $("#modal-create").on("hidden.bs.modal", function(e) {
            id = ""
        })
    });

    // ----------------------After SetError Function ------------------------------
    function bringEditModal(dd) {
        id = dd
        let url = ""
        // admin.attendants.show', 'a_id'
        url = url.replace('a_id', id)
        $(function() {
            $.ajax({
                method: "GET",
                url: url,
            }).done(function(res) {
                if (res.hasOwnProperty("attendant")) {
                    Object.entries(res.attendant).forEach(([key, value]) => {
                        if (['name', 'email', 'phone', 'company', 'job_title'].includes(
                                key)) {
                            $(`#${key}`).val(value)
                        }
                    })
                    $("#modal-create").modal("toggle")

                }
            }).fail(function(error) {
                console.log(error);
            })
        })
    }


    // ----------------------------Here Code On Notepad Second part--------------------------

    $('#emailsend').click(function() {
        $.ajax({
            url: "{{route('admin.sendemail.emails')}}",
            type: "GET",
            success: function(response) {
                toastr.success(response.message);
                //  alert(response.message);

               

            },

        });
    })
</script>
@endsection