@extends('layouts.admin')

@section('custom-css')
@toastr_css
@endsection

@section('section-content')
    <div class="mx-3">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Admin Edit</h1>
                    </div>
                </div>
            </div>
        </section>

        <section class="content">
            <div class="row px-4">
                <div class="col-sm-4">
                    <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                        <a class="nav-link {{ !$errors->any('old_password', 'password') ? 'active' : '' }}" id="v-info-tab"
                            data-toggle="pill" href="#v-info" role="tab" aria-controls="v-info"
                            aria-selected="true">Info</a>
                        <a class="nav-link {{ $errors->any('old_password', 'password') ? 'active' : '' }}" id="v-pass-tab"
                            data-toggle="pill" href="#v-pass" role="tab" aria-controls="v-pass"
                            aria-selected="false">Password</a>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="tab-content" id="v-pills-tabContent">
                        <div class="tab-pane fade {{ !$errors->any('old_password', 'password') ? 'show active' : '' }}"
                            id="v-info" role="tabpanel" aria-labelledby="v-info-tab">
                            <form action="{{ route('admin.update') }}" method="POST">
                                @csrf
                                @method('PUT')
                                <div class="form-group">
                                    <label for="name">Name</label>
                                    <input type="text" id="name" name="name"
                                        class="form-control @error('name') is-invalid @enderror"
                                        value="{{ old('name') ?: $user->name }}">
                                    @error('name')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="email" id="email" name="email"
                                        class="form-control @error('email') is-invalid @enderror"
                                        value="{{ old('email') ?: $user->email }}">
                                    @error('email')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                                <button type="submit" class="btn btn-primary">Update Info</button>
                            </form>
                        </div>
                        <div class="tab-pane fade {{ $errors->any('old_password', 'password') ? 'show active' : '' }}"
                            id="v-pass" role="tabpanel" aria-labelledby="v-pass-tab">
                            <form action="{{ route('admin.update') }}" method="POST">
                                @csrf
                                @method('PUT')
                                <div class="form-group">
                                    <label for="old-password">Old Password</label>
                                    <input type="password" id="old-password" name="old_password"
                                        class="form-control @error('old_password') is-invalid @enderror">
                                    @error('old_password')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="password">New Password</label>
                                    <input type="password" id="password" name="password"
                                        class="form-control @error('password') is-invalid @enderror">
                                    @error('password')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="cpassword">Confirm Password</label>
                                    <input type="password" id="cpassword" name="confirmation_password"
                                        class="form-control">
                                </div>
                                <button type="submit" class="btn btn-primary">Change Password</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@jquery
@toastr_js
@toastr_render
@endsection
