@extends('layouts.app')

@section('content')
<!-- {{ __('check') }} -->
<!-- Start main-content -->
<div class="main-content">
    <!-- Section: home -->
    <section id="home">

        <!-- Slider Revolution Start -->
        <div class="rev_slider_wrapper" style="height: 800px !important;">
            <div class="rev_slider" data-version="5.0">
                <ul>

                    <!-- SLIDE 1 -->
                    <li data-index="rs-1" data-transition="slidingoverlayhorizontal" data-slotamount="default"
                        data-easein="default" data-easeout="default" data-masterspeed="default"
                        data-thumb="{{ asset('images/banner-new-1.jpg') }}" data-rotate="0" data-saveperformance="off"
                        data-title="Slide 1" data-description="">
                        <!-- MAIN IMAGE -->
                        <img src="{{ asset('images/banner-new-1.jpg') }}" alt="" data-bgposition="center bottom"
                            data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-bgparallax="10"
                            data-no-retina>
                        <!-- LAYERS -->

                        <!-- LAYER NR. 1 -->
                        <div class="tp-caption tp-resizeme text-uppercase text-white font-raleway" id="rs-1-layer-1"
                            data-x="['left']" data-hoffset="['30']" data-y="['middle']" data-voffset="['-110']"
                            data-fontsize="['100']" data-lineheight="['110']" data-width="none" data-height="none"
                            data-whitespace="nowrap" data-transform_idle="o:1;s:500"
                            data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                            data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                            data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                            data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1000"
                            data-splitin="none" data-splitout="none" data-responsive_offset="on"
                            style="z-index: 7; white-space: nowrap; font-weight:700;">{{ __('Education') }}
                        </div>

                        <!-- LAYER NR. 2 -->
                        <div class="tp-caption tp-resizeme text-uppercase text-white font-raleway bg-theme-colored-transparent border-left-theme-color-2-6px pl-20 pr-20"
                            id="rs-1-layer-2" data-x="['left']" data-hoffset="['35']" data-y="['middle']"
                            data-voffset="['-25']" data-fontsize="['35']" data-lineheight="['54']" data-width="none"
                            data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;s:500"
                            data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                            data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                            data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                            data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1000"
                            data-splitin="none" data-splitout="none" data-responsive_offset="on"
                            style="z-index: 7; white-space: nowrap; font-weight:600;">
                            {{ __('King Salman global academy') }}
                        </div>

                        <!-- LAYER NR. 3 -->
                        <div class="tp-caption tp-resizeme text-white" id="rs-1-layer-3" data-x="['left']"
                            data-hoffset="['35']" data-y="['middle']" data-voffset="['35']" data-fontsize="['16']"
                            data-lineheight="['28']" data-width="none" data-height="none" data-whitespace="nowrap"
                            data-transform_idle="o:1;s:500" data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                            data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                            data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                            data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1400"
                            data-splitin="none" data-splitout="none" data-responsive_offset="on"
                            style="z-index: 5; white-space: nowrap; letter-spacing:0px; font-weight:400;">
                        </div>

                        <!-- LAYER NR. 4 -->
                        <div class="tp-caption tp-resizeme" id="rs-1-layer-4" data-x="['left']" data-hoffset="['35']"
                            data-y="['middle']" data-voffset="['100']" data-width="none" data-height="none"
                            data-whitespace="nowrap" data-transform_idle="o:1;"
                            data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
                            data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                            data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;"
                            data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1400"
                            data-splitin="none" data-splitout="none" data-responsive_offset="on"
                            style="z-index: 5; white-space: nowrap; letter-spacing:1px;"><a
                                class="btn btn-colored btn-lg btn-flat btn-theme-colored border-left-theme-color-2-6px pl-20 pr-20 h-colo"
                                href="{{ route('register') }}">{{ __('Register Now') }}</a>
                        </div>
                    </li>

                    <!-- SLIDE 2 -->
                    <li data-index="rs-2" data-transition="slidingoverlayhorizontal" data-slotamount="default"
                        data-easein="default" data-easeout="default" data-masterspeed="default"
                        data-thumb="{{ asset('images/banner-new-2.jpg') }}" data-rotate="0" data-saveperformance="off"
                        data-title="Slide 2" data-description="">
                        <!-- MAIN IMAGE -->
                        <img src="{{ asset('images/banner-new-2.jpg') }}" alt="" data-bgposition="center bottom"
                            data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-bgparallax="10"
                            data-no-retina>
                        <!-- LAYERS -->

                        <!-- LAYER NR. 1 -->
                        <div class="tp-caption tp-resizeme text-uppercase  bg-theme-colored-transparent text-white font-raleway border-left-theme-color-2-6px border-right-theme-color-2-6px pl-30 pr-30"
                            id="rs-2-layer-1" data-x="['center']" data-hoffset="['0']" data-y="['middle']"
                            data-voffset="['-90']" data-fontsize="['28']" data-lineheight="['54']" data-width="none"
                            data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;s:500"
                            data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                            data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                            data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                            data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1000"
                            data-splitin="none" data-splitout="none" data-responsive_offset="on"
                            style="z-index: 7; white-space: nowrap; font-weight:400; border-radius: 30px;">
                            {{ __('King Salman global academy') }}
                        </div>

                        <!-- LAYER NR. 2 -->
                        <div class="tp-caption tp-resizeme text-uppercase bg-theme-colored-transparent text-white font-raleway pl-30 pr-30 tp-caption-mob"
                            id="rs-2-layer-2" data-x="['center']" data-hoffset="['0']" data-y="['middle']"
                            data-voffset="['-20']" data-fontsize="['48']" data-lineheight="['70']" data-width="none"
                            data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;s:500"
                            data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                            data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                            data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                            data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1000"
                            data-splitin="none" data-splitout="none" data-responsive_offset="on"
                            style="z-index: 7; white-space: nowrap; font-weight:700; border-radius: 30px;">
                            {{ __('King Salman global academy') }}
                        </div>

                        <!-- LAYER NR. 3 -->
                        <div class="tp-caption tp-resizeme text-white text-center" id="rs-2-layer-3" data-x="['center']"
                            data-hoffset="['0']" data-y="['middle']" data-voffset="['50']" data-fontsize="['16']"
                            data-lineheight="['28']" data-width="none" data-height="none" data-whitespace="nowrap"
                            data-transform_idle="o:1;s:500" data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                            data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                            data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                            data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1400"
                            data-splitin="none" data-splitout="none" data-responsive_offset="on"
                            style="z-index: 5; white-space: nowrap; letter-spacing:0px; font-weight:400;">
                        </div>

                        <!-- LAYER NR. 4 -->
                        <div class="tp-caption tp-resizeme" id="rs-2-layer-4" data-x="['center']" data-hoffset="['0']"
                            data-y="['middle']" data-voffset="['115']" data-width="none" data-height="none"
                            data-whitespace="nowrap" data-transform_idle="o:1;"
                            data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
                            data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                            data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;"
                            data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1400"
                            data-splitin="none" data-splitout="none" data-responsive_offset="on"
                            style="z-index: 5; white-space: nowrap; letter-spacing:1px;"><a
                                class="btn-circled btn btn-colored btn-lg btn-flat btn-theme-colored border-left-theme-color-2-6px pl-20 pr-20 h-colo"
                                href="{{ route('register') }}">{{ __('Register Now') }}</a>
                        </div>

                    </li>

                    <!-- SLIDE 3 -->
                    <li data-index="rs-3" data-transition="slidingoverlayhorizontal" data-slotamount="default"
                        data-easein="default" data-easeout="default" data-masterspeed="default"
                        data-thumb="{{ asset('images/banner-new-3.jpg') }}" data-rotate="0" data-saveperformance="off"
                        data-title="Slide 3" data-description="">
                        <!-- MAIN IMAGE -->
                        <img src="{{ asset('images/banner-new-3.jpg') }}" alt="" data-bgposition="center top"
                            data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-bgparallax="10"
                            data-no-retina>
                        <!-- LAYERS -->

                        <!-- LAYER NR. 1 -->
                        <div class="tp-caption tp-resizeme text-uppercase text-white font-raleway bg-theme-colored-transparent border-right-theme-color-2-6px pr-20 pl-20"
                            id="rs-3-layer-1" data-x="['right']" data-hoffset="['30']" data-y="['middle']"
                            data-voffset="['-90']" data-fontsize="['64']" data-lineheight="['72']" data-width="none"
                            data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;s:500"
                            data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                            data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                            data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                            data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1000"
                            data-splitin="none" data-splitout="none" data-responsive_offset="on"
                            style="z-index: 7; white-space: nowrap; font-weight:600;">
                            {{ __('King Salman global academy') }}
                        </div>

                        <!-- LAYER NR. 2 -->
                        {{-- <div class="tp-caption tp-resizeme text-uppercase text-white font-raleway"
                  id="rs-3-layer-2"

                  data-x="['right']"
                  data-hoffset="['35']"
                  data-y="['middle']"
                  data-voffset="['-25']"
                  data-fontsize="['32']"
                  data-lineheight="['54']"
                  data-width="none"
                  data-height="none"
                  data-whitespace="nowrap"
                  data-transform_idle="o:1;s:500"
                  data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                  data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                  data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                  data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                  data-start="1000"
                  data-splitin="none"
                  data-splitout="none"
                  data-responsive_offset="on"
                  style="z-index: 7; white-space: nowrap; font-weight:600;">For Your Better Future
                </div> --}}

                        <!-- LAYER NR. 3 -->
                        {{-- <div class="tp-caption tp-resizeme text-white text-right"
                  id="rs-3-layer-3"

                  data-x="['right']"
                  data-hoffset="['35']"
                  data-y="['middle']"
                  data-voffset="['30']"
                  data-fontsize="['16']"
                  data-lineheight="['28']"
                  data-width="none"
                  data-height="none"
                  data-whitespace="nowrap"
                  data-transform_idle="o:1;s:500"
                  data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                  data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                  data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                  data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                  data-start="1400"
                  data-splitin="none"
                  data-splitout="none"
                  data-responsive_offset="on"
                  style="z-index: 5; white-space: nowrap; letter-spacing:0px; font-weight:400;">We provides always our best services for our clients and  always<br> try to achieve our client's trust and satisfaction.
                </div> --}}

                        <!-- LAYER NR. 4 -->
                        <div class="tp-caption tp-resizeme" id="rs-3-layer-4" data-x="['right']" data-hoffset="['35']"
                            data-y="['middle']" data-voffset="['95']" data-width="none" data-height="none"
                            data-whitespace="nowrap" data-transform_idle="o:1;"
                            data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
                            data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                            data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;"
                            data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1400"
                            data-splitin="none" data-splitout="none" data-responsive_offset="on"
                            style="z-index: 5; white-space: nowrap; letter-spacing:1px;"><a
                                class="btn btn-colored btn-lg btn-flat btn-theme-colored btn-theme-colored border-right-theme-color-2-6px pl-20 pr-20 {{ route('register') }}">{{ __('Register Now') }}</a>
                        </div>
                    </li>



                    <!-- SLIDE 1 -->
                    <li data-index="rs-1" data-transition="slidingoverlayhorizontal" data-slotamount="default"
                        data-easein="default" data-easeout="default" data-masterspeed="default"
                        data-thumb="{{ asset('images/banner-new-4.jpg') }}" data-rotate="0" data-saveperformance="off"
                        data-title="Slide 1" data-description="">
                        <!-- MAIN IMAGE -->
                        <img src="{{ asset('images/banner-new-4.jpg') }}" alt="" data-bgposition="center bottom"
                            data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-bgparallax="10"
                            data-no-retina>
                        <!-- LAYERS -->

                        <!-- LAYER NR. 1 -->
                        <div class="tp-caption tp-resizeme text-uppercase text-white font-raleway" id="rs-1-layer-1"
                            data-x="['left']" data-hoffset="['30']" data-y="['middle']" data-voffset="['-110']"
                            data-fontsize="['100']" data-lineheight="['110']" data-width="none" data-height="none"
                            data-whitespace="nowrap" data-transform_idle="o:1;s:500"
                            data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                            data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                            data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                            data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1000"
                            data-splitin="none" data-splitout="none" data-responsive_offset="on"
                            style="z-index: 7; white-space: nowrap; font-weight:700;">{{ __('Education') }}
                        </div>

                        <!-- LAYER NR. 2 -->
                        <div class="tp-caption tp-resizeme text-uppercase text-white font-raleway bg-theme-colored-transparent border-left-theme-color-2-6px pl-20 pr-20"
                            id="rs-1-layer-2" data-x="['left']" data-hoffset="['35']" data-y="['middle']"
                            data-voffset="['-25']" data-fontsize="['35']" data-lineheight="['54']" data-width="none"
                            data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;s:500"
                            data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                            data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                            data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                            data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1000"
                            data-splitin="none" data-splitout="none" data-responsive_offset="on"
                            style="z-index: 7; white-space: nowrap; font-weight:600;">
                            {{ __('King Salman global academy') }}
                        </div>

                        <!-- LAYER NR. 3 -->
                        <div class="tp-caption tp-resizeme text-white" id="rs-1-layer-3" data-x="['left']"
                            data-hoffset="['35']" data-y="['middle']" data-voffset="['35']" data-fontsize="['16']"
                            data-lineheight="['28']" data-width="none" data-height="none" data-whitespace="nowrap"
                            data-transform_idle="o:1;s:500" data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                            data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                            data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                            data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1400"
                            data-splitin="none" data-splitout="none" data-responsive_offset="on"
                            style="z-index: 5; white-space: nowrap; letter-spacing:0px; font-weight:400;">
                        </div>

                        <!-- LAYER NR. 4 -->
                        <div class="tp-caption tp-resizeme" id="rs-1-layer-4" data-x="['left']" data-hoffset="['35']"
                            data-y="['middle']" data-voffset="['100']" data-width="none" data-height="none"
                            data-whitespace="nowrap" data-transform_idle="o:1;"
                            data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
                            data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                            data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;"
                            data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1400"
                            data-splitin="none" data-splitout="none" data-responsive_offset="on"
                            style="z-index: 5; white-space: nowrap; letter-spacing:1px;"><a
                                class="btn btn-colored btn-lg btn-flat btn-theme-colored border-left-theme-color-2-6px pl-20 pr-20 h-colo"
                                href="{{ route('register') }}">{{ __('Register Now') }}</a>
                        </div>
                    </li>

                    <!-- SLIDE 2 -->
                    <li data-index="rs-2" data-transition="slidingoverlayhorizontal" data-slotamount="default"
                        data-easein="default" data-easeout="default" data-masterspeed="default"
                        data-thumb="{{ asset('images/banner-new-5.png') }}" data-rotate="0" data-saveperformance="off"
                        data-title="Slide 2" data-description="">
                        <!-- MAIN IMAGE -->
                        <img src="{{ asset('images/banner-new-5.png') }}" alt="" data-bgposition="center bottom"
                            data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-bgparallax="10"
                            data-no-retina>
                        <!-- LAYERS -->

                        <!-- LAYER NR. 1 -->
                        <div class="tp-caption tp-resizeme text-uppercase  bg-theme-colored-transparent text-white font-raleway border-left-theme-color-2-6px border-right-theme-color-2-6px pl-30 pr-30"
                            id="rs-2-layer-1" data-x="['center']" data-hoffset="['0']" data-y="['middle']"
                            data-voffset="['-90']" data-fontsize="['28']" data-lineheight="['54']" data-width="none"
                            data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;s:500"
                            data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                            data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                            data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                            data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1000"
                            data-splitin="none" data-splitout="none" data-responsive_offset="on"
                            style="z-index: 7; white-space: nowrap; font-weight:400; border-radius: 30px;">
                            {{ __('King Salman global academy') }}
                        </div>

                        <!-- LAYER NR. 2 -->
                        <div class="tp-caption tp-resizeme text-uppercase bg-theme-colored-transparent text-white font-raleway pl-30 pr-30 tp-caption-mob"
                            id="rs-2-layer-2" data-x="['center']" data-hoffset="['0']" data-y="['middle']"
                            data-voffset="['-20']" data-fontsize="['48']" data-lineheight="['70']" data-width="none"
                            data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;s:500"
                            data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                            data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                            data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                            data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1000"
                            data-splitin="none" data-splitout="none" data-responsive_offset="on"
                            style="z-index: 7; white-space: nowrap; font-weight:700; border-radius: 30px;">
                            {{ __('King Salman global academy') }}
                        </div>

                        <!-- LAYER NR. 3 -->
                        <div class="tp-caption tp-resizeme text-white text-center" id="rs-2-layer-3" data-x="['center']"
                            data-hoffset="['0']" data-y="['middle']" data-voffset="['50']" data-fontsize="['16']"
                            data-lineheight="['28']" data-width="none" data-height="none" data-whitespace="nowrap"
                            data-transform_idle="o:1;s:500" data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                            data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                            data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                            data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1400"
                            data-splitin="none" data-splitout="none" data-responsive_offset="on"
                            style="z-index: 5; white-space: nowrap; letter-spacing:0px; font-weight:400;">
                        </div>

                        <!-- LAYER NR. 4 -->
                        <div class="tp-caption tp-resizeme" id="rs-2-layer-4" data-x="['center']" data-hoffset="['0']"
                            data-y="['middle']" data-voffset="['115']" data-width="none" data-height="none"
                            data-whitespace="nowrap" data-transform_idle="o:1;"
                            data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
                            data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                            data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;"
                            data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1400"
                            data-splitin="none" data-splitout="none" data-responsive_offset="on"
                            style="z-index: 5; white-space: nowrap; letter-spacing:1px;"><a
                                class="btn-circled btn btn-colored btn-lg btn-flat btn-theme-colored border-left-theme-color-2-6px pl-20 pr-20 h-colo"
                                href="{{ route('register') }}">{{ __('Register Now') }}</a>
                        </div>

                    </li>

                    <!-- SLIDE 3 -->
                    <li data-index="rs-3" data-transition="slidingoverlayhorizontal" data-slotamount="default"
                        data-easein="default" data-easeout="default" data-masterspeed="default"
                        data-thumb="{{ asset('images/banner-new-6.jpg') }}" data-rotate="0" data-saveperformance="off"
                        data-title="Slide 3" data-description="">
                        <!-- MAIN IMAGE -->
                        <img src="{{ asset('images/banner-new-6.jpg') }}" alt="" data-bgposition="center top"
                            data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-bgparallax="10"
                            data-no-retina>
                        <!-- LAYERS -->

                        <!-- LAYER NR. 1 -->
                        <div class="tp-caption tp-resizeme text-uppercase text-white font-raleway bg-theme-colored-transparent border-right-theme-color-2-6px pr-20 pl-20"
                            id="rs-3-layer-1" data-x="['right']" data-hoffset="['30']" data-y="['middle']"
                            data-voffset="['-90']" data-fontsize="['64']" data-lineheight="['72']" data-width="none"
                            data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;s:500"
                            data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                            data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                            data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                            data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1000"
                            data-splitin="none" data-splitout="none" data-responsive_offset="on"
                            style="z-index: 7; white-space: nowrap; font-weight:600;">
                            {{ __('King Salman global academy') }}
                        </div>

                        <!-- LAYER NR. 2 -->
                        {{-- <div class="tp-caption tp-resizeme text-uppercase text-white font-raleway"
                                     id="rs-3-layer-2"
                   
                                     data-x="['right']"
                                     data-hoffset="['35']"
                                     data-y="['middle']"
                                     data-voffset="['-25']"
                                     data-fontsize="['32']"
                                     data-lineheight="['54']"
                                     data-width="none"
                                     data-height="none"
                                     data-whitespace="nowrap"
                                     data-transform_idle="o:1;s:500"
                                     data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                                     data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                                     data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                                     data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                                     data-start="1000"
                                     data-splitin="none"
                                     data-splitout="none"
                                     data-responsive_offset="on"
                                     style="z-index: 7; white-space: nowrap; font-weight:600;">For Your Better Future
                                   </div> --}}

                        <!-- LAYER NR. 3 -->
                        {{-- <div class="tp-caption tp-resizeme text-white text-right"
                                     id="rs-3-layer-3"
                   
                                     data-x="['right']"
                                     data-hoffset="['35']"
                                     data-y="['middle']"
                                     data-voffset="['30']"
                                     data-fontsize="['16']"
                                     data-lineheight="['28']"
                                     data-width="none"
                                     data-height="none"
                                     data-whitespace="nowrap"
                                     data-transform_idle="o:1;s:500"
                                     data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                                     data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                                     data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                                     data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                                     data-start="1400"
                                     data-splitin="none"
                                     data-splitout="none"
                                     data-responsive_offset="on"
                                     style="z-index: 5; white-space: nowrap; letter-spacing:0px; font-weight:400;">We provides always our best services for our clients and  always<br> try to achieve our client's trust and satisfaction.
                                   </div> --}}

                        <!-- LAYER NR. 4 -->
                        <div class="tp-caption tp-resizeme" id="rs-3-layer-4" data-x="['right']" data-hoffset="['35']"
                            data-y="['middle']" data-voffset="['95']" data-width="none" data-height="none"
                            data-whitespace="nowrap" data-transform_idle="o:1;"
                            data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
                            data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                            data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;"
                            data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1400"
                            data-splitin="none" data-splitout="none" data-responsive_offset="on"
                            style="z-index: 5; white-space: nowrap; letter-spacing:1px;"><a
                                class="btn btn-colored btn-lg btn-flat btn-theme-colored btn-theme-colored border-right-theme-color-2-6px pl-20 pr-20 {{ route('register') }}">{{ __('Register Now') }}</a>
                        </div>
                    </li>


                    <!-- SLIDE 2 -->
                    <li data-index="rs-2" data-transition="slidingoverlayhorizontal" data-slotamount="default"
                        data-easein="default" data-easeout="default" data-masterspeed="default"
                        data-thumb="{{ asset('images/banner-new-7.jpg') }}" data-rotate="0" data-saveperformance="off"
                        data-title="Slide 2" data-description="">
                        <!-- MAIN IMAGE -->
                        <img src="{{ asset('images/banner-new-7.jpg') }}" alt="" data-bgposition="center bottom"
                            data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-bgparallax="10"
                            data-no-retina>
                        <!-- LAYERS -->

                        <!-- LAYER NR. 1 -->
                        <div class="tp-caption tp-resizeme text-uppercase  bg-theme-colored-transparent text-white font-raleway border-left-theme-color-2-6px border-right-theme-color-2-6px pl-30 pr-30"
                            id="rs-2-layer-1" data-x="['center']" data-hoffset="['0']" data-y="['middle']"
                            data-voffset="['-90']" data-fontsize="['28']" data-lineheight="['54']" data-width="none"
                            data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;s:500"
                            data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                            data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                            data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                            data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1000"
                            data-splitin="none" data-splitout="none" data-responsive_offset="on"
                            style="z-index: 7; white-space: nowrap; font-weight:400; border-radius: 30px;">
                            {{ __('King Salman global academy') }}
                        </div>

                        <!-- LAYER NR. 2 -->
                        <div class="tp-caption tp-resizeme text-uppercase bg-theme-colored-transparent text-white font-raleway pl-30 pr-30 tp-caption-mob"
                            id="rs-2-layer-2" data-x="['center']" data-hoffset="['0']" data-y="['middle']"
                            data-voffset="['-20']" data-fontsize="['48']" data-lineheight="['70']" data-width="none"
                            data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;s:500"
                            data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                            data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                            data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                            data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1000"
                            data-splitin="none" data-splitout="none" data-responsive_offset="on"
                            style="z-index: 7; white-space: nowrap; font-weight:700; border-radius: 30px;">
                            {{ __('King Salman global academy') }}
                        </div>

                        <!-- LAYER NR. 3 -->
                        <div class="tp-caption tp-resizeme text-white text-center" id="rs-2-layer-3" data-x="['center']"
                            data-hoffset="['0']" data-y="['middle']" data-voffset="['50']" data-fontsize="['16']"
                            data-lineheight="['28']" data-width="none" data-height="none" data-whitespace="nowrap"
                            data-transform_idle="o:1;s:500" data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                            data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                            data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                            data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1400"
                            data-splitin="none" data-splitout="none" data-responsive_offset="on"
                            style="z-index: 5; white-space: nowrap; letter-spacing:0px; font-weight:400;">
                        </div>

                        <!-- LAYER NR. 4 -->
                        <div class="tp-caption tp-resizeme" id="rs-2-layer-4" data-x="['center']" data-hoffset="['0']"
                            data-y="['middle']" data-voffset="['115']" data-width="none" data-height="none"
                            data-whitespace="nowrap" data-transform_idle="o:1;"
                            data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
                            data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                            data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;"
                            data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1400"
                            data-splitin="none" data-splitout="none" data-responsive_offset="on"
                            style="z-index: 5; white-space: nowrap; letter-spacing:1px;"><a
                                class="btn-circled btn btn-colored btn-lg btn-flat btn-theme-colored border-left-theme-color-2-6px pl-20 pr-20 h-colo"
                                href="{{ route('register') }}">{{ __('Register Now') }}</a>
                        </div>

                    </li>

                    <!-- SLIDE 3 -->
                    <li data-index="rs-3" data-transition="slidingoverlayhorizontal" data-slotamount="default"
                        data-easein="default" data-easeout="default" data-masterspeed="default"
                        data-thumb="{{ asset('images/banner-new-8.jpg') }}" data-rotate="0" data-saveperformance="off"
                        data-title="Slide 3" data-description="">
                        <!-- MAIN IMAGE -->
                        <img src="{{ asset('images/banner-new-8.jpg') }}" alt="" data-bgposition="center top"
                            data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-bgparallax="10"
                            data-no-retina>
                        <!-- LAYERS -->

                        <!-- LAYER NR. 1 -->
                        <div class="tp-caption tp-resizeme text-uppercase text-white font-raleway bg-theme-colored-transparent border-right-theme-color-2-6px pr-20 pl-20"
                            id="rs-3-layer-1" data-x="['right']" data-hoffset="['30']" data-y="['middle']"
                            data-voffset="['-90']" data-fontsize="['64']" data-lineheight="['72']" data-width="none"
                            data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;s:500"
                            data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                            data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                            data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                            data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1000"
                            data-splitin="none" data-splitout="none" data-responsive_offset="on"
                            style="z-index: 7; white-space: nowrap; font-weight:600;">
                            {{ __('King Salman global academy') }}
                        </div>

                        <!-- LAYER NR. 2 -->
                        {{-- <div class="tp-caption tp-resizeme text-uppercase text-white font-raleway"
                                  id="rs-3-layer-2"
                
                                  data-x="['right']"
                                  data-hoffset="['35']"
                                  data-y="['middle']"
                                  data-voffset="['-25']"
                                  data-fontsize="['32']"
                                  data-lineheight="['54']"
                                  data-width="none"
                                  data-height="none"
                                  data-whitespace="nowrap"
                                  data-transform_idle="o:1;s:500"
                                  data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                                  data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                                  data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                                  data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                                  data-start="1000"
                                  data-splitin="none"
                                  data-splitout="none"
                                  data-responsive_offset="on"
                                  style="z-index: 7; white-space: nowrap; font-weight:600;">For Your Better Future
                                </div> --}}

                        <!-- LAYER NR. 3 -->
                        {{-- <div class="tp-caption tp-resizeme text-white text-right"
                                  id="rs-3-layer-3"
                
                                  data-x="['right']"
                                  data-hoffset="['35']"
                                  data-y="['middle']"
                                  data-voffset="['30']"
                                  data-fontsize="['16']"
                                  data-lineheight="['28']"
                                  data-width="none"
                                  data-height="none"
                                  data-whitespace="nowrap"
                                  data-transform_idle="o:1;s:500"
                                  data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                                  data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                                  data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                                  data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                                  data-start="1400"
                                  data-splitin="none"
                                  data-splitout="none"
                                  data-responsive_offset="on"
                                  style="z-index: 5; white-space: nowrap; letter-spacing:0px; font-weight:400;">We provides always our best services for our clients and  always<br> try to achieve our client's trust and satisfaction.
                                </div> --}}

                        <!-- LAYER NR. 4 -->
                        <div class="tp-caption tp-resizeme" id="rs-3-layer-4" data-x="['right']" data-hoffset="['35']"
                            data-y="['middle']" data-voffset="['95']" data-width="none" data-height="none"
                            data-whitespace="nowrap" data-transform_idle="o:1;"
                            data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
                            data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                            data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;"
                            data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1400"
                            data-splitin="none" data-splitout="none" data-responsive_offset="on"
                            style="z-index: 5; white-space: nowrap; letter-spacing:1px;"><a
                                class="btn btn-colored btn-lg btn-flat btn-theme-colored btn-theme-colored border-right-theme-color-2-6px pl-20 pr-20 {{ route('register') }}">{{ __('Register Now') }}</a>
                        </div>
                    </li>
                </ul>
            </div>
            <!-- end .rev_slider -->
        </div>
        <!-- end .rev_slider_wrapper -->
        <script>
        $(document).ready(function(e) {
            $(".rev_slider").revolution({
                sliderType: "standard",
                sliderLayout: "auto",
                dottedOverlay: "none",
                delay: 5000,
                navigation: {
                    keyboardNavigation: "off",
                    keyboard_direction: "horizontal",
                    mouseScrollNavigation: "off",
                    onHoverStop: "off",
                    touch: {
                        touchenabled: "on",
                        swipe_threshold: 75,
                        swipe_min_touches: 1,
                        swipe_direction: "horizontal",
                        drag_block_vertical: false
                    },
                    arrows: {
                        style: "zeus",
                        enable: true,
                        hide_onmobile: true,
                        hide_under: 600,
                        hide_onleave: true,
                        hide_delay: 200,
                        hide_delay_mobile: 1200,
                        tmp: '<div class="tp-title-wrap">    <div class="tp-arr-imgholder"></div> </div>',
                        left: {
                            h_align: "left",
                            v_align: "center",
                            h_offset: 30,
                            v_offset: 0
                        },
                        right: {
                            h_align: "right",
                            v_align: "center",
                            h_offset: 30,
                            v_offset: 0
                        }
                    },
                    bullets: {
                        enable: true,
                        hide_onmobile: true,
                        hide_under: 600,
                        style: "metis",
                        hide_onleave: true,
                        hide_delay: 200,
                        hide_delay_mobile: 1200,
                        direction: "horizontal",
                        h_align: "center",
                        v_align: "bottom",
                        h_offset: 0,
                        v_offset: 30,
                        space: 5,
                        tmp: '<span class="tp-bullet-img-wrap">  <span class="tp-bullet-image"></span></span><span class="tp-bullet-title"></span>'
                    }
                },
                responsiveLevels: [1240, 1024, 778],
                visibilityLevels: [1240, 1024, 778],
                gridwidth: [1170, 1024, 778, 480],
                gridheight: [800, 768, 960, 720],
                lazyType: "none",
                parallax: {
                    origo: "slidercenter",
                    speed: 1000,
                    levels: [5, 10, 15, 20, 25, 30, 35, 40, 45, 46, 47, 48, 49, 50, 100, 55],
                    type: "scroll"
                },
                shadow: 0,
                spinner: "off",
                stopLoop: "on",
                stopAfterLoops: 0,
                stopAtSlide: -1,
                shuffle: "off",
                autoHeight: "off",
                fullScreenAutoWidth: "off",
                fullScreenAlignForce: "off",
                fullScreenOffsetContainer: "",
                fullScreenOffset: "0",
                hideThumbsOnMobile: "off",
                hideSliderAtLimit: 0,
                hideCaptionAtLimit: 0,
                hideAllCaptionAtLilmit: 0,
                debugMode: false,
                fallbacks: {
                    simplifyAll: "off",
                    nextSlideOnWindowFocus: "off",
                    disableFocusListener: false,
                }
            });
        });
        </script>
        <!-- Slider Revolution Ends -->

    </section>

    <!-- Section: welcome -->

    <section class="about-main wow fadeInDown animation-delay2"
        style="background-image: url({{ asset('images/about-bg.jpg') }}); background-position: bottom;">
        <section>
            <div class="container pt-0 pb-0">
                <div class="section-content">
                    <div class="row equal-height-inner mt-sm-0">
                        <div
                            class="col-md-offset-2 col-sm-12 col-md-8 pr-0 pr-sm-15 sm-height-auto mt-sm-0 wow fadeInLeft animation-delay1">
                            {{-- <h2 class="text-white text-uppercase text-center mt-0 tp-caption-mob mb-sm-0">
                                    {{ __('Conference Date') }}</h2> --}}
                            <h1 class="title text-white text-uppercase line-bottom mt-0 mb-0 text-center">
                                {{ __('Conference Date') }}
                            </h1>
                            {{-- <span style="background: #F5CC44;
                                                          width: 14%;
                                                          margin: auto;
                                                          height: 10px;
                                                          display: block; color: #F5CC44; text-align: center;">_____</span> --}}
                            <br>
                            {{-- <span id="days"></span>:
    <span id="hours"></span>:
    <span id="minutes"></span>:
    <span id="seconds"></span> --}}
                            <section class="coming-soon">
                                <div>
                                    <div class="countdown">
                                        <div class="container-day child">
                                            <h3 id="days">Time</h3>
                                            <h3 class="time-dis">{{ __('Days') }}</h3>
                                        </div>
                                        <div class="container-hour child">
                                            <h3 id="hours">Time</h3>
                                            <h3 class="time-dis"> {{ __('Hours') }} </h3>
                                        </div>
                                        <div class="container-minute child">
                                            <h3 id="minutes">Time</h3>
                                            <h3 class="time-dis">{{ __('Minutes') }}</h3>
                                        </div>
                                        <div class="container-second child-last">
                                            <h3 id="seconds">Time</h3>
                                            <h3 class="time-dis">{{ __('Seconds') }}</h3>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            {{-- <div class="sm-height-auto aboutImage"
                                    style="background-size: cover;background-position: center;">
                                    <div class="p-20" style="position: relative;"> --}}



                            {{-- <div class="demodiv">
                                            <h1 class="text-white text-center m-0 tp-caption-mob" id="demo"></h1>

                                            <h2 class="text-white text-center m-0 tp-caption-mob">{{ __('Days') }} :
                            {{ __('Hours') }} : {{ __('Minutes') }} : {{ __('Seconds') }}
                            </h2>
                        </div> --}}


                        {{-- </div>
                                </div> --}}
                    </div>

                </div>
            </div>
</div>
</section>

<div class="container">
    <div class="row">
        <div class="col-md-1"></div>
        <div class=" col-md-10">
            <h1 class="title text-white text-uppercase line-bottom mt-0 mb-0 text-center">
                {{ __('About The Conference') }}
            </h1>
            {{-- <span style="background: #F5CC44;
                                              width: 14%;
                                              margin: auto;
                                              height: 10px;
                                              display: block; color: #F5CC44; text-align: center;">_____</span> --}}
            <br>
            <p class="text-center">
                {{ __('Under the patronage of the Custodian of the Two Holy Mosques King Salman bin Abdulaziz Al Saud - may God protect him - the city of Riyadh will host the “Arabic Language in International Organizations” conference, which is organized by the King Salman International Complex for the Arabic Language in the first half of 2022. His Highness Prince Badr bin Abdullah bin Farhan, Minister of Culture, Chairman of the Board of Trustees of the King Salman International Complex for the Arabic Language, with this thanks to the Custodian of the Two Holy Mosques and His Highness the Crown Prince - may God preserve them - for their sincere efforts in supporting the Arabic language, serving it, and enhancing its cultural presence and civilized interaction Internationally, pointing out that the sponsorship offered to the essential call for the King Salman International Complex for the Arabic Language, the Kingdom’s international position, and the strengthening of the qualitative presence of our dear country in the field of serving the Arabic language and its leadership at the international level, supporting its presentations, responding to its needs and aspirations, and crowning its achievements.') }}
            </p>
        </div>
    </div>
</div>
</section>

<!-- Section: Mission -->
<section id="mission" class="wow fadeInLeft animation-delay2">
    <div class="container-fluid pt-0 pb-0">
        <div class="row equal-height">
            <div class="col-sm-6 col-md-6 pull-right xs-pull-none bg-theme-colored wow fadeInLeft"
                data-wow-duration="1s" data-wow-delay="0.3s">
                <div class="pt-60 pb-40 pl-90 pr-50 p-md-30">
                    <h2 class="title text-white text-uppercase line-bottom mt-0 mb-0">
                        {{ __('The conference addresses the following topics') }}</h2>
                    {{-- <span style="    background: #F5CC44;
                                                    width: 14%;
                                                    height: 10px;
                                                    display: block;
                                                    color: #F5CC44;
                                                    margin-bottom: 17px;">_____</span> --}}

                    <ul class="text-white" style="list-style-type: circle;">
                        <li class="mb-2">
                            {{ __('Linguistic reality in international organizations, and its strategic importance.') }}
                        </li>
                        <li class="mb-2">
                            {{ __('The civilizational and cultural dimension of multilingualism, and the responsibility of international organizations in this regard.') }}
                        </li>
                        <li class="mb-2">
                            {{ __('The Arabic language in international organizations between difficulties and solutions.') }}
                        </li>
                        <li class="mb-2">
                            {{ __('Translation from and into Arabic in international organizations; Reality and future prospects.') }}
                        </li>
                        <li class="mb-2">
                            {{ __('Initiatives and projects to enable Arabic presence in international organizations') }}
                        </li>
                    </ul>
                    <br>
                    <br>
                    <br>
                    <br>
                </div>
            </div>
            <div class="col-sm-6 col-md-6 p-0 bg-img-cover wow fadeInRight about-first"
                data-bg-img="{{ asset('images/about.jpg') }}" data-wow-duration="1s" data-wow-delay="0.3s">
            </div>
        </div>
    </div>
</section>


<section id="mission">
    <div class="container-fluid pt-0 pb-0">
        <div class="row equal-height">



            <div class="col-sm-6 col-md-6 xs-pull-none bg-theme-colored wow fadeInLeft"
                style="background-color: #4a4a4a !important;" data-wow-duration="1s" data-wow-delay="0.3s">
                <div class="pt-60 pb-40 pl-90 pr-50 p-md-30">
                    <h2 class="title text-white text-uppercase line-bottom mt-0 mb-0">
                        {{ __('Criteria for writing scientific papers') }}</h2>
                    {{-- <span style="    background: #F5CC44;
                                                        width: 14%;
                                                        height: 10px;
                                                        display: block;
                                                        color: #F5CC44;
                                                        margin-bottom: 17px;">_____</span> --}}

                    <ul class="text-white" style="list-style-type: circle;">
                        <li class="mb-2">
                            {{ __('The research should be written in sound Arabic or sound English.') }}
                        </li>
                        <li class="mb-2">
                            {{ __('The research should be in one of the specified axes.') }}</li>
                        <li class="mb-2">
                            {{ __('The researcher must adopt the scientific foundations in all his steps and follow the rules of scientific documentation in the American Psychological Association (APA).') }}

                        </li>
                        <li class="mb-2">
                            {{ __('The number of search words should be between 5000 and 10,000 words.') }}

                        </li>
                        <li class="mb-2">
                            {{ __('The research has not been previously published in any scientific forum or forum.') }}

                        </li>

                        <li class="mb-2">
                            {{ __('The researcher prepares a summary of his research within 200 words in both languages; Arabic English.') }}


                        </li>
                    </ul>

                    <h6 class="text-white"> {{ __('The conference retains copyright') }}</h6>
                    <br>
                    <br><br>
                    <br>
                </div>
            </div>

            <div class="col-sm-6 col-md-6 p-0 bg-img-cover wow fadeInRight about-first"
                data-bg-img="{{ asset('images/ab-2.jpg') }}" data-wow-duration="1s" data-wow-delay="0.3s">
            </div>

        </div>
    </div>
</section>



<!-- Section: teachers -->
<section class="wow fadeInLeft animation-delay3">
    <div class="container pt-70 pb-40">
        <div class="section-title text-center">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <h1 class="mt-0 line-height-1 text-uppercase"> <span class="text-theme-color-2">{{ __('Speakers') }}
                        </span></h1>
                    {{-- <span
                                    style="background: #F5CC44;
                                                width: 14%;
                                                margin: auto;
                                                height: 10px;
                                                display: block; color: #F5CC44; margin-bottom: 20px;text-align: center;">_____</span> --}}
                    {{-- <p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem
                            autem<br> voluptatem obcaecati!</p> --}}
                </div>
            </div>
        </div>
        <div class="row multi-row-clearfix">
            <div class="col-md-12">
                <div class="owl-carousel-3col" data-nav="false">
                    @foreach ($speakers as $speakers)
                    <div class="item">
                        <div class="hover-effect mb-30">
                            <div class="thumb">
                                <img class="img-fullwidth" alt="" src="{{ asset($speakers->image) }}"
                                    style="height: 450px;">
                                <div class="hover-link">
                                    <!-- <ul class="styled-icons icon-dark icon-theme-colored icon-circled icon-sm">
                                                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-skype"></i></a></li>
                                                      </ul> -->
                                </div>
                            </div>
                            <div class="hover-description">
                                <p>{{ \App::getLocale() == 'en' ? $speakers->en->description : $speakers->ar->description }}
                                </p>
                            </div>
                            <div class="details p-15 pt-10 pb-10">
                                <h4 class="title mb-5">
                                    {{ \App::getLocale() == 'en' ? $speakers->en->name : $speakers->ar->name }}
                                </h4>
                                <h5 class="sub-title mt-0 mb-15">
                                    {{ \App::getLocale() == 'en' ? $speakers->en->title : $speakers->ar->title }}
                                </h5>

                            </div>
                        </div>
                    </div>
                    @endforeach





                    <!-- <div class="item">
                                                    <div class="hover-effect mb-30">
                                                        <div class="thumb">
                                                            <img class="img-fullwidth" alt="" src="{{ asset('images/speaker-2.jpg') }}"
                                                                style="height: 350px;">
                                                            <div class="hover-link"> -->


                    <!-- <ul class="styled-icons icon-dark icon-theme-colored icon-circled icon-sm">
                                                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-skype"></i></a></li>
                                                      </ul> -->


                    <!-- </div>
                                                        </div>
                                                        <div class="hover-description">
                                                            <p> Lorem ipsum dolor sit amet consectetur adipisicing elit. Hic ea obcaecati
                                                                voluptate Lorem ipsum dolor sit amet consectetur adipisicing elit. Hic ea
                                                                obcaecati voluptate</p>
                                                        </div>
                                                        <div class="details p-15 pt-10 pb-10">
                                                            <h4 class="title mb-5">Audrey Azoulay</h4>
                                                            <h5 class="sub-title mt-0 mb-15">UNESCO</h5>

                                                        </div>
                                                    </div>
                                                </div> -->

                    <!-- <div class="item">
                                                    <div class="hover-effect mb-30">
                                                        <div class="thumb">
                                                            <img class="img-fullwidth" alt="" src="{{ asset('images/speaker-3.jpg') }}"
                                                                style="height: 350px;">
                                                            <div class="hover-link"> -->


                    <!-- <ul class="styled-icons icon-dark icon-theme-colored icon-circled icon-sm">
                                                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-skype"></i></a></li>
                                                      </ul> -->


                    <!-- </div>
                                                        </div>
                                                        <div class="hover-description">
                                                            <p> Lorem ipsum dolor sit amet consectetur adipisicing elit. Hic ea obcaecati
                                                                voluptate Lorem ipsum dolor sit amet consectetur adipisicing elit. Hic ea
                                                                obcaecati voluptate</p>
                                                        </div>
                                                        <div class="details p-15 pt-10 pb-10">
                                                            <h4 class="title mb-5">Ngozi Okonjo-Iweala</h4>
                                                            <h5 class="sub-title mt-0 mb-15">World Trade Organization</h5>

                                                        </div>
                                                    </div>
                                                </div> -->

                    <!-- <div class="item">
                                                    <div class="hover-effect mb-30">
                                                        <div class="thumb">
                                                            <img class="img-fullwidth" alt="" src="{{ asset('images/speaker-4.jpg') }}"
                                                                style="height: 350px;">
                                                            <div class="hover-link"> -->

                    <!-- <ul class="styled-icons icon-dark icon-theme-colored icon-circled icon-sm">
                                                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-skype"></i></a></li>
                                                      </ul> -->
                    <!--
                                                            </div>
                                                        </div>
                                                        <div class="hover-description">
                                                            <p> Lorem ipsum dolor sit amet consectetur adipisicing elit. Hic ea obcaecati
                                                                voluptate Lorem ipsum dolor sit amet consectetur adipisicing elit. Hic ea
                                                                obcaecati voluptate</p>
                                                        </div>
                                                        <div class="details p-15 pt-10 pb-10">
                                                            <h4 class="title mb-5">Gianni Infantino
                                                            </h4>
                                                            <h5 class="sub-title mt-0 mb-15">FIFA</h5>

                                                        </div>
                                                    </div>
                                                </div> -->

                </div>
            </div>





        </div>
    </div>
</section>

<section class="wow fadeInRight animation-delay3">
    <div class="bg-col" style="background-image: url('{{ asset('images/session-bg.jpeg') }}'); padding: 60px 0;">
        <div class="container">
            <div class="section-title text-center">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <h1 class="mt-0 line-height-1 text-uppercase"> <span
                                class="text-theme-color-2 text-white">{{ __('Sessions') }}
                            </span></h1>
                        {{-- <span
                                    style="background: #F5CC44;
                                                            width: 14%;
                                                            margin: auto;
                                                            height: 10px;
                                                            display: block; color: #F5CC44; margin-bottom: 20px;">_____</span> --}}
                        {{-- <p class="mb-0 text-white">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem
                                autem<br> voluptatem obcaecati!</p> --}}
                    </div>
                </div>
            </div>


            <div class="col-md-12">
                <div class="owl-carousel-3col" data-nav="false">
                    @foreach ($sessions as $session)
                    <div class="item">
                        <div class="hover-effect mb-30">
                            <div class="thumb">
                                <img class="img-fullwidth" alt="" src="{{ asset($session->image) }}"
                                    style="height: 350px;">
                                <div class="hover-link">
                                    <!-- <ul class="styled-icons icon-dark icon-theme-colored icon-circled icon-sm">
                                                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-skype"></i></a></li>
                                                      </ul> -->
                                </div>
                            </div>
                            <div class="hover-description">
                                <p>{{ \App::getLocale() == 'en' ? $session->en->description : $session->ar->description }}
                                </p>
                            </div>
                            <div class="details p-15 pt-10 pb-10">
                                <h4 class="title mb-5">
                                    {{ \App::getLocale() == 'en' ? $session->en->name : $session->ar->name }}
                                </h4>
                                <h5 class="sub-title mt-0 mb-15" style="font-size: 11px;">
                                    {{ \App::getLocale() == 'en' ? $session->en->title : $session->ar->title }}
                                </h5>

                            </div>
                        </div>
                    </div>
                    @endforeach



                    <!-- <div class="item">
                                                    <div class="hover-effect mb-30">
                                                        <div class="thumb">
                                                            <img class="img-fullwidth" alt="" src="{{ asset('images/session-1.jpg') }}"
                                                                style="height: 350px;">
                                                            <div class="hover-link"> -->


                    <!-- <ul class="styled-icons icon-dark icon-theme-colored icon-circled icon-sm">
                                                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-skype"></i></a></li>
                                                      </ul> -->
                    <!-- </div>
                                                        </div>
                                                        <div class="hover-description">
                                                            <p> Lorem ipsum dolor sit amet consectetur adipisicing elit. Hic ea obcaecati
                                                                voluptate Lorem ipsum dolor sit amet consectetur adipisicing elit. Hic ea
                                                                obcaecati voluptate</p>
                                                        </div>
                                                        <div class="details p-15 pt-10 pb-10">
                                                            <h4 class="title mb-5">{{ __('Muhammad bin Abdul Karim Al-Issa') }}</h4>
                                                            <h5 class="sub-title mt-0 mb-15" style="font-size: 11px;">
                                                                {{ __('Secretary-General of the Muslim World League') }}
                                                            </h5>

                                                        </div>
                                                    </div>
                                                </div> -->

                    <!-- <div class="item">
                                                    <div class="hover-effect mb-30">
                                                        <div class="thumb">
                                                            <img class="img-fullwidth" alt="" src="{{ asset('images/session-2.png') }}"
                                                                style="height: 350px;">
                                                            <div class="hover-link"> -->


                    <!-- <ul class="styled-icons icon-dark icon-theme-colored icon-circled icon-sm">
                                                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-skype"></i></a></li>
                                                      </ul> -->

                    <!-- </div>
                                                        </div>
                                                        <div class="hover-description">
                                                            <p> Lorem ipsum dolor sit amet consectetur adipisicing elit. Hic ea obcaecati
                                                                voluptate Lorem ipsum dolor sit amet consectetur adipisicing elit. Hic ea
                                                                obcaecati voluptate</p>
                                                        </div>
                                                        <div class="details p-15 pt-10 pb-10">
                                                            <h4 class="title mb-5">{{ __('Abdullah Al-Moalimi') }}</h4>

                                                            <h5 class="sub-title mt-0 mb-15" style="font-size: 11px;">Saudi Politician
                                                            </h5>

                                                        </div>
                                                    </div>
                                                </div> -->
                    <!-- <div class="item">
                                                    <div class="hover-effect mb-30">
                                                        <div class="thumb">
                                                            <img class="img-fullwidth" alt="" src="{{ asset('images/session-3.jpg') }}"
                                                                style="height: 350px;">
                                                            <div class="hover-link"> -->


                    <!-- <ul class="styled-icons icon-dark icon-theme-colored icon-circled icon-sm">
                                                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-skype"></i></a></li>
                                                      </ul> -->

                    <!-- </div>
                                                        </div>
                                                        <div class="hover-description">
                                                            <p> Lorem ipsum dolor sit amet consectetur adipisicing elit. Hic ea obcaecati
                                                                voluptate Lorem ipsum dolor sit amet consectetur adipisicing elit. Hic ea
                                                                obcaecati voluptate</p>
                                                        </div>
                                                        <div class="details p-15 pt-10 pb-10">
                                                            <h4 class="title mb-5">{{ __('Muhammad Al-Jasser') }}</h4>
                                                            <h5 class="sub-title mt-0 mb-15" style="font-size: 11px;">Former Governor of the
                                                                Saudi Arabian Monetary Agency
                                                            </h5>

                                                        </div>
                                                    </div>
                                                </div> -->

                </div>
            </div>
        </div>
    </div>
</section>

<section class="wow fadeInLeft animation-delay3">
    <div class="container">
        <div class="section-title text-center">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <h1 class="mt-0 line-height-1 text-uppercase"> <span class="text-theme-color-2">{{ __('Writers') }}
                        </span></h1>
                    {{-- <span
                                style="background: #F5CC44;
                                                        width: 14%;
                                                        margin: auto;
                                                        height: 10px;
                                                        display: block; color: #F5CC44; margin-bottom: 20px;text-align: center;">_____</span> --}}
                    {{-- <p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem
                            autem<br> voluptatem obcaecati!</p> --}}
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="owl-carousel-3col" data-nav="false">
                @foreach ($writers as $writers)
                <div class="item">
                    <div class="hover-effect mb-30">
                        <div class="thumb">
                            <img class="img-fullwidth" alt="" src="{{ asset($writers->image) }}" height="350">
                            <div class="hover-link">
                                <!-- <ul class="styled-icons icon-dark icon-theme-colored icon-circled icon-sm">
                                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-skype"></i></a></li>
                                                  </ul> -->
                            </div>
                        </div>
                        <div class="hover-description">
                            <p>{{ \App::getLocale() == 'en' ? $writers->en->description : $writers->ar->description }}
                            </p>
                        </div>
                        <div class="details p-15 pt-10 pb-10">
                            <h4 class="title mb-5">
                                {{ \App::getLocale() == 'en' ? $writers->en->name : $writers->ar->name }}
                            </h4>
                            <h5 class="sub-title mt-0 mb-15">
                                {{ \App::getLocale() == 'en' ? $writers->en->title : $writers->ar->title }}
                            </h5>

                        </div>
                    </div>
                </div>
                @endforeach

                <!-- <div class="item">
                                                <div class="hover-effect mb-30">
                                                    <div class="thumb">
                                                        <img class="img-fullwidth" alt="" src="{{ asset('images/writer-1.jpg') }}" height="350">
                                                        <div class="hover-link"> -->


                <!-- <ul class="styled-icons icon-dark icon-theme-colored icon-circled icon-sm">
                                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-skype"></i></a></li>
                                                  </ul> -->

                <!-- </div>
                                                    </div>
                                                    <div class="hover-description">
                                                        <p>
                                                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Hic ea obcaecati voluptate
                                                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Hic ea obcaecati voluptate
                                                        </p>
                                                    </div>
                                                    <div class="details p-15 pt-10 pb-10">
                                                        <h4 class="title mb-5">{{ __('Muhammad Al-Safi Mosteghanemi') }}
                                                        </h4>
                                                        <h5 class="sub-title mt-0 mb-15">Secretary General of the Sharjah Arabic Language
                                                            Academy
                                                        </h5>

                                                    </div>
                                                </div>
                                            </div> -->
                <!-- <div class="item">
                                                <div class="hover-effect mb-30">
                                                    <div class="thumb">
                                                        <img class="img-fullwidth" alt="" src="{{ asset('images/writer-2.jpg') }}" height="350">
                                                        <div class="hover-link"> -->

                <!-- <ul class="styled-icons icon-dark icon-theme-colored icon-circled icon-sm">
                                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-skype"></i></a></li>
                                                  </ul> -->

                <!-- </div>
                                                    </div>
                                                    <div class="hover-description">
                                                        <p>
                                                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Hic ea obcaecati voluptate
                                                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Hic ea obcaecati voluptate
                                                        </p>
                                                    </div>
                                                    <div class="details p-15 pt-10 pb-10">
                                                        <h4 class="title mb-5">{{ __('Nasser Al-Ghali') }}
                                                        </h4>
                                                        <h5 class="sub-title mt-0 mb-15">Former Dean of the Linguistics Institute at King Saud
                                                            University
                                                        </h5>

                                                    </div>
                                                </div>
                                            </div> -->
                <!-- <div class="item">
                                                <div class="hover-effect mb-30">
                                                    <div class="thumb">
                                                        <img class="img-fullwidth" alt="" src="{{ asset('images/writer-3.jpg') }}" height="350">
                                                        <div class="hover-link"> -->

                <!-- <ul class="styled-icons icon-dark icon-theme-colored icon-circled icon-sm">
                                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-skype"></i></a></li>
                                                  </ul> -->

                <!-- </div>
                                                    </div>
                                                    <div class="hover-description">
                                                        <p>
                                                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Hic ea obcaecati voluptate
                                                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Hic ea obcaecati voluptate
                                                        </p>
                                                    </div>
                                                    <div class="details p-15 pt-10 pb-10">
                                                        <h4 class="title mb-5">{{ __('Saleh Al-Suhaibani') }}
                                                        </h4>
                                                        <h5 class="sub-title mt-0 mb-15">Former Dean of the Institute of Arabic Language
                                                            Teaching at Imam University</h5>

                                                    </div>
                                                </div> -->
            </div>

        </div>
    </div>
    </div>
</section>

<!-- Section: Gallery -->
{{-- <section class="divider parallax layer-overlay overlay-dark-8 wow fadeInUp animation-delay3" data-bg-img="{{ asset('images/bg/bg6.jpg') }}"
id="gallery">
<div class="container pt-60 pb-40">
    <div class="section-title text-center">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <h1 class="mt-0 pb-0 mb-0 text-white text-uppercase">{{__('Scientific')}} <span
                        class="text-theme-color-2" style="color: #f5cc44;">{{__('Committee')}}</span></h1>
                <span style="background: #F5CC44;
                                width: 14%;
                                margin: auto;
                                height: 10px;
                                display: block; color: #F5CC44; margin-bottom: 20px;text-align: center;">_____</span>
            </div>
        </div>
    </div>
    <div class="section-content">
        <div class="row">
            <div class="owl-carousel-3col" data-nav="false">
                <div class="item">
                    <div class="work-gallery">
                        <div class="gallery-thumb">
                            <img class="img-fullwidth" alt="" src="{{ asset('images/writer-4.jpg') }}" height="350">
                            <div class="gallery-overlay"></div>
                            <div class="gallery-contect">

                            </div>
                        </div>
                        <div class="hover-description-2">
                            <p>
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Hic ea obcaecati voluptate
                            </p>
                        </div>
                        <div class="gallery-bottom-part text-center">
                            <h4 class="title text-uppercase font-raleway font-weight-600 m-0">Prof. Abdulaziz Al
                                Hamid</h4>
                            <p>Chairman of the Scientific Committee
                            </p>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="work-gallery">
                        <div class="gallery-thumb">
                            <img class="img-fullwidth" alt="" src="{{ asset('images/writer-5.jpg') }}" height="350">
                            <div class="gallery-overlay"></div>
                            <div class="gallery-contect">

                            </div>
                        </div>
                        <div class="hover-description-2">
                            <p>
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Hic ea obcaecati voluptate
                            </p>
                        </div>
                        <div class="gallery-bottom-part text-center">
                            <h4 class="title text-uppercase font-raleway font-weight-600 m-0">Dr. Majed Al-Hamad
                            </h4>
                            <p>Member of the scientific committee

                            </p>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="work-gallery">
                        <div class="gallery-thumb">
                            <img class="img-fullwidth" alt="" src="{{ asset('images/writer-6.jpg') }}" height="350">
                            <div class="gallery-overlay"></div>
                            <div class="gallery-contect">

                            </div>
                        </div>
                        <div class="hover-description-2">
                            <p>
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Hic ea obcaecati voluptate
                            </p>
                        </div>
                        <div class="gallery-bottom-part text-center">
                            <h4 class="title text-uppercase font-raleway font-weight-600 m-0">Prof. Ibrahim bin
                                Yusuf Al-Balawi
                            </h4>
                            <p>Member of the scientific committee
                            </p>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
</section> --}}
</div>
<!-- end main-content -->

<section class="agenda">

    <div class="container">

        <div class="row">
            <div class="col-md-12">
                <h1 class="mt-0 line-height-1 text-uppercase text-center table-main-text"> <span
                        class="text-theme-color-2"> {{ __('Agenda') }} | {{ __('May 2022') }}
                    </span></h1>
                {{-- <span
                        style="background: #F5CC44;
                                            width: 14%;
                                            margin: auto;
                                            height: 10px;
                                            display: block; color: #F5CC44; margin-bottom: 20px;text-align: center;">_____</span> --}}
            </div>
            {{-- <div class="col-md-6">
                    <h2 class="mt-0 line-height-1 text-uppercase text-center"> <span
                        class="text-theme-color-2 text-white">May - 2022
                    </span></h2>
                <span
                    style="background: #F5CC44;
                                width: 14%;
                                margin: auto;
                                height: 10px;
                                display: block; color: #F5CC44; margin-bottom: 20px;text-align: center;">_____</span>
                </div> --}}
        </div>
        <br>
        <div class="row">
            <div class="col-md-12">

                <div class="agenda-main">
                    <div class="agenda-header">
                        <h1 class="mt-0 line-height-1 text-uppercase text-white m-0"> <span
                                class="text-theme-color-2 text-white">
                                {{ __('Day One') }}
                                <small class="small-text"> {{ __('Wednesday') }} -
                                    {{ __('25 May 2022') }}</small>

                        </h1>
                    </div>
                    @foreach ($DayoneAgenda as $agenda)
                    <?php
                        $western_arabic = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
                        $eastern_arabic = ['٠', '١', '٢', '٣', '٤', '٥', '٦', '٧', '٨', '٩'];
                        ?>
                    @if ($agenda->type == 'Break')
                    <div class="agenda-item">
                        <div class="row">
                            <div class="col-md-12 text-center bg-line">
                                <div class="waviy">
                                    {{-- <span class="fa fa-clock-o"></span> --}}
                                    {{-- <span class="time-dir"
                                                    style="--i:1">
                                                    @if(\App::getLocale() == 'ar')

                                                    {{str_replace($western_arabic, $eastern_arabic, date('g:i', strtotime($agenda->start)))}}
                                    –
                                    {{str_replace($western_arabic, $eastern_arabic, date('g:i', strtotime($agenda->end)))}}
                                    {{-- {{ \App::getLocale()=="en"?Carbon\Carbon::parse($agenda->start)->format('H:i')? }}
                                    --}}
                                    {{-- @else
                                                         {{ date('g:i', strtotime($agenda->start))}} –
                                    {{ date('g:i a', strtotime($agenda->end)) }}

                                    @endif
                                    </span>
                                    <span style="margin-left: 10px;">{{ __('Break') }}</span> --}}
                                    <span class="fa fa-clock-o" style="margin-right: 7px;"></span>
                                    <span class="text">

                                        <i>
                                            @if(\App::getLocale() == 'ar')

                                            {{str_replace($western_arabic, $eastern_arabic, date('g:i', strtotime($agenda->start)))}}
                                            –
                                            {{str_replace($western_arabic, $eastern_arabic, date('g:i', strtotime($agenda->end)))}}
                                            {{-- {{ \App::getLocale()=="en"?Carbon\Carbon::parse($agenda->start)->format('H:i')? }}
                                            --}}
                                            @else
                                            {{ date('g:i', strtotime($agenda->start))}} –
                                            {{ date('g:i a', strtotime($agenda->end)) }}

                                            @endif
                                            {{ __('Break') }}
                                        </i>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    @else
                    <div class="agenda-body">
                        <div class="agenda-item">
                            <div class="row">
                                <div class="col-md-12">
                                    <h4 class="m-0"> <i><i class="fa fa-clock-o"></i>
                                            @if(\App::getLocale() == 'ar')

                                            {{str_replace($western_arabic, $eastern_arabic, date('g:i', strtotime($agenda->start)))}}
                                            –
                                            {{str_replace($western_arabic, $eastern_arabic, date('g:i', strtotime($agenda->end)))}}
                                            {{-- {{ \App::getLocale()=="en"?Carbon\Carbon::parse($agenda->start)->format('H:i')? }}
                                            --}}
                                            @else
                                            {{ date('g:i', strtotime($agenda->start))}} –
                                            {{ date('g:i a', strtotime($agenda->end)) }}

                                            @endif
                                            {{-- {{ Carbon\Carbon::parse($agenda->start)->format('H:i') }} -
                                            {{ Carbon\Carbon::parse($agenda->end)->format('H:i') }} --}}
                                        </i></h4>
                                </div>
                                <div class="col-md-12">
                                    <h4 class="m-0 c-pri">
                                        @if (\App::getLocale()=='ar')
                                        @if ($agenda->type=="Session")
                                        {{"جلسة"}}
                                        @elseif ($agenda->type=="Opening Ceremony")
                                        {{"الافتتاحية"}}
                                        @else
                                        {{"الختام"}}
                                        @endif
                                        @else
                                        {{$agenda->type}}
                                        @endif
                                        <span style="">

                                        </span>
                                        <span style="">
                                            {{ \App::getLocale() == 'en' ? $agenda->title_en : $agenda->title_ar }}</span>
                                    </h4>
                                </div>
                                <div class="col-md-12">
                                    <h4 class="m-0">
                                        {{ \App::getLocale() == 'en' ? $agenda->topic_en : $agenda->topic_ar }}
                                    </h4>
                                </div>

                            </div>
                            <ul style="list-style-type: circle" class="table-ul">

                                <li class="mb-6">
                                    {{ \App::getLocale() == 'en' ? $agenda->desc_en : $agenda->desc_ar }}
                                </li>
                                {{-- <li class="mb-6">
                                        {{ __('A visual presentation on the Kingdoms role in supporting the Arabic language globally.') }}
                                </li>
                                <li class="mb-6">
                                    {{ __('Speech by the heads of organizations invited to speak. (Words will be arranged according to approvals, and the importance of the organization).') }}
                                </li>
                                <li class="mb-6">
                                    {{ __('Speech of His Highness the Minister of Culture (Chairman of the Complexs Board of Trustees)') }}
                                </li>
                                <li class="mb-6">
                                    {{ __('Speech of the Custodian of the Two Holy Mosques (conference sponsor)') }}
                                </li> --}}
                            </ul>


                        </div>

                        <hr>
                    </div>
                    @endif
                    @endforeach

                </div>



                <br>

                <div class="agenda-main">
                    <div class="agenda-header">
                        <h1 class="m-0 line-height-1 text-uppercase text-white "> <span
                                class="text-theme-color-2 text-white">
                                {{ __('Day two') }} <small class="small-text">{{ __('Thursday') }} -
                                    {{ __('26 May 2022') }}</small>
                            </span></h1>
                    </div>
                    @foreach ($DaytwoAgenda as $agenda)
                    @if ($agenda->type == 'Break')
                    <div class="agenda-item">
                        <div class="row">
                            <div class="col-md-12 text-center bg-line">
                                <div class="waviy">
                                    <span class="fa fa-clock-o" style="margin-right: 7px;"></span>
                                    <span class="text">

                                        <i>
                                            @if(\App::getLocale() == 'ar')

                                            {{str_replace($western_arabic, $eastern_arabic, date('g:i', strtotime($agenda->start)))}}
                                            –
                                            {{str_replace($western_arabic, $eastern_arabic, date('g:i', strtotime($agenda->end)))}}
                                            {{-- {{ \App::getLocale()=="en"?Carbon\Carbon::parse($agenda->start)->format('H:i')? }}
                                            --}}
                                            @else
                                            {{ date('g:i', strtotime($agenda->start))}} –
                                            {{ date('g:i a', strtotime($agenda->end)) }}

                                            @endif
                                            {{ __('Break') }}
                                        </i>
                                    </span>

                                </div>
                            </div>
                        </div>
                    </div>
                    @else
                    <div class="agenda-body">
                        <div class="agenda-item">
                            <div class="row">
                                <div class="col-md-12">
                                    <h4 class="m-0"> <i><i class="fa fa-clock-o"></i>
                                            @if(\App::getLocale() == 'ar')

                                            {{str_replace($western_arabic, $eastern_arabic, date('g:i', strtotime($agenda->start)))}}
                                            –
                                            {{str_replace($western_arabic, $eastern_arabic, date('g:i', strtotime($agenda->end)))}}
                                            {{-- {{ \App::getLocale()=="en"?Carbon\Carbon::parse($agenda->start)->format('H:i')? }}
                                            --}}
                                            @else
                                            {{ date('g:i', strtotime($agenda->start))}} –
                                            {{ date('g:i a', strtotime($agenda->end)) }}

                                            @endif
                                            {{-- {{ Carbon\Carbon::parse($agenda->start)->format('H:i') }} -
                                            {{ Carbon\Carbon::parse($agenda->end)->format('H:i') }} --}}
                                        </i></h4>
                                </div>
                                <div class="col-md-12">
                                    <h4 class="m-0 c-pri">
                                        @if (\App::getLocale()=='ar')
                                        @if ($agenda->type=="Session")
                                        {{"جلسة"}}
                                        @elseif ($agenda->type=="Opening Ceremony")
                                        {{"الافتتاحية"}}
                                        @else
                                        {{"الختام"}}
                                        @endif
                                        @else
                                        {{$agenda->type}}
                                        @endif
                                        <span style="">

                                        </span>
                                        <span style="">
                                            {{ \App::getLocale() == 'en' ? $agenda->title_en : $agenda->title_ar }}</span>
                                    </h4>
                                </div>
                                <div class="col-md-12">
                                    <h4 class="m-0">
                                        {{ \App::getLocale() == 'en' ? $agenda->topic_en : $agenda->topic_ar }}
                                    </h4>
                                </div>

                            </div>
                            <ul style="list-style-type: circle" class="table-ul">

                                <li class="mb-6">
                                    {{ \App::getLocale() == 'en' ? $agenda->desc_en : $agenda->desc_ar }}
                                </li>

                            </ul>


                        </div>

                        <hr>
                    </div>
                    @endif
                    @endforeach

                </div>


            </div>

        </div>
    </div>
</section>




<script>
var tab = $('.tab-header li');
var tabData = $('.tab-body .tab-data');

tab.on('click', function() {
    tab.removeClass('active');
    $(this).addClass('active');

    let thisId = $(this).attr('id');
    let thisTabData = $('.tab-body').find(`#data-${thisId}`);

    tabData.removeClass('active');
    thisTabData.addClass('active');
})
</script>
@endsection