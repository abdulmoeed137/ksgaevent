@extends('layouts.app')

@section('content')
    <!-- Start main-content -->
    <div class="main-content">
        <!-- Section: home -->
        <section id="home">

            <!-- Slider Revolution Start -->
            <div class="rev_slider_wrapper">
                <div class="rev_slider" data-version="5.0">
                    <ul>

                        <!-- SLIDE 1 -->
                        <li data-index="rs-1" data-transition="slidingoverlayhorizontal" data-slotamount="default"
                            data-easein="default" data-easeout="default" data-masterspeed="default"
                            data-thumb="{{ asset('images/banner-1.jpg') }}" data-rotate="0" data-saveperformance="off"
                            data-title="Slide 1" data-description="">
                            <!-- MAIN IMAGE -->
                            <img src="{{ asset('images/banner-1.jpg') }}" alt="" data-bgposition="center bottom"
                                data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-bgparallax="10"
                                data-no-retina>
                            <!-- LAYERS -->

                            <!-- LAYER NR. 1 -->
                            <div class="tp-caption tp-resizeme text-uppercase text-white font-raleway" id="rs-1-layer-1"
                                data-x="['left']" data-hoffset="['30']" data-y="['middle']" data-voffset="['-110']"
                                data-fontsize="['100']" data-lineheight="['110']" data-width="none" data-height="none"
                                data-whitespace="nowrap" data-transform_idle="o:1;s:500"
                                data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                                data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                                data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                                data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1000"
                                data-splitin="none" data-splitout="none" data-responsive_offset="on"
                                style="z-index: 7; white-space: nowrap; font-weight:700;">Education
                            </div>

                            <!-- LAYER NR. 2 -->
                            <div class="tp-caption tp-resizeme text-uppercase text-white font-raleway bg-theme-colored-transparent border-left-theme-color-2-6px pl-20 pr-20"
                                id="rs-1-layer-2" data-x="['left']" data-hoffset="['35']" data-y="['middle']"
                                data-voffset="['-25']" data-fontsize="['35']" data-lineheight="['54']" data-width="none"
                                data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;s:500"
                                data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                                data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                                data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                                data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1000"
                                data-splitin="none" data-splitout="none" data-responsive_offset="on"
                                style="z-index: 7; white-space: nowrap; font-weight:600;">King Salman Global Academy
                            </div>

                            <!-- LAYER NR. 3 -->
                            <div class="tp-caption tp-resizeme text-white" id="rs-1-layer-3" data-x="['left']"
                                data-hoffset="['35']" data-y="['middle']" data-voffset="['35']" data-fontsize="['16']"
                                data-lineheight="['28']" data-width="none" data-height="none" data-whitespace="nowrap"
                                data-transform_idle="o:1;s:500" data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                                data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                                data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                                data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1400"
                                data-splitin="none" data-splitout="none" data-responsive_offset="on"
                                style="z-index: 5; white-space: nowrap; letter-spacing:0px; font-weight:400;">We provides
                                always our best services for our clients and always<br> try to achieve our client's trust
                                and satisfaction.
                            </div>

                            <!-- LAYER NR. 4 -->
                            <div class="tp-caption tp-resizeme" id="rs-1-layer-4" data-x="['left']" data-hoffset="['35']"
                                data-y="['middle']" data-voffset="['100']" data-width="none" data-height="none"
                                data-whitespace="nowrap" data-transform_idle="o:1;"
                                data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
                                data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                                data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;"
                                data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1400"
                                data-splitin="none" data-splitout="none" data-responsive_offset="on"
                                style="z-index: 5; white-space: nowrap; letter-spacing:1px;"><a
                                    class="btn btn-colored btn-lg btn-flat btn-theme-colored border-left-theme-color-2-6px pl-20 pr-20"
                                    href="#">View Details</a>
                            </div>
                        </li>

                        <!-- SLIDE 2 -->
                        <li data-index="rs-2" data-transition="slidingoverlayhorizontal" data-slotamount="default"
                            data-easein="default" data-easeout="default" data-masterspeed="default"
                            data-thumb="{{ asset('images/banner-2.jpg') }}" data-rotate="0" data-saveperformance="off"
                            data-title="Slide 2" data-description="">
                            <!-- MAIN IMAGE -->
                            <img src="{{ asset('images/banner-2.jpg') }}" alt="" data-bgposition="center bottom"
                                data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-bgparallax="10"
                                data-no-retina>
                            <!-- LAYERS -->

                            <!-- LAYER NR. 1 -->
                            <div class="tp-caption tp-resizeme text-uppercase  bg-theme-colored-transparent text-white font-raleway border-left-theme-color-2-6px border-right-theme-color-2-6px pl-30 pr-30"
                                id="rs-2-layer-1" data-x="['center']" data-hoffset="['0']" data-y="['middle']"
                                data-voffset="['-90']" data-fontsize="['28']" data-lineheight="['54']" data-width="none"
                                data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;s:500"
                                data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                                data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                                data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                                data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1000"
                                data-splitin="none" data-splitout="none" data-responsive_offset="on"
                                style="z-index: 7; white-space: nowrap; font-weight:400; border-radius: 30px;">King Salman
                                Global Academy
                            </div>

                            <!-- LAYER NR. 2 -->
                            <div class="tp-caption tp-resizeme text-uppercase bg-theme-colored-transparent text-white font-raleway pl-30 pr-30"
                                id="rs-2-layer-2" data-x="['center']" data-hoffset="['0']" data-y="['middle']"
                                data-voffset="['-20']" data-fontsize="['48']" data-lineheight="['70']" data-width="none"
                                data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;s:500"
                                data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                                data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                                data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                                data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1000"
                                data-splitin="none" data-splitout="none" data-responsive_offset="on"
                                style="z-index: 7; white-space: nowrap; font-weight:700; border-radius: 30px;">King Salman
                                Global Academy
                            </div>

                            <!-- LAYER NR. 3 -->
                            <div class="tp-caption tp-resizeme text-white text-center" id="rs-2-layer-3" data-x="['center']"
                                data-hoffset="['0']" data-y="['middle']" data-voffset="['50']" data-fontsize="['16']"
                                data-lineheight="['28']" data-width="none" data-height="none" data-whitespace="nowrap"
                                data-transform_idle="o:1;s:500" data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                                data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                                data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                                data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1400"
                                data-splitin="none" data-splitout="none" data-responsive_offset="on"
                                style="z-index: 5; white-space: nowrap; letter-spacing:0px; font-weight:400;">We provides
                                always our best services for our clients and always<br> try to achieve our client's trust
                                and satisfaction.
                            </div>

                            <!-- LAYER NR. 4 -->
                            <div class="tp-caption tp-resizeme" id="rs-2-layer-4" data-x="['center']" data-hoffset="['0']"
                                data-y="['middle']" data-voffset="['115']" data-width="none" data-height="none"
                                data-whitespace="nowrap" data-transform_idle="o:1;"
                                data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
                                data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                                data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;"
                                data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1400"
                                data-splitin="none" data-splitout="none" data-responsive_offset="on"
                                style="z-index: 5; white-space: nowrap; letter-spacing:1px;"><a
                                    class="btn btn-default btn-circled btn-transparent pl-20 pr-20" href="#">Apply Now</a>
                            </div>
                        </li>

                        <!-- SLIDE 3 -->
                        <li data-index="rs-3" data-transition="slidingoverlayhorizontal" data-slotamount="default"
                            data-easein="default" data-easeout="default" data-masterspeed="default"
                            data-thumb="{{ asset('images/banner-3.jpg') }}" data-rotate="0" data-saveperformance="off"
                            data-title="Slide 3" data-description="">
                            <!-- MAIN IMAGE -->
                            <img src="{{ asset('images/banner-3.jpg') }}" alt="" data-bgposition="center top"
                                data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-bgparallax="10"
                                data-no-retina>
                            <!-- LAYERS -->

                            <!-- LAYER NR. 1 -->
                            <div class="tp-caption tp-resizeme text-uppercase text-white font-raleway bg-theme-colored-transparent border-right-theme-color-2-6px pr-20 pl-20"
                                id="rs-3-layer-1" data-x="['right']" data-hoffset="['30']" data-y="['middle']"
                                data-voffset="['-90']" data-fontsize="['64']" data-lineheight="['72']" data-width="none"
                                data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;s:500"
                                data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                                data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                                data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                                data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1000"
                                data-splitin="none" data-splitout="none" data-responsive_offset="on"
                                style="z-index: 7; white-space: nowrap; font-weight:600;">King Salman Global Academy
                            </div>

                            <!-- LAYER NR. 2 -->
                            <div class="tp-caption tp-resizeme text-uppercase text-white font-raleway" id="rs-3-layer-2"
                                data-x="['right']" data-hoffset="['35']" data-y="['middle']" data-voffset="['-25']"
                                data-fontsize="['32']" data-lineheight="['54']" data-width="none" data-height="none"
                                data-whitespace="nowrap" data-transform_idle="o:1;s:500"
                                data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                                data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                                data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                                data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1000"
                                data-splitin="none" data-splitout="none" data-responsive_offset="on"
                                style="z-index: 7; white-space: nowrap; font-weight:600;">For Your Better Future
                            </div>

                            <!-- LAYER NR. 3 -->
                            <div class="tp-caption tp-resizeme text-white text-right" id="rs-3-layer-3" data-x="['right']"
                                data-hoffset="['35']" data-y="['middle']" data-voffset="['30']" data-fontsize="['16']"
                                data-lineheight="['28']" data-width="none" data-height="none" data-whitespace="nowrap"
                                data-transform_idle="o:1;s:500" data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                                data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                                data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                                data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1400"
                                data-splitin="none" data-splitout="none" data-responsive_offset="on"
                                style="z-index: 5; white-space: nowrap; letter-spacing:0px; font-weight:400;">We provides
                                always our best services for our clients and always<br> try to achieve our client's trust
                                and satisfaction.
                            </div>

                            <!-- LAYER NR. 4 -->
                            <div class="tp-caption tp-resizeme" id="rs-3-layer-4" data-x="['right']" data-hoffset="['35']"
                                data-y="['middle']" data-voffset="['95']" data-width="none" data-height="none"
                                data-whitespace="nowrap" data-transform_idle="o:1;"
                                data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
                                data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                                data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;"
                                data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1400"
                                data-splitin="none" data-splitout="none" data-responsive_offset="on"
                                style="z-index: 5; white-space: nowrap; letter-spacing:1px;"><a
                                    class="btn btn-colored btn-lg btn-flat btn-theme-colored btn-theme-colored border-right-theme-color-2-6px pl-20 pr-20"
                                    href="#">Apply Now</a>
                            </div>
                        </li>

                    </ul>
                </div>
                <!-- end .rev_slider -->
            </div>
            <!-- end .rev_slider_wrapper -->
            <script>
                $(document).ready(function(e) {
                    $(".rev_slider").revolution({
                        sliderType: "standard",
                        sliderLayout: "auto",
                        dottedOverlay: "none",
                        delay: 5000,
                        navigation: {
                            keyboardNavigation: "off",
                            keyboard_direction: "horizontal",
                            mouseScrollNavigation: "off",
                            onHoverStop: "off",
                            touch: {
                                touchenabled: "on",
                                swipe_threshold: 75,
                                swipe_min_touches: 1,
                                swipe_direction: "horizontal",
                                drag_block_vertical: false
                            },
                            arrows: {
                                style: "zeus",
                                enable: true,
                                hide_onmobile: true,
                                hide_under: 600,
                                hide_onleave: true,
                                hide_delay: 200,
                                hide_delay_mobile: 1200,
                                tmp: '<div class="tp-title-wrap">    <div class="tp-arr-imgholder"></div> </div>',
                                left: {
                                    h_align: "left",
                                    v_align: "center",
                                    h_offset: 30,
                                    v_offset: 0
                                },
                                right: {
                                    h_align: "right",
                                    v_align: "center",
                                    h_offset: 30,
                                    v_offset: 0
                                }
                            },
                            bullets: {
                                enable: true,
                                hide_onmobile: true,
                                hide_under: 600,
                                style: "metis",
                                hide_onleave: true,
                                hide_delay: 200,
                                hide_delay_mobile: 1200,
                                direction: "horizontal",
                                h_align: "center",
                                v_align: "bottom",
                                h_offset: 0,
                                v_offset: 30,
                                space: 5,
                                tmp: '<span class="tp-bullet-img-wrap">  <span class="tp-bullet-image"></span></span><span class="tp-bullet-title"></span>'
                            }
                        },
                        responsiveLevels: [1240, 1024, 778],
                        visibilityLevels: [1240, 1024, 778],
                        gridwidth: [1170, 1024, 778, 480],
                        gridheight: [650, 768, 960, 720],
                        lazyType: "none",
                        parallax: {
                            origo: "slidercenter",
                            speed: 1000,
                            levels: [5, 10, 15, 20, 25, 30, 35, 40, 45, 46, 47, 48, 49, 50, 100, 55],
                            type: "scroll"
                        },
                        shadow: 0,
                        spinner: "off",
                        stopLoop: "on",
                        stopAfterLoops: 0,
                        stopAtSlide: -1,
                        shuffle: "off",
                        autoHeight: "off",
                        fullScreenAutoWidth: "off",
                        fullScreenAlignForce: "off",
                        fullScreenOffsetContainer: "",
                        fullScreenOffset: "0",
                        hideThumbsOnMobile: "off",
                        hideSliderAtLimit: 0,
                        hideCaptionAtLimit: 0,
                        hideAllCaptionAtLilmit: 0,
                        debugMode: false,
                        fallbacks: {
                            simplifyAll: "off",
                            nextSlideOnWindowFocus: "off",
                            disableFocusListener: false,
                        }
                    });
                });
            </script>
            <!-- Slider Revolution Ends -->

        </section>

        <!-- Section: welcome -->
        <section>
            <div class="container pt-0 pb-0">
                <div class="section-content">
                    <div class="row equal-height-inner mt-sm-0" data-margin-top="-150px">
                        <div
                            class="col-md-offset-2 col-sm-12 col-md-8 pr-0 pr-sm-15 sm-height-auto mt-sm-0 wow fadeInLeft animation-delay1">
                            <div class="sm-height-auto aboutImage" data-bg-img="{{ asset('images/timer.jpg') }}"
                                style="background-size: cover;background-position: center;">
                                <div class="p-20" style="position: relative;">
                                    <h2 class="text-white text-uppercase text-center ">Conference Date</h2>
                                    <div class="clearfix">
                                    </div>
                                    <h1 class="text-white text-center m-0" id="demo"></h1>
                                    <h2 class="text-white text-center m-0">Days : hours : Mins : secs</h2>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>

        <section class="about-main" style="background-image: url({{ asset('images/about.jpg') }})">
          <div class="container">
              <div class="row">
                <div class="col-md-1"></div>
                <div class=" col-md-10">
                  <h1 class="title text-white text-uppercase line-bottom mt-0 mb-0 text-center">About Us</h1>
                  <br>
                  <p class="text-justify">
                      Under the patronage of the Custodian of the Two Holy Mosques King Salman bin Abdulaziz Al Saud - may God
                      protect him - the city of Riyadh will host the “Arabic Language in International Organizations” conference,
                      which is organized by the King Salman International Complex for the Arabic Language in the first half of
                      2022.
                      His Highness Prince Badr bin Abdullah bin Farhan, Minister of Culture, Chairman of the Board of Trustees of
                      the King Salman International Complex for the Arabic Language, with this thanks to the Custodian of the Two
                      Holy Mosques and His Highness the Crown Prince - may God preserve them - for their sincere efforts in
                      supporting the Arabic language, serving it, and enhancing its cultural presence and civilized interaction
                      Internationally, pointing out that the sponsorship offered to the essential call for the King Salman
                      International Complex for the Arabic Language, the Kingdom’s international position, and the strengthening
                      of the qualitative presence of our dear country in the field of serving the Arabic language and its
                      leadership at the international level, supporting its presentations, responding to its needs and
                      aspirations, and crowning its achievements.
      
                  </p>  
                </div>
              </div>
          </div>
        </section>

        <!-- Section: Mission -->
        <section id="mission">
            <div class="container-fluid pt-0 pb-0">
                <div class="row equal-height">
                    <div class="col-sm-6 col-md-6 pull-right xs-pull-none bg-theme-colored wow fadeInLeft"
                        data-wow-duration="1s" data-wow-delay="0.3s">
                        <div class="pt-60 pb-40 pl-90 pr-50 p-md-30">
                            <h2 class="title text-white text-uppercase line-bottom mt-0 mb-0">The conference addresses the
                                following topics</h2>

                            <ul class="text-white" style="list-style-type: circle;">
                                <li class="mb-2"> Linguistic reality in international organizations, and its
                                    strategic importance.</li>
                                <li class="mb-2">The civilizational and cultural dimension of multilingualism, and
                                    the responsibility of international organizations in this regard.</li>
                                <li class="mb-2"> The Arabic language in international organizations between
                                    difficulties and solutions.
                                </li>
                                <li class="mb-2"> Translation from and into Arabic in international organizations;
                                    Reality and future prospects.
                                </li>
                                <li class="mb-2"> Initiatives and projects to enable Arabic presence in
                                    international organizations
                                </li>
                            </ul>

                        </div>
                    </div>
                    <div class="col-sm-6 col-md-6 p-0 bg-img-cover wow fadeInRight hidden-xs"
                        data-bg-img="{{ asset('images/photos/about-1.jpg') }}" data-wow-duration="1s" data-wow-delay="0.3s">
                    </div>
                </div>
            </div>
        </section>


        <section id="mission">
            <div class="container-fluid pt-0 pb-0">
                <div class="row equal-height">



                    <div class="col-sm-6 col-md-6 xs-pull-none bg-theme-colored wow fadeInLeft"
                        style="background-color: #4a4a4a !important;" data-wow-duration="1s" data-wow-delay="0.3s">
                        <div class="pt-60 pb-40 pl-90 pr-50 p-md-30">
                            <h2 class="title text-white text-uppercase line-bottom mt-0 mb-0">Criteria for writing
                                scientific papers</h2>

                            <ul class="text-white" style="list-style-type: circle;">
                                <li class="mb-2"> The research should be written in sound Arabic or sound English.
                                </li>
                                <li class="mb-2"> The research should be in one of the specified axes.</li>
                                <li class="mb-2"> The researcher must adopt the scientific foundations in all his
                                    steps and follow the rules of scientific documentation in the American Psychological
                                    Association (APA).

                                </li>
                                <li class="mb-2"> The number of search words should be between 5000 and 10,000
                                    words.

                                </li>
                                <li class="mb-2"> The research has not been previously published in any scientific
                                    forum or forum.

                                </li>

                                <li class="mb-2"> The researcher prepares a summary of his research within 200
                                    words in both languages; Arabic English.


                                </li>
                            </ul>

                            <h6 class="text-white"> The conference retains copyright</h6>

                        </div>
                    </div>

                    <div class="col-sm-6 col-md-6 p-0 bg-img-cover wow fadeInRight hidden-xs"
                        data-bg-img="{{ asset('images/photos/about-2.jpg') }}" data-wow-duration="1s" data-wow-delay="0.3s">
                    </div>

                </div>
            </div>
        </section>



        <!-- Section: teachers -->
        <section>
            <div class="container pt-70 pb-40">
                <div class="section-title text-center">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <h1 class="mt-0 line-height-1 text-uppercase"> <span class="text-theme-color-2">Speakers
                                </span></h1>
                            <p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem
                                autem<br> voluptatem obcaecati!</p>
                        </div>
                    </div>
                </div>
                <div class="row multi-row-clearfix">
                    <div class="col-md-12">
                        <div class="owl-carousel-4col" data-nav="false">
                            <div class="item">
                                <div class="hover-effect mb-30">
                                    <div class="thumb">
                                        <img class="img-fullwidth" alt="" src="{{ asset('images/speaker-1.jpg') }}"
                                            style="height: 500px;">
                                        <div class="hover-link">
                                            <!-- <ul class="styled-icons icon-dark icon-theme-colored icon-circled icon-sm">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-skype"></i></a></li>
                          </ul> -->
                                        </div>
                                    </div>
                                    <div class="details p-15 pt-10 pb-10">
                                        <h4 class="title mb-5">António Guterres</h4>
                                        <h5 class="sub-title mt-0 mb-15">UN</h5>
                                        <a class="btn btn-theme-colored btn-sm" href="page-doctor-details.html">view
                                            details</a>
                                    </div>
                                </div>
                            </div>


                            <div class="item">
                                <div class="hover-effect mb-30">
                                    <div class="thumb">
                                        <img class="img-fullwidth" alt="" src="{{ asset('images/speaker-2.jpg') }}"
                                            style="height: 500px;">
                                        <div class="hover-link">
                                            <!-- <ul class="styled-icons icon-dark icon-theme-colored icon-circled icon-sm">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-skype"></i></a></li>
                          </ul> -->
                                        </div>
                                    </div>
                                    <div class="details p-15 pt-10 pb-10">
                                        <h4 class="title mb-5">Audrey Azoulay</h4>
                                        <h5 class="sub-title mt-0 mb-15">UNESCO</h5>
                                        <a class="btn btn-theme-colored btn-sm" href="page-doctor-details.html">view
                                            details</a>
                                    </div>
                                </div>
                            </div>

                            <div class="item">
                                <div class="hover-effect mb-30">
                                    <div class="thumb">
                                        <img class="img-fullwidth" alt="" src="{{ asset('images/speaker-3.jpg') }}"
                                            style="height: 500px;">
                                        <div class="hover-link">
                                            <!-- <ul class="styled-icons icon-dark icon-theme-colored icon-circled icon-sm">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-skype"></i></a></li>
                          </ul> -->
                                        </div>
                                    </div>
                                    <div class="details p-15 pt-10 pb-10">
                                        <h4 class="title mb-5">Ngozi Okonjo-Iweala</h4>
                                        <h5 class="sub-title mt-0 mb-15">World Trade Organization</h5>
                                        <a class="btn btn-theme-colored btn-sm" href="page-doctor-details.html">view
                                            details</a>
                                    </div>
                                </div>
                            </div>

                            <div class="item">
                                <div class="hover-effect mb-30">
                                    <div class="thumb">
                                        <img class="img-fullwidth" alt="" src="{{ asset('images/speaker-4.jpg') }}"
                                            style="height: 500px;">
                                        <div class="hover-link">
                                            <!-- <ul class="styled-icons icon-dark icon-theme-colored icon-circled icon-sm">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-skype"></i></a></li>
                          </ul> -->
                                        </div>
                                    </div>
                                    <div class="details p-15 pt-10 pb-10">
                                        <h4 class="title mb-5">Gianni Infantino
                                        </h4>
                                        <h5 class="sub-title mt-0 mb-15">FIFA</h5>
                                        <a class="btn btn-theme-colored btn-sm" href="page-doctor-details.html">view
                                            details</a>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="section-title text-center">
                        <div class="row">
                            <div class="col-md-8 col-md-offset-2">
                                <h1 class="mt-0 line-height-1 text-uppercase"> <span class="text-theme-color-2">Sessions
                                    </span></h1>
                                <p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem
                                    autem<br> voluptatem obcaecati!</p>
                            </div>
                        </div>
                    </div>


                    <div class="col-md-12">
                        <div class="owl-carousel-3col" data-nav="false">
                            <div class="item">
                                <div class="hover-effect mb-30">
                                    <div class="thumb">
                                        <img class="img-fullwidth" alt="" src="{{ asset('images/session-1.jpg') }}"
                                            style="height: 350px;">
                                        <div class="hover-link">
                                            <!-- <ul class="styled-icons icon-dark icon-theme-colored icon-circled icon-sm">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-skype"></i></a></li>
                          </ul> -->
                                        </div>
                                    </div>
                                    <div class="details p-15 pt-10 pb-10">
                                        <h4 class="title mb-5">Muhammad bin Abdulaziz Al-Issa
                                        </h4>
                                        <h5 class="sub-title mt-0 mb-15" style="font-size: 11px;">Secretary-General of the
                                            Muslim World League
                                        </h5>
                                        <a class="btn btn-theme-colored btn-sm" href="page-doctor-details.html">view
                                            details</a>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="hover-effect mb-30">
                                    <div class="thumb">
                                        <img class="img-fullwidth" alt="" src="{{ asset('images/session-2.png') }}"
                                            style="height: 350px;">
                                        <div class="hover-link">
                                            <!-- <ul class="styled-icons icon-dark icon-theme-colored icon-circled icon-sm">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-skype"></i></a></li>
                          </ul> -->
                                        </div>
                                    </div>
                                    <div class="details p-15 pt-10 pb-10">
                                        <h4 class="title mb-5">Abdallah Al-Mouallimi
                                        </h4>

                                        <h5 class="sub-title mt-0 mb-15" style="font-size: 11px;">Saudi Politician
                                        </h5>
                                        <a class="btn btn-theme-colored btn-sm" href="page-doctor-details.html">view
                                            details</a>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="hover-effect mb-30">
                                    <div class="thumb">
                                        <img class="img-fullwidth" alt="" src="{{ asset('images/session-3.jpg') }}"
                                            style="height: 350px;">
                                        <div class="hover-link">
                                            <!-- <ul class="styled-icons icon-dark icon-theme-colored icon-circled icon-sm">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-skype"></i></a></li>
                          </ul> -->
                                        </div>
                                    </div>
                                    <div class="details p-15 pt-10 pb-10">
                                        <h4 class="title mb-5">Muhammed Al-Jasser
                                        </h4>
                                        <h5 class="sub-title mt-0 mb-15" style="font-size: 11px;">Former Governor of the
                                            Saudi Arabian Monetary Agency
                                        </h5>
                                        <a class="btn btn-theme-colored btn-sm" href="page-doctor-details.html">view
                                            details</a>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>


                    <div class="section-title text-center">
                        <div class="row">
                            <div class="col-md-8 col-md-offset-2">
                                <h1 class="mt-0 line-height-1 text-uppercase"> <span class="text-theme-color-2">Writers
                                    </span></h1>
                                <p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem
                                    autem<br> voluptatem obcaecati!</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="owl-carousel-3col" data-nav="false">
                            <div class="item">
                                <div class="hover-effect mb-30">
                                    <div class="thumb">
                                        <img class="img-fullwidth" alt="" src="{{ asset('images/writer-1.jpg') }}"
                                            height="350">
                                        <div class="hover-link">
                                            <!-- <ul class="styled-icons icon-dark icon-theme-colored icon-circled icon-sm">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-skype"></i></a></li>
                          </ul> -->
                                        </div>
                                    </div>
                                    <div class="details p-15 pt-10 pb-10">
                                        <h4 class="title mb-5">Mohamed Al-Safi Mosteghanemi
                                        </h4>
                                        <h5 class="sub-title mt-0 mb-15">Secretary General of the Sharjah Arabic Language
                                            Academy
                                        </h5>
                                        <a class="btn btn-theme-colored btn-sm" href="page-doctor-details.html">view
                                            details</a>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="hover-effect mb-30">
                                    <div class="thumb">
                                        <img class="img-fullwidth" alt="" src="{{ asset('images/writer-2.jpg') }}"
                                            height="350">
                                        <div class="hover-link">
                                            <!-- <ul class="styled-icons icon-dark icon-theme-colored icon-circled icon-sm">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-skype"></i></a></li>
                          </ul> -->
                                        </div>
                                    </div>
                                    <div class="details p-15 pt-10 pb-10">
                                        <h4 class="title mb-5">Mr. Nasser Alghali
                                        </h4>
                                        <h5 class="sub-title mt-0 mb-15">Former Dean of the Linguistics Institute at King
                                            Saud University
                                        </h5>
                                        <a class="btn btn-theme-colored btn-sm" href="page-doctor-details.html">view
                                            details</a>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="hover-effect mb-30">
                                    <div class="thumb">
                                        <img class="img-fullwidth" alt="" src="{{ asset('images/writer-3.jpg') }}"
                                            height="350">
                                        <div class="hover-link">
                                            <!-- <ul class="styled-icons icon-dark icon-theme-colored icon-circled icon-sm">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-skype"></i></a></li>
                          </ul> -->
                                        </div>
                                    </div>
                                    <div class="details p-15 pt-10 pb-10">
                                        <h4 class="title mb-5">Mr. Saleh bin Nasser Al Shuwairekh
                                        </h4>
                                        <h5 class="sub-title mt-0 mb-15">Former Dean of the Institute of Arabic Language
                                            Teaching at Imam University</h5>
                                        <a class="btn btn-theme-colored btn-sm" href="page-doctor-details.html">view
                                            details</a>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </section>

        <!-- Section: Gallery -->
        <section class="divider parallax layer-overlay overlay-dark-8" data-bg-img="{{ asset('images/bg/bg6.jpg') }}"
            id="gallery">
            <div class="container pt-60 pb-40">
                <div class="section-title text-center">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <h1 class="mt-0 pb-0 mb-0 text-white text-uppercase">Scientific <span
                                    class="text-theme-color-2">Committee</span></h1>
                        </div>
                    </div>
                </div>
                <div class="section-content">
                    <div class="row">
                        <!-- Gallery Grid -->
                        <div class="owl-carousel-3col" data-nav="false">
                            <div class="item">
                                <div class="work-gallery">
                                    <div class="gallery-thumb">
                                        <img class="img-fullwidth" alt="" src="{{ asset('images/writer-4.jpg') }}"
                                            height="350">
                                        <div class="gallery-overlay"></div>
                                        <div class="gallery-contect">
                                            <!-- <ul class="styled-icons icon-bordered icon-circled icon-sm">
                            <li><a href="#"><i class="fa fa-link"></i></a></li>
                            <li><a data-rel="prettyPhoto" href="images/gallery/1.jpg"><i class="fa fa-arrows"></i></a></li>
                          </ul> -->
                                        </div>
                                    </div>
                                    <div class="gallery-bottom-part text-center">
                                        <h4 class="title text-uppercase font-raleway font-weight-600 m-0">Prof. Abdulaziz
                                            Al Hamid</h4>
                                        <p>Chairman of the Scientific Committee
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="work-gallery">
                                    <div class="gallery-thumb">
                                        <img class="img-fullwidth" alt="" src="{{ asset('images/writer-5.jpg') }}"
                                            height="350">
                                        <div class="gallery-overlay"></div>
                                        <div class="gallery-contect">
                                            <!-- <ul class="styled-icons icon-bordered icon-circled icon-sm">
                            <li><a href="#"><i class="fa fa-link"></i></a></li>
                            <li><a data-rel="prettyPhoto" href="images/gallery/2.jpg"><i class="fa fa-arrows"></i></a></li>
                          </ul> -->
                                        </div>
                                    </div>
                                    <div class="gallery-bottom-part text-center">
                                        <h4 class="title text-uppercase font-raleway font-weight-600 m-0">Dr. Majed
                                            Al-Hamad
                                        </h4>
                                        <p>Member of the scientific committee

                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="work-gallery">
                                    <div class="gallery-thumb">
                                        <img class="img-fullwidth" alt="" src="{{ asset('images/writer-6.jpg') }}"
                                            height="350">
                                        <div class="gallery-overlay"></div>
                                        <div class="gallery-contect">
                                            <!-- <ul class="styled-icons icon-bordered icon-circled icon-sm">
                            <li><a href="#"><i class="fa fa-link"></i></a></li>
                            <li><a data-rel="prettyPhoto" href="images/gallery/3.jpg"><i class="fa fa-arrows"></i></a></li>
                          </ul> -->
                                        </div>
                                    </div>
                                    <div class="gallery-bottom-part text-center">
                                        <h4 class="title text-uppercase font-raleway font-weight-600 m-0">Prof. Ibrahim bin
                                            Yusuf Al-Balawi
                                        </h4>
                                        <p>Member of the scientific committee
                                        </p>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- End Gallery Grid -->
                    </div>
                </div>
            </div>
        </section>

        <!-- Section: Why Choose Us -->


        <!-- Section: events -->
        <!-- <section id="events" class="divider parallax layer-overlay overlay-dark-8" data-stellar-background-ratio="0.5" data-bg-img="images/bg/bg6.jpg">
          <div class="container pt-70 pb-40">
            <div class="section-title mb-30">
              <div class="row">
                <div class="col-md-6 col-md-offset-3 text-center">
                  <h2 class="mt-0 line-height-1 text-center mb-10 text-white text-uppercase">Upcoming Events</h2>
                  <p class="text-white mb-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem autem<br> voluptatem obcaecati!</p>
                </div>
              </div>
            </div>
            <div class="section-content">
              <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-6 mb-30 wow fadeInRight" data-wow-duration="1s" data-wow-delay="0.5s">
                  <div class="pricing table-horizontal maxwidth400">
                    <div class="row">
                      <div class="col-md-6">
                        <div class="thumb">
                        <img class="img-fullwidth mb-sm-0" src="images/about/as7.jpg" alt="">
                        </div>
                      </div>
                      <div class="col-md-6 p-30 pl-sm-50">
                        <h4 class="mt-0 mb-5"><a href="#" class="text-white">Upcoming Events Title</a></h4>
                        <ul class="list-inline mb-5 text-white">
                          <li class="pr-0"><i class="fa fa-calendar mr-5"></i> June 26, 2016 |</li>
                          <li class="pl-5"><i class="fa fa-map-marker mr-5"></i>New York</li>
                        </ul>
                        <p class="mb-15 mt-15 text-white">Lorem ipsum dolor sit amet, consectetur adi isicing elit. Quas eveniet, nemo dicta. Ullam nam.</p>
                        <a class="text-white font-weight-600" href="#">Read More →</a>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6 mb-30 wow fadeInRight" data-wow-duration="1s" data-wow-delay="0.5s">
                  <div class="pricing table-horizontal maxwidth400">
                    <div class="row">
                      <div class="col-md-6">
                        <div class="thumb">
                        <img class="img-fullwidth mb-sm-0" src="images/about/as8.jpg" alt="">
                        </div>
                      </div>
                      <div class="col-md-6 p-30 pl-sm-50">
                        <h4 class="mt-0 mb-5"><a href="#" class="text-white">Upcoming Events Title</a></h4>
                        <ul class="list-inline mb-5 text-white">
                          <li class="pr-0"><i class="fa fa-calendar mr-5"></i> June 26, 2016 |</li>
                          <li class="pl-5"><i class="fa fa-map-marker mr-5"></i>New York</li>
                        </ul>
                        <p class="mb-15 mt-15 text-white">Lorem ipsum dolor sit amet, consectetur adi isicing elit. Quas eveniet, nemo dicta. Ullam nam.</p>
                        <a class="text-white font-weight-600" href="#">Read More →</a>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6 mb-30 wow fadeInRight" data-wow-duration="1s" data-wow-delay="0.5s">
                  <div class="pricing table-horizontal maxwidth400">
                    <div class="row">
                      <div class="col-md-6">
                        <div class="thumb">
                        <img class="img-fullwidth mb-sm-0" src="images/about/as9.jpg" alt="">
                        </div>
                      </div>
                      <div class="col-md-6 p-30 pl-sm-50">
                        <h4 class="mt-0 mb-5"><a href="#" class="text-white">Upcoming Events Title</a></h4>
                        <ul class="list-inline mb-5 text-white">
                          <li class="pr-0"><i class="fa fa-calendar mr-5"></i> June 26, 2016 |</li>
                          <li class="pl-5"><i class="fa fa-map-marker mr-5"></i>New York</li>
                        </ul>
                        <p class="mb-15 mt-15 text-white">Lorem ipsum dolor sit amet, consectetur adi isicing elit. Quas eveniet, nemo dicta. Ullam nam.</p>
                        <a class="text-white font-weight-600" href="#">Read More →</a>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6 mb-30 wow fadeInRight" data-wow-duration="1s" data-wow-delay="0.5s">
                  <div class="pricing table-horizontal maxwidth400">
                    <div class="row">
                      <div class="col-md-6">
                        <div class="thumb">
                        <img class="img-fullwidth mb-sm-0" src="images/about/as10.jpg" alt="">
                        </div>
                      </div>
                      <div class="col-md-6 p-30 pl-sm-50">
                        <h4 class="mt-0 mb-5"><a href="#" class="text-white">Upcoming Events Title</a></h4>
                        <ul class="list-inline mb-5 text-white">
                          <li class="pr-0"><i class="fa fa-calendar mr-5"></i> June 26, 2016 |</li>
                          <li class="pl-5"><i class="fa fa-map-marker mr-5"></i>New York</li>
                        </ul>
                        <p class="mb-15 mt-15 text-white">Lorem ipsum dolor sit amet, consectetur adi isicing elit. Quas eveniet, nemo dicta. Ullam nam.</p>
                        <a class="text-white font-weight-600" href="#">Read More →</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section> -->

        <!-- Section: Blog -->
        <!-- <section id="blog">
          <div class="container pb-70 pt-60 pb-sm-20">
            <div class="section-title text-center">
              <div class="row">
                <div class="col-md-8 col-md-offset-2">
                  <h2 class="mt-10 line-height-1 text-uppercase">Recent  <span class="text-theme-color-2"> News</span></h2>
                  <p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem autem<br> voluptatem obcaecati!</p>
                </div>
              </div>
            </div>
            <div class="section-content">
              <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-4 wow fadeInRight" data-wow-duration="1s" data-wow-delay="0.3s">
                  <article class="post clearfix mb-sm-30">
                    <div class="entry-header">
                      <div class="post-thumb thumb"> 
                        <img src="images/blog/1.jpg" alt="" class="img-responsive img-fullwidth"> 
                      </div>
                    </div>
                    <div class="entry-content p-20 pr-10">
                      <div class="entry-meta media mt-0 no-bg no-border">
                        <div class="entry-date media-left flip text-center">
                          <ul>
                            <li class="font-16 font-weight-600 border-bottom bg-white-f1 pt-5 pr-15 pb-5 pl-15">28</li>
                            <li class="font-12 text-white text-uppercase bg-theme-colored pt-5 pr-15 pb-5 pl-15">Feb</li>
                          </ul>
                        </div>
                        <div class="media-body pl-10">
                          <div class="event-content pull-left flip">
                            <h4 class="entry-title text-white text-capitalize m-0"><a href="#">This is a standard post with thumbnail</a></h4>
                            <span class="mb-10 text-gray-darkgray mr-10 font-13"><i class="fa fa-commenting-o mr-5 text-theme-colored"></i> 214 Comments</span>
                            <span class="mb-10 text-gray-darkgray mr-10 font-13"><i class="fa fa-heart-o mr-5 text-theme-colored"></i> 895 Likes</span>
                          </div>
                        </div>
                      </div>
                      <p class="mt-10">Lorem ipsum dolor sit amet, consectetur adipisi cing elit. Molestias eius illum libero dolor nobis deleniti, sint assumenda. Pariatur iste veritatis excepturi, ipsa optio nobis</p>
                          <div class="mt-10"> <a href="blog-single-left-sidebar.html" class="btn btn-theme-colored btn-sm">Read More</a> </div>
                      <div class="clearfix"></div>
                    </div>
                  </article>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4 wow fadeInRight" data-wow-duration="1s" data-wow-delay="0.3s">
                  <article class="post clearfix mb-sm-30">
                    <div class="entry-header">
                      <div class="post-thumb thumb"> 
                        <img src="images/blog/2.jpg" alt="" class="img-responsive img-fullwidth"> 
                      </div>
                    </div>
                    <div class="entry-content p-20 pr-10">
                      <div class="entry-meta media mt-0 no-bg no-border">
                        <div class="entry-date media-left flip text-center">
                          <ul>
                            <li class="font-16 font-weight-600 border-bottom bg-white-f1 pt-5 pr-15 pb-5 pl-15">28</li>
                            <li class="font-12 text-white text-uppercase bg-theme-colored pt-5 pr-15 pb-5 pl-15">Feb</li>
                          </ul>
                        </div>
                        <div class="media-body pl-10">
                          <div class="event-content pull-left flip">
                            <h4 class="entry-title text-white text-capitalize m-0"><a href="#">This is a standard post with thumbnail</a></h4>
                            <span class="mb-10 text-gray-darkgray mr-10 font-13"><i class="fa fa-commenting-o mr-5 text-theme-colored"></i> 214 Comments</span>
                            <span class="mb-10 text-gray-darkgray mr-10 font-13"><i class="fa fa-heart-o mr-5 text-theme-colored"></i> 895 Likes</span>
                          </div>
                        </div>
                      </div>
                      <p class="mt-10">Lorem ipsum dolor sit amet, consectetur adipisi cing elit. Molestias eius illum libero dolor nobis deleniti, sint assumenda. Pariatur iste veritatis excepturi, ipsa optio nobis</p>
                          <div class="mt-10"> <a href="blog-single-left-sidebar.html" class="btn btn-theme-colored btn-sm">Read More</a> </div>
                      <div class="clearfix"></div>
                    </div>
                  </article>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4 wow mb-sm-0  fadeInRight" data-wow-duration="1s" data-wow-delay="0.3s">
                  <article class="post clearfix mb-sm-30">
                    <div class="entry-header">
                      <div class="post-thumb thumb"> 
                        <img src="images/blog/3.jpg" alt="" class="img-responsive img-fullwidth"> 
                      </div>
                    </div>
                    <div class="entry-content p-20 pr-10">
                      <div class="entry-meta media mt-0 no-bg no-border">
                        <div class="entry-date media-left flip text-center">
                          <ul>
                            <li class="font-16 font-weight-600 border-bottom bg-white-f1 pt-5 pr-15 pb-5 pl-15">28</li>
                            <li class="font-12 text-white text-uppercase bg-theme-colored pt-5 pr-15 pb-5 pl-15">Feb</li>
                          </ul>
                        </div>
                        <div class="media-body pl-10">
                          <div class="event-content pull-left flip">
                            <h4 class="entry-title text-white text-capitalize m-0"><a href="#">This is a standard post with thumbnail</a></h4>
                            <span class="mb-10 text-gray-darkgray mr-10 font-13"><i class="fa fa-commenting-o mr-5 text-theme-colored"></i> 214 Comments</span>
                            <span class="mb-10 text-gray-darkgray mr-10 font-13"><i class="fa fa-heart-o mr-5 text-theme-colored"></i> 895 Likes</span>
                          </div>
                        </div>
                      </div>
                      <p class="mt-10">Lorem ipsum dolor sit amet, consectetur adipisi cing elit. Molestias eius illum libero dolor nobis deleniti, sint assumenda. Pariatur iste veritatis excepturi, ipsa optio nobis</p>
                          <div class="mt-10"> <a href="blog-single-left-sidebar.html" class="btn btn-theme-colored btn-sm">Read More</a> </div>
                      <div class="clearfix"></div>
                    </div>
                  </article>
                </div>
              </div>
            </div>
          </div>
        </section> -->

        <!-- Divider: Clients -->


    </div>
    <!-- end main-content -->


    <div class="main-content">
        <!-- Section: inner-header -->


        <!-- Section: Schedule -->
        <section id="schedule" class="divider parallax layer-overlay overlay-white-8"
            data-bg-img="{{ asset('images/table-bg.jpg') }}">

            <div class="main-content">
                <!-- Section: inner-header -->


                <section>
                    <div class="container pt-20 pb-20">
                        <div class="section-content">
                            <div class="row">
                                <div class="col-md-12">
                                    <ul id="myTab" class="nav nav-tabs boot-tabs">
                                        <li class="active"><a href="#one" data-toggle="tab">Day one</a></li>
                                        <li><a href="#profile" data-toggle="tab">Day two</a></li>

                                    </ul>
                                    <div id="myTabContent" class="tab-content">
                                        <div class="tab-pane fade in active" id="one">
                                            <div class="col-md-12">
                                                <table class="table table-striped table-schedule">
                                                    <thead>
                                                        <tr class="bg-theme-colored">
                                                            <th>Participants</th>
                                                            <th>Topic</th>
                                                            <th>Time</th>
                                                            <th>session</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>Speech of the King Salman Complex, delivered by the Acting
                                                                Secretary-General
                                                            </td>
                                                            <td>set of words</td>
                                                            <td>6:00 – 9:00 pm</td>
                                                            <td>opening ceremony</td>
                                                        </tr>
                                                        <tr>
                                                            <td> A visual presentation on the Kingdom's role in supporting
                                                                the Arabic language globally.

                                                            </td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                        </tr>

                                                        <tr>
                                                            <td> Speech by the heads of organizations invited to speak.
                                                                (Words will be arranged according to approvals, and the
                                                                importance of the organization).


                                                            </td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                        </tr>

                                                        <tr>
                                                            <td> Speech of His Highness the Minister of Culture (Chairman of
                                                                the Complex's Board of Trustees)

                                                            </td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                        </tr>

                                                        <tr>
                                                            <td> Speech of the Custodian of the Two Holy Mosques (conference
                                                                sponsor
                                                            </td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="profile">
                                            <div class="col-md-12">
                                                <table class="table table-striped table-schedule">
                                                    <thead>
                                                        <tr class="bg-theme-colored">
                                                            <th>Participants</th>
                                                            <th>Topic</th>
                                                            <th>Time</th>
                                                            <th>session</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>Dialogue session in which guests who work in international
                                                                organizations participate.

                                                            </td>
                                                            <td>Linguistic reality in international organizations, and its
                                                                strategic importance.
                                                            </td>
                                                            <td>9:00 – 10:30

                                                            </td>
                                                            <td>One</td>
                                                        </tr>
                                                        <tr>
                                                            <td> Muhammad bin Abdul Karim Al-Issa, Abdullah Al-Moalimi,
                                                                Muhammad Al-Jasser, Haifa Al-Ayaf, Saqr Al-Muqbel, Guy Daly,
                                                                Salvatore Schiachitano, Houlin Zhao.


                                                            </td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                        </tr>


                                                        <tr style="background: grey; ">
                                                            <td>


                                                            </td>
                                                            <td></td>
                                                            <td style="color: white;">10:30 – 10:45
                                                            </td>
                                                            <td style="color: white;">Break</td>
                                                        </tr>

                                                        <tr>
                                                            <td> Nabila Youn, Abdullah bin Abdul Rahman Al-Baridi, Saleh bin
                                                                Nasser Al-Shuwairekh, Mona Bint Fattouh Al-Jamal, Badr
                                                                Al-Abdulqader, Muhammad Al-Safi Mosteghanemi, Maria Avino.



                                                            </td>
                                                            <td>The civilizational and cultural dimension of
                                                                multilingualism, and the responsibility of international
                                                                organizations in this regard.
                                                            </td>
                                                            <td>10:45– 12:15

                                                            </td>
                                                            <td>Two</td>
                                                        </tr>
                                                        <tr style="background: grey; ">
                                                            <td>


                                                            </td>
                                                            <td></td>
                                                            <td style="color: white;">12:15 – 13:15


                                                            </td>
                                                            <td style="color: white;">Lunch Break
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td> Ali bin Tamim, Nasser Al-Ghali, Al-Tayeb Al-Amrousy, Saleh
                                                                Al-Suhaibani, Ziyad Al-Drees, Hind Muhammad Lootah, Khaleda
                                                                Bouzar.



                                                            </td>
                                                            <td>The Arabic language in international organizations between
                                                                difficulties and solutions.

                                                            </td>
                                                            <td>13:15 – 14:45



                                                            </td>
                                                            <td>Three</td>
                                                        </tr>

                                                        <tr>
                                                            <td> Saad Al-Bazai, Abdulaziz Al-Nawfal, Hala Al-Shahrani,
                                                                Muhammad Al-Alam, Abdul-Fattah Al-Hajjari, Sultan Al-Harbi,
                                                                Lee Junho, Vitaly sleeping.




                                                            </td>
                                                            <td>Translation from and into Arabic in international
                                                                organizations; Reality and future prospects.


                                                            </td>
                                                            <td>14:45 – 16:15





                                                            </td>
                                                            <td>Four</td>
                                                        </tr>
                                                        <tr style="background: grey; ">
                                                            <td>


                                                            </td>
                                                            <td></td>
                                                            <td style="color: white;">16:15 – 16:45




                                                            </td>
                                                            <td style="color: white;">Break

                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td> Salem Al-Malik, Muhammad Omar, Saleh Al-Wahaibi, Abdul
                                                                Rahman Al-Asimi, Hassan Al-Shafei, Abdullah Al-Washmi.





                                                            </td>
                                                            <td>Initiatives and projects of international organizations. To
                                                                enable Arabic in organizations



                                                            </td>
                                                            <td>16:45 –17:15







                                                            </td>
                                                            <td>Five</td>
                                                        </tr>
                                                    </tbody>

                                                </table>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="col-md-12">

                                </div>
                            </div>

                        </div>
                    </div>
                </section>



            </div>
            <div class="container pt-80 pb-60">
                <div class="section-content">
                    <div class="row">

                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
