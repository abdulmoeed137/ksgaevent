@extends('layouts.app')
<style>
    .login{
        display: none !important;
    }
    .banner-img::before{
        content: "";
        position: absolute;
        width: 100%;
        height: 100%;
        background-color: #000000d4;
    }
</style>
@section('content')
    <section class="banner-img" style="background-image: url({{ asset('images/about.jpg') }});  background-size: cover; background-repeat: no-repeat;background-position: bottom;padding-bottom: 40px;">
        <section>
            <div class="container pt-0 pb-0">
                <div class="section-content">
                    <div class="col-md-offset-2 col-sm-12 col-md-8">
                        <br>
                        <h1 class="title text-white text-uppercase line-bottom mt-0 mb-0 text-center">
                            {{ __('Conference Date') }}
                        </h1>
                        {{-- <span style="background: #F5CC44;
                                      width: 14%;
                                      margin: auto;
                                      height: 10px;
                                      display: block; color: #F5CC44; text-align: center;">_____</span> --}}
                        <br>
                        <section class="coming-soon">
                            <div>
                                <div class="countdown">
                                    <div class="container-day child">
                                        <h3  id="days">Time</h3>
                                        <h3 class="time-dis">{{ __('Days') }}</h3>
                                    </div>
                                    <div class="container-hour child">
                                        <h3 id="hours">Time</h3>
                                        <h3 class="time-dis"> {{ __('Hours') }} </h3>
                                    </div>
                                    <div class="container-minute child">
                                        <h3  id="minutes">Time</h3>
                                        <h3 class="time-dis">{{ __('Minutes') }}</h3>
                                    </div>
                                    <div class="container-second child-last">
                                        <h3 id="seconds">Time</h3>
                                        <h3 class="time-dis">{{ __('Seconds') }}</h3>
                                    </div>
                                </div>
                                {{-- <div id="aftercountdownbutton">
                                    <a href="{{route('livelinks')}}" class="btn btn-info">Join Us</a>
                                </div> --}}
                            </div>
                        </section>

                    </div>

                    <br>

                    

                    {{-- <div class="row equal-height-inner mt-sm-0">
                        <div
                            class="col-md-offset-1 col-sm-12 col-md-10 pr-0 pr-sm-15 sm-height-auto mt-sm-0 wow fadeInLeft animation-delay1 mt-120">
                            <div class="sm-height-auto aboutImage"
                                style="background-size: cover;background-position: center;">
                                <div class="p-20" style="position: relative;">



                                </div>
                            </div>
                        </div>

                    </div> --}}
                </div>

                <div class="row">
                    <div class="col-md-12 text-center">
                        <div>
                            <a href="{{route('livelinks')}}" class="join-button">Join Us</a>
                        </div>
                    </div>
                </div>
            </div>
            {{-- <a href="">Submission</a> --}}
        </section>
    </section>
    {{-- <iframe style="display: block;" width="100%" height="450px;" src="https://www.youtube.com/embed/2W85Dwxx218" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


    <div style="width: 50%; margin: auto;">
        <blockquote class="twitter-tweet"><p lang="en" dir="ltr">Sunsets don&#39;t get much better than this one over <a href="https://twitter.com/GrandTetonNPS?ref_src=twsrc%5Etfw">@GrandTetonNPS</a>. <a href="https://twitter.com/hashtag/nature?src=hash&amp;ref_src=twsrc%5Etfw">#nature</a> <a href="https://twitter.com/hashtag/sunset?src=hash&amp;ref_src=twsrc%5Etfw">#sunset</a> <a href="http://t.co/YuKy2rcjyU">pic.twitter.com/YuKy2rcjyU</a></p>&mdash; US Department of the Interior (@Interior) <a href="https://twitter.com/Interior/status/463440424141459456?ref_src=twsrc%5Etfw">May 5, 2014</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
    </div> --}}

@endsection
