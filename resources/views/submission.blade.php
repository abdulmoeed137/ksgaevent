@extends('layouts.app')

@section('content')
<style>
    .reg{
        display: none !important;
    }
</style>
<div class="row foot-row" style="display: flex;">

    <div class="col-md-6 hidden-xs p-0">
        <figure class="match"
            style="background-image: url({{asset('images/party-bg.jpg')}});  background-size: cover; background-repeat: no-repeat;">

        </figure>
    </div>
    <div class="col-md-6 bg-sil match">

            <div class="col-md-12">
                <h2 class="mt-0 line-height-1 text-uppercase text-left pt-30 mb-10 right-cap"> <span class="text-theme-color-2">{{__('Scientific Participation')}}
                </span></h2>
                {{-- <span style="background: #F5CC44;
                          width: 14%;
                          height: 10px;
                          display: block; color: #F5CC44; margin-bottom: 20px;;">_____</span> --}}
            </div>
        <div class="reg-form">
            <form action="{{route('storesubmission')}}" method="POST" enctype="multipart/form-data" >
                @CSRF
           <div class="row">
            <div class="form-group col-md-6">
                <label for="Name">{{__('Name')}} <span style="color: red;">*</span></label>
                <input name="Name" class="form-control @error('Name') is-invalid @enderror" type="text">
                @error('Name')
                <span style="color: red;">{{$message}}</span>
                @enderror
            </div>

            <div class="form-group col-md-6">
                <label for="Email">{{__('Email')}} <span style="color: red;">*</span></label>
                <input name="Email" class="form-control @error('Email') is-invalid @enderror" type="email">
                @error('Email')
                <span style="color: red;">{{$message}}</span>
                @enderror
            </div>
           </div>

              <div class="row">
                <div class="form-group col-md-6">
                    <label for="PhoneNumber">{{__('Phone Number')}} <span style="color: red;">*</span></label>
                    <input name="PhoneNumber" class="form-control @error('PhoneNumber') is-invalid @enderror" type="text">
                    @error('PhoneNumber')
                    <span style="color: red;">{{$message}}</span>
                    @enderror
                </div>

                <div class="form-group col-md-6">
                    <label for="Organization">{{__('Organization')}} <span style="color: red;">*</span></label>
                    <input name="Organization" class="form-control @error('Organization') is-invalid @enderror" type="text">
                    @error('Organization')
                    <span style="color: red;">{{$message}}</span>
                    @enderror
                </div>
              </div>

               <div class="row">
                <div class="form-group col-md-6">
                    <label for="ConferenceTopic">{{__('Conference Theme and Topic')}} <span style="color: red;">*</span></label>
                    <input name="ConferenceTopic" class="form-control @error('ConferenceTopic') is-invalid @enderror" type="text">
                    @error('ConferenceTopic')
                    <span style="color: red;">{{$message}}</span>
                    @enderror
                </div>
                <div class="form-group col-md-6">
                    <label for="Titleofresearch">{{__('Title Of Scientific Participation')}} <span style="color: red;">*</span></label>
                    <input name="Titleofresearch" class="form-control @error('Titleofresearch') is-invalid @enderror" type="text">
                    @error('Titleofresearch')
                    <span style="color: red;">{{$message}}</span>
                    @enderror
                </div>
               </div>
               
               <div class="row">
                <div class="form-group col-md-6">
                    <label for="Country">{{__('Country')}} <span style="color: red;">*</span></label>
                    <input name="Country" class="form-control @error('Country') is-invalid @enderror" type="text">
                    @error('Country')
                    <span style="color: red;">{{$message}}</span>
                    @enderror
                </div>
                <div class="form-group col-md-6">
                    <label for="City">{{__('City')}} <span style="color: red;">*</span></label>
                    <input name="City" class="form-control @error('City') is-invalid @enderror" type="text">
                    @error('City')
                    <span style="color: red;">{{$message}}</span>
                    @enderror
                </div>
               </div>

                <div class="form-group col-md-12">
                    <label for="fileupload">{{__('File Upload')}} <span style="color: red;">*</span></label>
                    <input name="fileupload"  class="form-control @error('fileupload') is-invalid @enderror" type="file">
                    @error('fileupload')
                    <span style="color: red;">{{$message}}</span>
                    @enderror
                </div>
               <div class="row">
                <div class="form-group col-md-12 text-center">
                    <hr style="background: #00000017;height: 1px;">
                    <button class="sub-btn">{{__('Submit')}}</button>
                    <br> <br>
                </div>
               </div>
            </form>
        </div>
    </div>
</div>

<script>
@if(Session::has('message'))
toastr.options = {
    "closeButton": true,
    "progressBar": true
}
toastr.success("{{ session('message') }}");
@endif
</script>
@endsection
