@extends('layouts.app')

@section('content')
<style>
    .login{
        display: none !important;
    }
</style>
<div class="container-fluid p-0">
    <div class="row foot-row">

        <div class="col-md-7 hidden-xs p-0">
            <figure class=""
                style="background-image: url({{asset('images/login-bg-2.jpg')}}); height: 500px; background-size: cover; background-repeat: no-repeat;background-position: center;">
    
            </figure>
        </div>
        <div class="col-md-5  bg-sil" style="height: 500px;">
            <div class="col-md-12">
                <h2 class="mt-0 line-height-1 text-uppercase text-left pt-30 mb-10 right-cap"> <span class="text-theme-color-2 ">{{__('Log In')}}
                </span></h2>
                {{-- <span style="background: #F5CC44;
                          width: 14%;
                          height: 10px;
                          display: block; color: #F5CC44; margin-bottom: 20px;;">_____</span> --}}
            </div>
            <div class="reg-form">
                <form method="POST" action="{{ route('login') }}">
                    @CSRF
                    <div class="form-group col-md-12">
                        <label for="email">{{__('Email')}}</label>
                        <input type="email" name="email" class="form-control @error('email') is-invalid @enderror">
                        @error('email')
                       <span style="color: red;">{{$message}}</span>
                        @enderror
                    </div>
    
                    <div class="form-group col-md-12">
                        <label for="password">{{__('Password')}}</label>
                        <input name="password" type="password" class="form-control @error('password') is-invalid @enderror">
                        @error('password')
                       <span style="color: red;">{{$message}}</span>
                        @enderror
                       
                    </div>
    
              
                  
                    <div class="form-group col-md-12 text-center">
                        <hr>
                        <button class="sub-btn">{{__('Log In')}}</button>
                        <br>
                        <br>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection