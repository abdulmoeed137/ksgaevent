@extends('layouts.app')

@section('content')
    <style>
        .reg-form {
            padding: 17px;
        }

    </style>
    <div class="row foot-row">

        <div class="col-md-6 hidden-xs pr-0 pl-0">
            <figure class="match"
                style="background-image: url({{ asset('images/photos/about-2.jpg') }}); background-size: cover; background-repeat: no-repeat;">

            </figure>
        </div>
        <div class="col-md-6 match bg-sil">
            <div class="col-md-12">
                <h2 class="mt-0 line-height-1 text-uppercase text-left pt-30 mb-10 right-cap"> <span
                        class="text-theme-color-2">{{__('Registration')}}
                    </span></h2>
                {{-- <span style="background: #F5CC44;
                          width: 14%;
                          height: 10px;
                          display: block; color: #F5CC44; margin-bottom: 20px;;">_____</span> --}}
            </div>
            <div class="reg-form">
                <form action="{{ route('storeregister') }}" method="POST">
                    @CSRF
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="Name">{{__('Name')}} <span style="color: red;">*</span> </label>
                            <input name="Name" class="form-control @error('Name') is-invalid @enderror" type="text">
                            @error('Name')
                            <span style="color: red;">{{$message}}</span>
                            @enderror
                        </div>

                        <div class="form-group col-md-6">
                            <label for="Email">{{__('Email')}}</label>
                            <input name="Email" class="form-control @error('Email') is-invalid @enderror" type="text">
                            @error('Email')
                            <span style="color: red;">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="Company">{{__('Company')}} <span style="color: red;">*</span></label>
                            <input name="Company" class="form-control @error('Company') is-invalid @enderror" type="text">
                            @error('Company')
                            <span style="color: red;">{{$message}}</span>
                            @enderror
                        </div>

                        <div class="form-group col-md-6">
                            <label for="Position">{{__('Position')}} <span style="color: red;">*</span></label>
                            <input name="Position" class="form-control @error('Position') is-invalid @enderror" type="text">
                            @error('Position')
                            <span style="color: red;">{{$message}}</span>
                            @enderror
                        </div>
                    </div>



                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="Phone">{{__('Phone')}} <span style="color: red;">*</span></label>
                            <input name="Phone" class="form-control @error('Phone') is-invalid @enderror" type="number">
                            @error('Phone')
                                 <span style="color: red;">{{$message}}</span>
                            @enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label for="Address">{{__('Address')}} <span style="color: red;">*</span></label>
                            <input name="Address" class="form-control @error('Address') is-invalid @enderror" type="text">
                            @error('Address')
                                 <span style="color: red;">{{$message}}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="Password">{{__('Password')}} </label>
                            <input name="Password" class="form-control @error('Password') is-invalid @enderror"
                                type="password">
                            @error('Password')
                                 <span style="color: red;">{{$message}}</span>
                            @enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label for="Confirm_Password">{{__('Confirm Password')}}<span style="color: red;">*</span>
                            </label>
                            <input name="Confirm_Password"
                                class="form-control @error('Confirm_Password') is-invalid @enderror" type="password">
                            @error('Confirm_Password')
                                 <span style="color: red;">{{$message}}</span>
                            @enderror
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-12">
                            <br>
                            <label>{{__('Attendance')}}</label>
                            {{-- <br> --}}
                            {{-- <span
                              class="toggle--on1 mr-3" style="    font-size: 17px;">{{__('Online')}}</span>
                            <input type="checkbox" id="id-name--1" name="attendance" class="switch-input">
                            <label for="id-name--1" class="switch-label"> <span class="toggle--off1"> </span></label>
                            <span style="    font-size: 17px;">{{__('Onsight')}}</span> --}}
                            <!-- <div class="row">
                                <div class="col-md-12">

                                    <input class="form-check-input " type="radio" name="attendance" id="exampleRadios1"
                                        value="Online" checked>
                                    <label class="form-check-label" for="exampleRadios1">
                                        {{__('Online')}}
                                    </label>
                                    <br>
                                    <input class="form-check-input" type="radio" name="attendance" id="exampleRadios2"
                                        value="Onsight">
                                    <label class="form-check-label" for="exampleRadios2">
                                        {{__('Onsight')}}
                                    </label>
                                </div>
                            </div> -->




                        </div>
                    </div>
                    <div class="radio">


                      <ul>
                      <li>
                        <input type="radio" id="f-option" value="Online" name="attendance" checked>
                        <label for="f-option">{{__('Online')}}</label>

                        <div class="check"></div>
                      </li>

                      <li>
                        <input type="radio" id="s-option" value="Onsight" name="attendance">
                        <label for="s-option">{{__('Onsight')}}</label>

                        <div class="check"><div class="inside"></div></div>
                      </li>

                    </ul>
                    </div>

                    <div class="form-group col-md-12 text-center">
                        <hr>
                        <button class="sub-btn">{{__('Submit')}}</button>
                        <br>
                        <br>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script>
        @if (Session::has('message'))
            toastr.options = {
            "closeButton": true,
            "progressBar": true,
            }
            toastr.success("{{ session('message') }}");
        @endif
    </script>
@endsection
