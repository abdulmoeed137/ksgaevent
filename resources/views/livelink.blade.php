@extends('layouts.app')

@section('content')
<br>
<br>
    {{-- <iframe style="display: block;" width="100%" height="450px;" src="https://www.youtube.com/embed?v=ibsJDPbwEL4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> --}}
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            @foreach ($youtubelink as $youtubelink)
                <iframe width="100%" height="409" src="{{ $youtubelink->link }}" title="YouTube video player"
                    frameborder="0"
                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                    allowfullscreen></iframe>
            @endforeach
        </div>
    </div>
    <br>

    @foreach ($twitterlink as $twitterlink)
        <div style="width: 50%; margin: auto;">
            <blockquote class="twitter-tweet">
                <p lang="en" dir="ltr">Sunsets don&#39;t get much better than this one over.</p>&mdash; US Department of the
                Interior (@Interior) <a href="{{ $twitterlink->link }}">May 5, 2014</a>
            </blockquote>
            <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
        </div>
    @endforeach
@endsection
