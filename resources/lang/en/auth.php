<?php
return [
    'failed' => 'These credentials do not match our records',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'required'    => 'Input is required here',
    'email.required'    => 'Input is required here',
    'Email.required'    => 'Input is required here',
    'username.required'    => 'Input is required here',
    'password.required'    => 'Input is required here',
    'Password.required'    => 'Input is required here',
]
?>
