<?php
return [
'required'=>'الحقل مطلوب',
'unique'=>'This email already registered',
'regex'=>'رقم الجوال غير صحيح',
'same'=>'يجب أن يتطابق تأكيد كلمة المرور وكلمة المرور',
'failed' => 'لا يوجد تطابق',

// 'required'=>'الحقل مطلوب',
// 'email'=>'Email format invalid',
// 'unique'=>'This email already registered',
// 'regex'=>'رقم الجوال غير صحيح',
// 'same'=>'يجب أن يتطابق تأكيد كلمة المرور وكلمة المرور',
// 'failed' => 'لا يوجد تطابق',
'mimes' => 'File should be pdf format',
]
?>
